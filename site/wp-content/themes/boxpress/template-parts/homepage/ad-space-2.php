<?php if( have_rows('ads_space_2') ): ?>

	<div class="homepage-ads">
    <div class="wrap">
      <div class="ads-wrap">

        <?php while( have_rows('ads_space_2') ): the_row();

      		// vars
      		$image = get_sub_field('graphic');
      		$link = get_sub_field('link');

      		?>

      		<div class="ad">
      			<?php if( $link ): ?>
      				<a href="<?php echo $link; ?>" target="_blank">
      			<?php endif; ?>
      				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
      			<?php if( $link ): ?>
      				</a>
      			<?php endif; ?>
      		</div>

      	<?php endwhile; ?>

      </div>
    </div>
  </div>

<?php endif; ?>

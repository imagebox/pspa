<?php
  // Membership Callout Vars
  $membership_heading = get_field('membership_heading');
  $membership_content = get_field('membership_content');
  $membership_new     = get_field('new_members');
  $membership_current = get_field('current_members');
?>

<div class="home-member-callout">
  <img class="mc-background" src="<?php bloginfo('template_directory');?>/assets/img/temp/bg-member-callout.jpg" alt="">
  <div class="wrap">

    <div class="home-member-callout__inner">

      <div class="home-member-callout__left">
        <h3>
          <div class="icon">
            <svg width="78" height="78">
              <use xlink:href="#member-icon"></use>
            </svg>
          </div>
          <span><?php echo $membership_heading;?></span>
        </h3>
        <p><?php echo $membership_content;?></p>
      </div>

      <div class="home-member-callout__right">
        <div class="member-links">

          <?php if( $membership_new ): ?>
            <div class="member-links__left">
              <h4><?php echo $membership_new['heading'];?></h4>
              <?php
                if( have_rows('new_members') ):
                  while ( have_rows('new_members') ) : the_row();
                  ?>
                    <?php if( have_rows('links') ): ?>
                      <ul>
                      <?php while ( have_rows('links') ) : the_row();
                        $membership_link = get_sub_field('link');
                        if( $membership_link ):
                        	$membership_link_url = $membership_link['url'];
                        	$membership_link_title = $membership_link['title'];
                        	$membership_link_target = $membership_link['target'] ? $membership_link['target'] : '_self';
                        ?>
                        <li><a href="<?php echo esc_url($membership_link_url); ?>" target="<?php echo esc_attr($membership_link_target); ?>"><span><?php echo esc_html($membership_link_title); ?></span> <svg width="23" height="23"><use xlink:href="#arrow-blue"></use></svg></a></li>
                      <?php endif; ?>

                      <?php endwhile; ?>
                      </ul>
                    <?php endif;
                  endwhile;
                endif;
              ?>
            </div>
          <?php endif; ?>

          <?php if( $membership_current ): ?>
            <div class="member-links__right">
              <h4><?php echo $membership_current['heading'];?></h4>
              <?php
                if( have_rows('current_members') ):
                  while ( have_rows('current_members') ) : the_row();
                  ?>
                    <?php if( have_rows('links') ): ?>
                      <ul>
                      <?php while ( have_rows('links') ) : the_row();
                        $membership_link = get_sub_field('link');
                        if( $membership_link ):
                        	$membership_link_url = $membership_link['url'];
                        	$membership_link_title = $membership_link['title'];
                        	$membership_link_target = $membership_link['target'] ? $membership_link['target'] : '_self';
                        ?>
                        <li><a href="<?php echo esc_url($membership_link_url); ?>" target="<?php echo esc_attr($membership_link_target); ?>"><span><?php echo esc_html($membership_link_title); ?></span> <svg width="23" height="23"><use xlink:href="#arrow-blue"></use></svg></a></li>
                      <?php endif; ?>

                      <?php endwhile; ?>
                      </ul>
                    <?php endif;
                  endwhile;
                endif;
              ?>
            </div>
          <?php endif; ?>

        </div>
      </div>

    </div>

  </div>
</div>

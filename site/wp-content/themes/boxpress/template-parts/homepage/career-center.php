<?php
  $career_center_heading    = get_field('career_center_heading');
  $career_center_embed_code = get_field('career_center_embed_code');
  $career_center_more_info  = get_field('career_center_more_information');
?>

<div class="home-career-center">
  <div class="wrap">
    <header>
      <h3>
        <div class="icon icon-career-center">
          <img src="<?php bloginfo('template_directory');?>/assets/img/dist/icons/career-icon.png" width="78" height="78" alt="">
        </div>
        <span><?php echo $career_center_heading;?></span>
      </h3>
    </header>
    <div class="section-info">
      <div class="section-info__left">
        <?php echo $career_center_embed_code;?>
      </div>
      <?php if( $career_center_more_info ): ?>
        <div class="section-info__right">
          <h4><?php echo $career_center_more_info['heading'];?></h4>
          <?php
            if( have_rows('career_center_more_information') ):
              while ( have_rows('career_center_more_information') ) : the_row();
              ?>
                <?php if( have_rows('links') ): ?>
                  <ul>
                  <?php while ( have_rows('links') ) : the_row();
                    $career_center_link = get_sub_field('link');
                    if( $career_center_link ):
                      $career_center_link_url = $career_center_link['url'];
                      $career_center_link_title = $career_center_link['title'];
                      $career_center_link_target = $career_center_link['target'] ? $career_center_link['target'] : '_self';
                    ?>
                    <li><a href="<?php echo esc_url($career_center_link_url); ?>" target="<?php echo esc_attr($career_center_link_target); ?>"><span><?php echo esc_html($career_center_link_title); ?></span> <svg width="23" height="23"><use xlink:href="#arrow-blue"></use></svg></a></li>
                  <?php endif; ?>

                  <?php endwhile; ?>
                  </ul>
                <?php endif;
              endwhile;
            endif;
          ?>
        </div>
      <?php endif;?>
    </div>
  </div>
</div>

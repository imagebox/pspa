<?php
/**
 * Displays the Slideshow layout
 *
 * @package BoxPress
 */
?>
<?php if ( have_rows( 'masthead_carousel' )) : ?>
  <div class="homepage--slideshow">
    <div class="homepage-carousel owl-carousel js-owl-carousel">
      <?php while ( have_rows( 'masthead_carousel' )) : the_row();
          $slide_photo      = get_sub_field( 'slide_photo' );
          $slide_heading  = get_sub_field( 'heading' );
          $slide_subhead  = get_sub_field( 'subheading' );
          $slide_button  = get_sub_field( 'button' );
          $image_size     = 'home_slideshow';
          $slide_photo_url  = $slide_photo['sizes'][ $image_size ];
          $slide_toggle     = get_sub_field('slide_toggle');
        ?>
        <?php if ($slide_toggle == true) : ?>
          <div class="item" style="background-image: linear-gradient(black, black), url(<?php echo $slide_photo_url; ?>); background-repeat: no-repeat; background-position: center center; background-size: cover; background-blend-mode: saturation;">
            <div class="slide-overlay"></div>
            <div class="wrap">
              <div class="slide-message">
                <h1><?php echo $slide_heading;?></h1>
                <?php echo $slide_subhead;?>
                <?php if( $slide_button ):
                	$slide_button_url = $slide_button['url'];
                	$slide_button_title = $slide_button['title'];
                	$slide_button_target = $slide_button['target'] ? $slide_button['target'] : '_self';
                ?>
                	<a class="button button--blue button--arrow" href="<?php echo esc_url($slide_button_url); ?>" target="<?php echo esc_attr($slide_button_target); ?>"><?php echo esc_html($slide_button_title); ?></a>
                <?php endif; ?>
              </div>
            </div>
          </div>
        <?php endif; ?>
      <?php endwhile; ?>
    </div>
  </div>
<?php endif; ?>

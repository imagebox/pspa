<?php if( have_rows('sponsors_carousel') ): ?>
  <div class="homepage-sponsors">
    <?php if(get_field('sponsors_heading')) { ?>
      <div class="carousel-heading">
        <div class="wrap">
          <h3><?php the_field('sponsors_heading');?></h3>
        </div>
      </div>
    <?php } ?>
    <div class="custom-owl-nav"></div>
    <div class="sponsor-carousel owl-carousel">
      <?php while( have_rows('sponsors_carousel') ): the_row();
    		$sponsor_logo = get_sub_field('logo');
    		$sponsor_name = get_sub_field('name');
    		$sponsor_link = get_sub_field('link');

    	?>
        <div class="item">
          <?php if( $sponsor_link ): ?>
    				<a href="<?php echo $sponsor_link; ?>" target="_blank">
    			<?php endif; ?>


          <?php
          if( !empty($sponsor_logo) ):

            // vars
            $url = $sponsor_logo['url'];
            $alt = $sponsor_logo['alt'];

            // thumbnail
            $size = 'sponsor_logo';
            $thumb = $sponsor_logo['sizes'][ $size ];
            $width = $sponsor_logo['sizes'][ $size . '-width' ];
            $height = $sponsor_logo['sizes'][ $size . '-height' ];
            ?>

              <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />

          <?php endif; ?>


          <?php if( $sponsor_link ): ?>
    				</a>
    			<?php endif; ?>
        </div>
      <?php endwhile; ?>
    </div>
  </div>

<?php endif; ?>

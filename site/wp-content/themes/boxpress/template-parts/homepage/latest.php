<div class="home-latest">
  <div class="wrap">

    <div class="home-latest__inner">

      <div class="home-latest__box">
        <header>
          <h4>Latest News</h4>
        </header>
        <div class="box__inner">
        <?php
        	$news_query = array(
        		'post_type' => 'post',
        		'posts_per_page' => 3,
        	);
        	$news_loop = new WP_Query( $news_query );
        ?>
        <ul class="post-list">
        	<?php while ( $news_loop->have_posts() ) : $news_loop->the_post(); ?>
        		<li>
              <h6>
                <?php
                  $catList = '';
                  foreach((get_the_category()) as $cat) {
                  	$catID = get_cat_ID( $cat->cat_name );
                  	$catLink = get_category_link( $catID );
                  	if(!empty($catList)) {
                  		$catList .= '&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;&nbsp;';
                  	}
                  	//$catList .= '<a href="'.$catLink.'">'.$cat->cat_name.'</a>';
                    $catList .= ''.$cat->cat_name.'';
                  }
                  echo $catList;

                ?>
              </h6>
        			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
        		</li>
        	<?php
            endwhile;
            wp_reset_query();
          ?>
        </ul>

        </div>
        <footer class="box__callout">
          <a href="/news/blog/" class="button button--yellow button--arrow">View All</a>
        </footer>
      </div>

      <div class="home-latest__box">
        <header>
          <h4>Events and CME</h4>
        </header>
        <div class="box__inner">
          <?php
          	$events_query = array(
              'post_type' => 'tribe_events',
							'posts_per_page' => '3',
							'start_date' => date('Y-m-d H:i:s', strtotime("now")),
							'orderby' => 'event_date',
							'order' => 'ASC',
          	);
          	$events_loop = new WP_Query( $events_query );
          ?>
          <ul class="post-list">
          	<?php while ( $events_loop->have_posts() ) : $events_loop->the_post(); ?>
          		<li>
                <h6>
                  <?php echo tribe_get_start_date($post->ID, false, 'm.d.y'); ?>
                </h6>
          			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
          		</li>
          	<?php
              endwhile;
              wp_reset_query();
            ?>
          </ul>
        </div>
        <footer class="box__callout">
          <a href="/events-and-cme/calendar/" class="button button--yellow button--arrow">View All</a>
        </footer>
      </div>

      <div class="home-latest__box">
        <header>
          <h4>Facebook</h4>
        </header>
        <div class="box__inner">
          <div class="fb-page"
            data-href="https://www.facebook.com/PSPA-Pennsylvania-Society-of-Physician-Assistants-113836645307125/"
            data-tabs="timeline"
            data-width=""
            data-height=""
            data-small-header="true"
            data-adapt-container-width="true"
            data-hide-cover="true"
            data-show-facepile="false">
            <blockquote cite="https://www.facebook.com/PSPA-Pennsylvania-Society-of-Physician-Assistants-113836645307125/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PSPA-Pennsylvania-Society-of-Physician-Assistants-113836645307125/">PSPA-Pennsylvania Society of Physician Assistants</a></blockquote></div>
        </div>
        <footer class="box__callout">
          <a target="_blank" href="https://www.facebook.com/PSPA-Pennsylvania-Society-of-Physician-Assistants-113836645307125/" class="button button--yellow button--arrow">View Facebook</a>
        </footer>
      </div>

    </div>

  </div>
</div>

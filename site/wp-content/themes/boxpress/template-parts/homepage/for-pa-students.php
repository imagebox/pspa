<?php
  $students_heading = get_field('students_heading');
?>

<div class="home-for-pa-students">
  <div class="wrap">
    <div class="home-for-pa-students__content">
      <h3>
        <div class="icon">
          <svg width="78" height="78">
            <use xlink:href="#computer-icon"></use>
          </svg>
        </div>
        <span><?php echo $students_heading;?></span>
      </h3>
      <?php if( have_rows('students_links') ): ?>
        <ul>
        <?php while ( have_rows('students_links') ) : the_row();
          $student_link = get_sub_field('link');
          if( $student_link ):
            $student_link_url = $student_link['url'];
            $student_link_title = $student_link['title'];
            $student_link_target = $student_link['target'] ? $student_link['target'] : '_self';
          ?>
          <li><a href="<?php echo esc_url($student_link_url); ?>" target="<?php echo esc_attr($student_link_target); ?>"><span><?php echo esc_html($student_link_title); ?></span> <svg width="23" height="23"><use xlink:href="#arrow-white"></use></svg></a></li>
        <?php endif; ?>

        <?php endwhile; ?>
        </ul>
      <?php endif; ?>
    </div>
  </div>
</div>

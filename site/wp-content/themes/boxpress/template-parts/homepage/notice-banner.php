<?php
  // Notice Banner Vars
  $display_notice_banner = get_field('display_notice_banner');
  $notice_banner_photo   = get_field('notice_banner_photo');
  $notice_banner_heading = get_field('notice_banner_heading');
  $notice_banner_content = get_field('notice_banner_content');
  $notice_banner_button  = get_field('notice_banner_button');

  $display_two_notices   = get_field('display_two_notices');
?>

<?php if( $display_notice_banner ): ?>


  <?php if ( $display_two_notices ) { ?>

    <div class="notice-banner-dual">
      <div class="wrap">

        <?php if( have_rows('multiple_notices') ): ?>

        	<div class="notices">

        	<?php while( have_rows('multiple_notices') ): the_row();
        		// vars
        		$photo = get_sub_field('photo');
        		$heading = get_sub_field('heading');
            $content = get_sub_field('content');
        		$button = get_sub_field('button');
        		?>

        		<div class="notice">

              <?php if( $photo ):
                $photo_image_size     = 'small_square';
                $photo_url  = $photo['sizes'][ $photo_image_size ];
              ?>
                <div class="notice-photo">
                  <img src="<?php echo $photo_url;?>" alt="">
                </div>
              <?php endif;?>

              <div class="notice-content">
                <?php if ($heading) : ?>
                  <h3><?php echo $heading; ?></h3>
                <?php endif; ?>

        		    <?php if ($content) : ?>
                  <p><?php echo $content; ?></p>
                <?php endif; ?>

                <?php if( $button ):
                  $button_url = $button['url'];
                  $button_title = $button['title'];
                  $button_target = $button['target'] ? $button['target'] : '_self';
                ?>
                  <a class="button" href="<?php echo esc_url( $button_url ); ?>" target="<?php echo esc_attr( $button_target ); ?>"><?php echo esc_html( $button_title ); ?></a>
                <?php endif; ?>
              </div>

        		</div>

        	<?php endwhile; ?>

          </div>

        <?php endif; ?>

      </div>
    </div>

  <?php } else { ?>

    <div class="notice-banner">
      <div class="wrap">

        <div class="notice-banner__inner">

          <?php if( $notice_banner_photo ):
            $notice_banner_image_size     = 'small_square';
            $notice_banner_photo_url  = $notice_banner_photo['sizes'][ $notice_banner_image_size ];
          ?>
            <div class="notice-banner__photo">
              <img src="<?php echo $notice_banner_photo_url;?>" alt="">
            </div>
          <?php endif;?>

          <div class="notice-banner__info">
            <?php if( $notice_banner_heading ): ?>
              <h3><?php echo $notice_banner_heading;?></h3>
            <?php endif;?>
            <?php if( $notice_banner_content ): ?>
              <p><?php echo $notice_banner_content;?></p>
            <?php endif;?>
          </div>

          <?php if( $notice_banner_button ):
            $notice_banner_button_url = $notice_banner_button['url'];
            $notice_banner_button_title = $notice_banner_button['title'];
            $notice_banner_button_target = $notice_banner_button['target'] ? $notice_banner_button['target'] : '_self';
          ?>
            <div class="notice-banner__link">
              <a class="button button--arrow" href="<?php echo esc_url($notice_banner_button_url); ?>" target="<?php echo esc_attr($notice_banner_button_target); ?>"><?php echo esc_html($notice_banner_button_title); ?></a>
            </div>
          <?php endif; ?>

        </div>

      </div>
    </div>

  <?php } ?>

<?php endif;?>

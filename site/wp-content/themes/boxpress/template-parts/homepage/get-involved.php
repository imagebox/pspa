<?php
  $get_involved_heading    = get_field('get_involved_heading');
  $get_involved_subheading = get_field('get_involved_subheading');
  $get_involved_copy       = get_field('get_involved_copy');
  $get_involved_button     = get_field('get_involved_button');
  $get_involved_info       = get_field('get_involved_more_information');
?>
<div class="home-get-involved">
  <div class="wrap">
    <header>
      <h3>
        <div class="icon">
          <svg width="78" height="78">
            <use xlink:href="#bulb-icon"></use>
          </svg>
        </div>
        <span class="long"><?php echo $get_involved_heading;?></span>
      </h3>
    </header>
    <div class="section-info">
      <div class="section-info__left">
        <h4><?php echo $get_involved_subheading;?></h4>
        <p><?php echo $get_involved_copy;?></p>
        <?php if( $get_involved_button ):
        	$get_involved_button_url = $get_involved_button['url'];
        	$get_involved_button_title = $get_involved_button['title'];
        	$get_involved_button_target = $get_involved_button['target'] ? $get_involved_button['target'] : '_self';
        	?>
          <a class="button button--arrow" href="<?php echo esc_url($get_involved_button_url); ?>" target="<?php echo esc_attr($get_involved_button_target); ?>"><?php echo esc_html($get_involved_button_title); ?></a>
        <?php endif; ?>

      </div>
      <?php if( $get_involved_info ): ?>
        <div class="section-info__right">
          <h4><?php echo $get_involved_info['heading'];?></h4>
          <?php
            if( have_rows('get_involved_more_information') ):
              while ( have_rows('get_involved_more_information') ) : the_row();
              ?>
                <?php if( have_rows('links') ): ?>
                  <ul>
                  <?php while ( have_rows('links') ) : the_row();
                    $get_involved_link = get_sub_field('link');
                    if( $get_involved_link ):
                      $get_involved_link_url = $get_involved_link['url'];
                      $get_involved_link_title = $get_involved_link['title'];
                      $get_involved_link_target = $get_involved_link['target'] ? $get_involved_link['target'] : '_self';
                    ?>
                    <li><a href="<?php echo esc_url($get_involved_link_url); ?>" target="<?php echo esc_attr($get_involved_link_target); ?>"><span><?php echo esc_html($get_involved_link_title); ?></span> <svg width="23" height="23"><use xlink:href="#arrow-yellow"></use></svg></a></li>
                  <?php endif; ?>

                  <?php endwhile; ?>
                  </ul>
                <?php endif;
              endwhile;
            endif;
          ?>
        </div>
      <?php endif;?>
    </div>
  </div>
</div>

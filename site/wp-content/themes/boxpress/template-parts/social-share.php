
<div class="sharing">
  <h5><?php _e( 'Share on:', 'boxpress' ); ?></h5>
  <ul>
    <li>
      <a class="js-social-share"
         target="_blank"
         href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode('' . the_permalink() . ''); ?>">
        <span class="vh">Facebook</span>
        <svg class="social-facebook-svg" width="50" height="50">
          <use xlink:href="#social-facebook"></use>
        </svg>
      </a>
    </li>
    <li>
      <a class="js-social-share"
         target="_blank"
         href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php echo urlencode('' . the_permalink() . ''); ?>">
        <span class="vh">Twitter</span>
        <svg class="social-twitter-svg" width="50" height="50">
          <use xlink:href="#social-twitter"></use>
        </svg>
      </a>
    </li>
    <li>
      <a class="js-social-share"
         target="_blank"
         href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('' . the_permalink() . ''); ?>&title=<?php echo urlencode( '' . get_the_title() . '' ); ?>&source=<?php echo urlencode('' . the_permalink() . ''); ?>&summary=<?php echo urlencode( '' . strip_tags( get_the_excerpt() ) . '' ); ?>">
        <span class="vh">LinkedIn</span>
        <svg class="social-linkedin-svg" width="50" height="50">
          <use xlink:href="#social-linkedin"></use>
        </svg>
      </a>
    </li>
  </ul>
</div>

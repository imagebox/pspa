<?php
/**
 * The template part for displaying results in search pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoxPress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--search' ); ?>>
  <header class="entry-header">

    <?php if ( 'team' == get_post_type() ) { ?>

      <?php
        $term_obj_list = get_the_terms( $post->ID, 'team_type' );
        $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name'));
      ?>
      <h6><?php echo $terms_string;?></h6>
      <h2 class="entry-title"><?php the_title(); ?></h2>
    <?php } else { ?>
      <h2 class="entry-title">
        <a href="<?php the_permalink(); ?>">
          <?php the_title(); ?>
        </a>
      </h2>
    <?php } ?>



    <?php if ( 'post' == get_post_type() ) : ?>
      <div class="entry-meta">
        <?php boxpress_posted_on(); ?>
      </div>
    <?php endif; ?>
  </header>

  <div class="entry-summary">
    <?php the_excerpt(); ?>

    <?php if ( 'team' == get_post_type() ) : ?>
      <?php
        $team_title = get_field('title');
        $team_email = get_field('email');
        $team_phone = get_field('phone');
        $team_mailing = get_field('mailing_address');
        $team_pdf_upload = get_field('pdf_upload_past_presidents');

        $current_user_id = get_current_user_id();
        $current_user_args = array(
          'status' => array( 'active', 'complimentary' ),
        );

        $active_memberships = wc_memberships_get_user_memberships( $current_user_id, $current_user_args );
      ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class( array('content--team-box','content--team-box-search') ); ?>>
        <div class="team-photo">
          <?php if ( has_post_thumbnail() ) : ?>
            <?php the_post_thumbnail('team_member_photo'); ?>
          <?php endif; ?>

          <?php if ( !has_post_thumbnail() ) : ?>
            <img src="<?php bloginfo('template_directory') ?>/assets/img/dist/branding/team-member-placeholder.jpg" alt="" width="100" height="133"/>
          <?php endif; ?>
        </div>

        <div class="team-content">

          <?php if ( ! empty( $team_title )) : ?>
            <p><?php echo $team_title; ?></p>
          <?php endif; ?>


          <?php if ( ! empty( $team_email )) : ?>
            <p class="email"><a href="mailto:<?php echo $team_email; ?>"><?php echo $team_email; ?></a></p>
          <?php endif; ?>


          <?php if ( ! empty( $team_pdf_upload )) : ?>
            <p><a target="_blank" href="<?php echo $team_pdf_upload['url']; ?>"><?php echo $team_pdf_upload['title']; ?></a></p>
          <?php endif; ?>

          <?php if ( ! empty( $active_memberships ) || current_user_can( 'administrator' ) ) { ?>

            <?php if ( ! empty( $team_phone )) : ?>
              <p><?php echo $team_phone; ?></p>
            <?php endif; ?>

            <?php if ( ! empty( $team_mailing )) : ?>
              <p><?php echo $team_mailing; ?></p>
            <?php endif; ?>

          <?php } ?>

        </div>

      </article>
    <?php endif; //if ( 'team' == get_post_type() ) ?>

  </div>

  <?php if ( 'team' == get_post_type() ) { ?>
    <!-- no link ? -->
  <?php } else { ?>
    <footer class="entry-footer">
      <a class="button button--arrow" href="<?php the_permalink(); ?>"><?php _e('Read More', 'boxpress'); ?></a>
    </footer>
  <?php } ?>




</article>

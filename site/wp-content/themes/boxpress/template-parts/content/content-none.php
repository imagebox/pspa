<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoxPress
 */
?>
<section class="no-results not-found">
  <header class="page-header">
    <h1 class="page-title"><?php _e( 'Nothing Found', 'boxpress' ); ?></h1>
  </header><!-- .page-header -->

  <div class="page-content">
    <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

      <p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'boxpress' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

    <?php elseif ( is_search() ) : ?>

      <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'boxpress' ); ?></p>
      <?php include( get_template_directory() . '/template-parts/search-bar.php'); ?>

    <?php else : ?>

      <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'boxpress' ); ?></p>
      <?php include( get_template_directory() . '/template-parts/search-bar.php'); ?>

    <?php endif; ?>
  </div>
</section>

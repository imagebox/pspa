<?php
  $intro_bod = get_field('intro_board_of_directors');

  if ( ! empty( $intro_bod )) : ?>
  <div class="officer-intro-text">
    <?php echo $intro_bod; ?>
  </div>
<?php endif; ?>

<div class="officer-grid">
  <?php
    $the_query = new WP_Query( array(
        'post_type' => 'team',
        'posts_per_page' => -1,
        'tax_query' => array(
          array (
            'taxonomy' => 'team_type',
            'field' => 'slug',
            'terms' => 'board-of-directors',
          )
        ),
    ) );
    while ( $the_query->have_posts() ) : $the_query->the_post();
      get_template_part( 'template-parts/content/content', 'team' );
    endwhile;
    wp_reset_postdata();
  ?>
</div>

<?php
  $intro_rr = get_field('intro_regional_representatives');

  if ( ! empty( $intro_rr )) : ?>
  <div class="officer-intro-text">
    <?php echo $intro_rr; ?>
  </div>
<?php endif; ?>
<div class="region-groups">
  <?php $team_teams = get_terms( 'regions', array('order' => 'ASC') ); ?>
  			<?php foreach ($team_teams as $team_team ) {
  				$team_team_query = new WP_Query( array(
  					'post_type' => 'team',
  					'posts_per_page' => -1,
  					'team_type' => 'regional-representatives',
  					// 'orderby' => 'name',
  					// 'order' => 'ASC',

  					'tax_query' => array(
  						array(
  							'taxonomy' => 'regions',
  							'field' => 'slug',
  							'terms' => array($team_team->slug),
  							'operator' => 'IN'
  						)
  					)
  				));
  			?>

  			<div class="region-heading">
  				<h2><?php echo $team_team->name;?></h2>
  				<p><?php echo $team_team->description;?></p>
  			</div>

  			<div class="officer-grid">

  				<?php
  				    if ( $team_team_query->have_posts() ) : while ( $team_team_query->have_posts() ) : $team_team_query->the_post();

  						get_template_part( 'template-parts/content/content', 'team' );

  				 	endwhile; endif;
  				?>
  			</div><!-- .team-grid -->

  	<?php
  		$team_team_query = null;
  		wp_reset_postdata();
  		}
  	?>

</div>

<?php
  $intro_pdeg = get_field('intro_pspa_delegates');

  if ( ! empty( $intro_pdeg )) : ?>
  <div class="officer-intro-text">
    <?php echo $intro_pdeg; ?>
  </div>
<?php endif; ?>
<div class="officer-grid">
  <?php
    $the_query = new WP_Query( array(
        'post_type' => 'team',
        'posts_per_page' => -1,
        'tax_query' => array(
          array (
            'taxonomy' => 'team_type',
            'field' => 'slug',
            'terms' => 'pspa-delegates',
          )
        ),
    ) );
    while ( $the_query->have_posts() ) : $the_query->the_post();
      get_template_part( 'template-parts/content/content', 'team' );
    endwhile;
    wp_reset_postdata();
  ?>
</div>

<div class="officer-grid">
  <?php
    $the_query = new WP_Query( array(
        'post_type' => 'team',
        'posts_per_page' => -1,
        'tax_query' => array(
          array (
            'taxonomy' => 'team_type',
            'field' => 'slug',
            'terms' => 'past-presidents',
          )
        ),
    ) );
    while ( $the_query->have_posts() ) : $the_query->the_post();
      get_template_part( 'template-parts/content/content', 'team' );
    endwhile;
    wp_reset_postdata();
  ?>
</div>

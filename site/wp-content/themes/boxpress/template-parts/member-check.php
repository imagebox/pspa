<?php
  $current_user_id = get_current_user_id();
  $current_user_args = array(
    'status' => array( 'active', 'complimentary' ),
  );

  $active_memberships = wc_memberships_get_user_memberships( $current_user_id, $current_user_args );

  if ( ! empty( $active_memberships ) ) {
    echo '<script>
      jQuery( document ).ready(function($) {
        var mem_status = "active_member";
        console.log(mem_status);
        $("input[name=input_26]").val(mem_status);
      });
    </script>';

      if (wc_memberships_is_user_active_member($current_user_id, 'prospective-pa-student')) {
        echo '<script>
          $mem_type = "prospective-pa-student";
          console.log($mem_type);
        </script>';
      }


      if (wc_memberships_is_user_active_member($current_user_id, 'student')) {
        echo '<script>
          $mem_type = "student";
          console.log($mem_type);
        </script>';
      }


      if (wc_memberships_is_user_active_member($current_user_id, 'solo-practice-physician')) {
        echo '<script>
          $mem_type = "solo-practice-physician";
          console.log($mem_type);
        </script>';
      }


      if (wc_memberships_is_user_active_member($current_user_id, 'sustaining')) {
        echo '<script>
          $mem_type = "sustaining";
          console.log($mem_type);
        </script>';
      }


      if (wc_memberships_is_user_active_member($current_user_id, 'associate')) {
        echo '<script>
          $mem_type = "associate";
          console.log($mem_type);
        </script>';
      }


      if (wc_memberships_is_user_active_member($current_user_id, 'affiliate')) {
        echo '<script>
          $mem_type = "affiliate";
          console.log($mem_type);
        </script>';
      }


      if (wc_memberships_is_user_active_member($current_user_id, 'fellow')) {
        echo '<script>
          $mem_type = "fellow";
          console.log($mem_type);
        </script>';
      }


  } else {

    echo '<script>
      jQuery( document ).ready(function($) {
        var mem_status = "inactive_member";
        console.log(mem_status);
      });
    </script>';

  }// end check for active or complimentary membership status

?>

<?php if ( ! empty( $active_memberships ) ) { ?>
  <?php // no message ?>
<?php } else { ?>
  <div class="pspa-member-notice">
    <h3><strong>Notice:</strong> Please <a href="/log-in/">login</a>, <a href="/join-or-renew/">join</a> or <a href="/join-or-renew/">renew</a> to receive your member discount.</h3>
  </div>
<?php } ?>

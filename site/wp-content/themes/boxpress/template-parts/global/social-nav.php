<?php
/**
 * Displays the Social Navigation
 *
 * @package BoxPress
 */

$group_ID = 160;
$fields = acf_get_raw_fields( $group_ID );

?>
<?php if ( $fields ) : ?>

  <h3 class="vh"><?php _e('Social', 'boxpress'); ?></h3>

  <ul class="social-nav">

    <?php foreach ( $fields as $field ) :
        $field_object = get_field_object( $field['name'], 'option' );
      ?>

      <?php if ( $field_object['value'] && ! empty( $field_object['value'] )) : ?>

        <li>
          <a href="<?php echo esc_url( $field_object['value'] ); ?>" target="_blank">
            <span class="vh"><?php echo $field_object['label']; ?></span>
            <svg class="social-<?php echo $field_object['name']; ?>-svg" width="50" height="50">
              <use xlink:href="#social-<?php echo $field_object['name']; ?>"></use>
            </svg>
          </a>
        </li>

      <?php endif; ?>
    <?php endforeach; ?>

  </ul>

<?php endif; ?>

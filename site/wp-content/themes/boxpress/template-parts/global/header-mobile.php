
<div class="site-header--mobile">
  <div class="mobile-header-left">
    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <h1 class="pspa-logo"><?php bloginfo( 'name' ); ?></h1>
      </a>
    </div>
  </div>
  <div class="mobile-header-right">
    <a href="#" class="menu-button toggle-nav">
      <span class="vh"><?php _e('Menu', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="48" height="33">
        <use xlink:href="#menu-icon"></use>
      </svg>
    </a>
  </div>
</div>

<div class="mobile-nav-tray">
  <nav class="navigation--mobile">
    <div class="mobile-nav-header">
      <a class="toggle-nav close-mobile-nav" href="#">
        <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
        <svg class="menu-icon-svg" width="38" height="39">
          <use xlink:href="#close-icon"></use>
        </svg>
      </a>
    </div>

    <?php if ( has_nav_menu( 'primary' ) ) : ?>
      <ul class="mobile-nav mobile-nav--main">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'items_wrap'      => '%3$s',
            'container'       => false,
          ));
        ?>
        <?php
          $user_logged_in = is_user_logged_in();
          $current_user_id = get_current_user_id();
          if($user_logged_in && !wc_memberships_is_user_active_member($current_user_id, 'conference-attendee')) { ?>
          <li class="members-only"><a href="/members-only/">Members Only</a></li>
        <?php } ?>
      </ul>
    <?php endif; ?>

    <?php if ( has_nav_menu( 'utility' ) ) : ?>
      <ul class="mobile-nav mobile-nav--utility">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'utility',
            'items_wrap'      => '%3$s',
            'container'       => false,
          ));
        ?>
      </ul>
    <?php endif; ?>

    <div class="header--buttons">
      <?php if ($user_logged_in && !wc_memberships_is_user_active_member($current_user_id, 'conference-attendee')) {?>
        <a class="button button--yellow" href="<?php echo wp_logout_url(get_permalink()); ?>">LOGOUT</a>
        <a class="button button--teal" href="<?php echo esc_url(home_url('/my-account')); ?>">My Account</a>
      <?} else {?>
        <a class="button button--yellow" href="<?php echo esc_url(home_url('/log-in')); ?>">Member Login</a>
        <a class="button button--teal" href="<?php echo esc_url(home_url('/join-or-renew')); ?>">Join or Renew</a>
      <?php }?>

      <?php
        $wp_cart_item_count = WC()->cart->cart_contents_count;
      ?>
      <?php if ($wp_cart_item_count > 0): ?>

        <a class="button button--yellow button--cart menu-cart-link" href="<?php echo esc_url(wc_get_cart_url()); ?>">
          <span class="vh"><?php _e('View Cart', 'boxpress');?></span>
          <svg class="cart-icon-svg" width="20" height="20">
            <use xlink:href="#cart-icon" />
          </svg>
          <?php if ($wp_cart_item_count > 0): ?>
            <span class="menu-cart-link-count"><?php
            if ($wp_cart_item_count <= 9) {
              echo esc_html($wp_cart_item_count);
            } else {
              echo '9+';
            }
            ?></span>
          <?php endif;?>
        </a>

      <?php endif;?>
    </div>

    <div class="mobile-nav--search">
      <?php include( get_template_directory() . '/template-parts/search-bar.php'); ?>
    </div>
  </nav>
</div>

<?php
/**
 * Displays the sidebar
 *
 * @package BoxPress
 */
?>
<aside class="sidebar" role="complementary">

  <?php if ( is_page() ) :
      /**
       * Query for Child Pages
       * ---
       * If we've already queries for the child
       * pages, don't query again.
       */
      if ( ! isset( $child_pages_list )) {
        $child_pages_list = query_for_child_page_list();
      }
    ?>
    <?php if ( $child_pages_list ) : ?>

      <div class="sidebar-widget">
        <nav class="sidebar-nav">
          <ul>
            <?php echo $child_pages_list; ?>
          </ul>
        </nav>
      </div>

    <?php endif; ?>
  <?php endif; ?>


  <?php // Blog Archive + Post ?>
  <?php if ( is_home() ||
             is_singular('post') ||
             ( is_archive() && get_post_type() == 'post' )) :


  ?>

  <div class="sidebar-widget">
    <nav class="sidebar-nav">
      <?php
        $ancestor_id = 5329; // id of 'News' parent page
        $descendants = get_pages( array( 'child_of' => $ancestor_id ) );
        $incl = '';

        foreach ( $descendants as $page ) {
          if ( ( $page->post_parent == $ancestor_id ) || ( $page->post_parent == $post->post_parent ) || ( $page->post_parent == $post->ID )){
            $incl .= $page->ID . ",";
          }
        }
      ?>
      <ul>
        <?php
          wp_list_pages( array(
            'child_of'    => $ancestor_id,
            'include'     => $incl,
            'link_before' => '<div class="sidebar-nav-text">',
            'link_after' => '</div>',
            'title_li'    => '',
          ) );
        ?>
      </ul>
    </nav>
  </div>

  <?php
      /**
       * Blog Category Links
       */

      $blog_cats = wp_list_categories( array(
        'title_li'  => '',
        'echo'      => false,
      ));
    ?>

    <?php if ( $blog_cats ) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Categories', 'boxpress'); ?></h4>
        <nav class="categories-widget">
          <ul>

            <?php echo $blog_cats; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>



    <?php
      /**
       * Blog Archive Links
       */

      $blog_archives = wp_get_archives( array(
        'type'            => 'monthly',
        'format'          => 'option',
        'show_post_count' => 1,
        'echo'            => false,
      ));
    ?>

    <?php if ( $blog_archives ) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Archives', 'boxpress'); ?></h4>
        <div class="archives-widget">
          <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
            <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
            <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
              <option value=""><?php echo _e( 'Select Month' ); ?></option>
              <?php echo $blog_archives; ?>
            </select>
          </form>
        </div>
      </div>

    <?php endif; ?>

    <?php
      /**
       * Blog Tag Links
       */

      $tags = get_tags( array(
        'orderby' => 'count',
        'order'   => 'DESC',
      ));
    ?>

    <?php if ( $tags ) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Tags', 'boxpress'); ?></h4>
        <div class="tags-widget">
          <ul>

            <?php foreach ( $tags as $tag ) : ?>

              <li>
                <a href="<?php echo get_tag_link( $tag->term_id ); ?>">
                  <?php echo $tag->name; ?>
                </a>
              </li>

            <?php endforeach; ?>

          </ul>
        </div>
      </div>

    <?php endif; ?>
  <?php endif; ?>

  <?php
  $posts = get_field('adverts');
  if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
      <?php setup_postdata($post); ?>

        <div class="sidebar-widget">
          <a target="_blank" href="<?php the_field('ad_link');?>">
            <?php echo get_the_post_thumbnail( $post->ID, 'full' );  ?>
          </a>
        </div>

    <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>


</aside>

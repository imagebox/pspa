<?php
/**
 * Template Name: Thad Test
 *
 * Page template for testing.
 *
 * @package BoxPress
 */
?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap">

      <?php
        $form_id = 14;
        $paging = array( 'offset' => 0, 'page_size' => 200 );
        $total_count     = 0;

        $sorting         = array( 'key' => '1.6', 'direction' => 'ASC' );

        $search_criteria = array(
          'field_filters' => array(
          'mode' => 'any',
            array(
              'key'   => '20',
              'value' => 'Yes'
            ),
            array(
              'key' => '22',
              'value' => 'Particpate Fun Run / Bike / Run + Shirt',
            )
          )
        );

        $entries = GFAPI::get_entries( $form_id, $search_criteria, $sorting, $paging, $total_count );
        $result  = GFAPI::count_entries( $form_id, $search_criteria );



      ?>

      <h2>Fun Run Participants</h2>
      <table>
        <thead>
          <tr>
            <td>
              First Name
            </td>
            <td>
              Last Name
            </td>
            <td>
              T-Shirt Size
            </td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($entries as $entry) {
            $first_name = rgar( $entry, '1.3' );
            $last_name = rgar( $entry, '1.6' );
            $shirt_size = rgar( $entry, '23');
          ?>
          <tr>
            <td><?php echo $first_name;?></td>
            <td><?php echo $last_name;?></td>
            <td><?php echo $shirt_size;?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>

      <h2>Total: <?php echo $result;?></h2>



      <?php
      // echo '<pre>';
      // print_r($entries);
      // echo '</pre>';
      ?>


      <div class="back-top back-top--article vh">
        <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
      </div>
    </div>
  </section>

<?php get_footer(); ?>

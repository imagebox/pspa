(function($) {
  'use strict';

  /**
   * Site javascripts
   */

  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="viemo.com"]:not(.not-responsive)').wrap('<div class="flexible-container"></div>');


  // Readonly gravity form fields
  $('li.gf_readonly input').each(function() {
      var $this = $(this);
      $this.attr('readonly', 'readonly');

  });


  // change '% in stock' text
  // todo: find a way to do this server-side and also work with product bundles
  $('.bundled_item_cart_details .stock.in-stock').each(function() {
      var text = $(this).text();
      text = text.replace('in stock', 'still available');
      $(this).text(text);
  });


  // replace text on subscription page when no subscriptions exist (was going to /shop/)
  $('.woocommerce_account_subscriptions .no_subscriptions').each(function() {
      var text = $(this).text();
      text = text.replace('You have no active subscriptions. Find your first subscription in the store.', 'You have no active subscriptions. Please purchase a subscription <a href="/membership/joining-pspa-register/">here</a>.');
      $(this).html(text);
  });


  // Display forgot password form toggle
  $('#forgotlink').click(function(e) {
      e.preventDefault();
      $('#forgotpassword').toggle('fast');
      $(this).toggle('fast');
  })



  var $owl_carousel = $('.js-owl-carousel');

  $owl_carousel.each(function() {
      var $this = $(this);
      var carousel_loop = false;
      var carousel_nav = false;
      var carousel_dots = false;
      var mouse_drag = false;
      var touch_drag = false;

      if ($this.find('> div').length > 1) {
          carousel_loop = true;
          carousel_nav = true;
          carousel_dots = true;
          mouse_drag = true;
          touch_drag = true;
      }

      // Carousels
      $this.owlCarousel({
          items: 1,
          mouseDrag: mouse_drag,
          touchDrag: touch_drag,
          dots: carousel_dots,
          nav: carousel_nav,
          loop: carousel_loop,
          navSpeed: 500,
          dotsSpeed: 500,
          dragEndSpeed: 500,
          autoplay: true,
          autoplayHoverPause: true,
          animateOut: 'fadeOut',
          animateIn: 'fadeIn',
          navText: [
              '<div class="button-nav-wrap"><svg class="svg-left-icon" width="14" height="23"><use xlink:href="#arrow-left-icon"></use></svg></div>',
              '<div class="button-nav-wrap"><svg class="svg-right-icon" width="14" height="23"><use xlink:href="#arrow-right-icon"></use></svg></div>',
          ]
      });
  });


  /**
   * Sponsor Carousel
   */
  var $owl_carousel = $('.sponsor-carousel');

  $owl_carousel.each(function() {
      var $this = $(this);
      var carousel_loop = false;
      var carousel_nav = false;
      var carousel_dots = false;
      var mouse_drag = false;
      var touch_drag = false;

      if ($this.find('> div').length > 1) {
          carousel_loop = true;
          carousel_nav = true;
          carousel_dots = false;
          mouse_drag = true;
          touch_drag = true;
      }

      // Carousels
      $this.owlCarousel({
          items: 5,
          mouseDrag: mouse_drag,
          touchDrag: touch_drag,
          dots: carousel_dots,
          nav: carousel_nav,
          loop: carousel_loop,
          navSpeed: 500,
          dotsSpeed: 500,
          dragEndSpeed: 500,
          autoplayHoverPause: true,
          autoplay: true,
          margin: 50,
          navContainer: '.custom-owl-nav',
          navText: [
              '<div class="button-nav-wrap"><svg class="svg-left-icon" width="14" height="23"><use xlink:href="#arrow-left-icon"></use></svg></div>',
              '<div class="button-nav-wrap"><svg class="svg-right-icon" width="14" height="23"><use xlink:href="#arrow-right-icon"></use></svg></div>',
          ],
          responsive: {
              0: {
                  items: 2,
                  nav: false,
              },
              600: {
                  items: 3,
              },
              900: {
                  items: 4,
              },
              1200: {
                  items: 5,
              }
          }

      });
  });


  /**
   * Button Arrows
   */

  $('.button--arrow, .text-button, .add_to_cart_button').each(function() {
      var $this = $(this);
      if (!$this.find('.button-arrow-icon-svg').length) {
          $this.append('<svg class="button--arrow button-arrow-icon-svg" width="7" height="12"><use xlink:href="#arrow-simple"></svg>');
      }
  });

  /**
   * Sidebar Navigation Arrows
   */

  $('.sidebar-nav ul > li.page_item_has_children > a .sidebar-nav-text').each(function() {
      var $this = $(this);
      if (!$this.find('.button-arrow-icon-svg').length) {
          $this.append('<svg class="button--arrow button-arrow-icon-svg" width="7" height="12"><use xlink:href="#arrow-simple"></svg>');
      }
  });

  /**
   * Officer Directory Tabs
   */

  $('.officer-directory-filter li').click(function() {
      var tab_id = $(this).attr('data-tab');

      $('.officer-directory-filter li').removeClass('active');
      $('.officer-directory-group').removeClass('active');

      $(this).addClass('active');
      $("#directory-" + tab_id).addClass('active');
  });

  if (window.location.href.indexOf('/officer-directory/#committee-chairpersons') > -1) {
      $('.officer-directory-filter li').removeClass('active');
      $('.officer-directory-group').removeClass('active');
      $('.officer-directory-filter li[data-tab="committee-chairpersons"]').addClass('active');
      $('.officer-directory-group#directory-committee-chairpersons').addClass('active');

      $('html, body').animate({
          scrollTop: $('#officer-directory').offset().top
      }, 500);

  }

  if (window.location.href.indexOf('/officer-directory/#liaisons') > -1) {
      $('.officer-directory-filter li').removeClass('active');
      $('.officer-directory-group').removeClass('active');
      $('.officer-directory-filter li[data-tab="liaisons"]').addClass('active');
      $('.officer-directory-group#directory-liaisons').addClass('active');

      $('html, body').animate({
          scrollTop: $('#officer-directory').offset().top
      }, 500);

  }


  if (window.location.href.indexOf('/officer-directory/#regional-representatives') > -1) {
      $('.officer-directory-filter li').removeClass('active');
      $('.officer-directory-group').removeClass('active');
      $('.officer-directory-filter li[data-tab="regional-representatives"]').addClass('active');
      $('.officer-directory-group#directory-regional-representatives').addClass('active');

      $('html, body').animate({
          scrollTop: $('#officer-directory').offset().top
      }, 500);

  }


  if (window.location.href.indexOf('/officer-directory/#pspa-delegates') > -1) {
      $('.officer-directory-filter li').removeClass('active');
      $('.officer-directory-group').removeClass('active');
      $('.officer-directory-filter li[data-tab="pspa-delegates"]').addClass('active');
      $('.officer-directory-group#directory-pspa-delegates').addClass('active');

      $('html, body').animate({ scrollTop: $('#officer-directory').offset().top - 200 }, 500);

  }


  // Student Member – Daily Tuition Rates – Conference 2019
  // Do not allow user to select all days...
  $('input[type="checkbox"].bundled_product_checkbox').on('change', function() {

      if ($('input[name="bundle_selected_optional_148"]').prop('checked') && $('input[name="bundle_selected_optional_149"]').prop('checked') && $('input[name="bundle_selected_optional_150"]').prop('checked') && $('input[name="bundle_selected_optional_151"]').prop('checked')) {
          alert('You have selected all of the possible conference days. If you wish to attend all of the possible days, please click the back button and select the option for the entire conference.');
          $("input[type='checkbox']").prop("checked", false);
      }

  });


  // Student Non-Member – Daily Tuition Rates – Conference 2019
  // Do not allow user to select all days...
  $('input[type="checkbox"].bundled_product_checkbox').on('change', function() {

      if ($('input[name="bundle_selected_optional_131"]').prop('checked') && $('input[name="bundle_selected_optional_132"]').prop('checked') && $('input[name="bundle_selected_optional_133"]').prop('checked') && $('input[name="bundle_selected_optional_134"]').prop('checked')) {
          alert('You have selected all of the possible conference days. If you wish to attend all of the possible days, please click the back button and select the option for the entire conference.');
          $("input[type='checkbox']").prop("checked", false);
      }

  });



  // Non-Member – Daily Tuition Rates – Conference 2019
  // Do not allow user to select all days...
  $('input[type="checkbox"].bundled_product_checkbox').on('change', function() {

      if ($('input[name="bundle_selected_optional_96"]').prop('checked') && $('input[name="bundle_selected_optional_97"]').prop('checked') && $('input[name="bundle_selected_optional_98"]').prop('checked') && $('input[name="bundle_selected_optional_99"]').prop('checked')) {
          alert('You have selected all of the possible conference days. If you wish to attend all of the possible days, please click the back button and select the option for the entire conference.');
          $("input[type='checkbox']").prop("checked", false);
      }

  });



  // PSPA Member – Daily Tuition Rates – Conference 2019
  // Do not allow user to select all days...
  $('input[type="checkbox"].bundled_product_checkbox').on('change', function() {

      if ($('input[name="bundle_selected_optional_38"]').prop('checked') && $('input[name="bundle_selected_optional_39"]').prop('checked') && $('input[name="bundle_selected_optional_40"]').prop('checked') && $('input[name="bundle_selected_optional_41"]').prop('checked')) {
          alert('You have selected all of the possible conference days. If you wish to attend all of the possible days, please click the back button and select the option for the entire conference.');
          $("input[type='checkbox']").prop("checked", false);
      }

  });

  window.isVirtualConference = function() {
      return jQuery('body').hasClass('postid-68229') || jQuery('body').hasClass('postid-68227');
  }

  window.isYesToVirtualFunRun = function() {
      return ("Yes" === $('input[name="input_62"]:checked').val());
  }

  // total the WC and GF options together for a correct Grand Total
  var data = $('.bundle_data').data();
  if(data) {
    setInterval(function() {
        var data = $('.bundle_data').data();
        var gform_options_price = $('#input_14_32').val();
        if(!gform_options_price) {
          gform_options_price = 0;
        }

        var total = parseFloat(data.bundle_price_data.subtotals.price) + parseFloat(gform_options_price);

        /* *** hack for virtual conference *** */
        var isVC = isVirtualConference();
        var isFunRun = isYesToVirtualFunRun();
        if(isVC && !isFunRun) {
          total = total - parseFloat(gform_options_price);
        }
        /* *** hack for virtual conference *** */
        $('.bundle_wrap .bundle_price').html('<strong>Grand Total: $' + total.toFixed(2) + '</strong>');
        if(isVC) {
            $('.bundle_price').css('display','block');
        }
    }, 500);
  }

  // uncheck bundled product box when variation form is cleared.
  $('.reset_variations').click(function() {
    $('.bundled_product_checkbox').prop('checked', false).change();
    $(this).closest('.variations_form').css('display','none');
  });


  // Hide table row for any memberships that were imported on 8/17/19
  $("table.my_account_memberships tr td:contains('August 17, 2019'),table.my_account_memberships tr td:contains('Expired')").parent().hide();

  // Hide table row for cancelled memberships
  $("table.my_account_subscriptions tr td:contains('Cancelled')").parent().hide();

  $('.product-name dl.variation dt:contains(":")').hide();


})(jQuery);

<?php
/**
 * Template Name: Prescription Card Registration
 *
 * Page template to displaying the Prescription Card Registration
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <div class="innerpage-posts">
            <form method="post" action="http://net-shapers.com/unarxcardcreator/createdrugcardtemplate-restat.php" target="_blank">

              <p>Please enter the information below to create your card.  After you submit the information you will be forwarded to our partner site.  If you do not see the page please make sure that popups are currently being blocked.</p>
              <ul>
                <li>
                  <div class="ginput_container">
                    <label for="firstName">First Name : </label>
                    <input type="text" name="firstName">
                  </div>
                </li>

                <li>
                  <div class="ginput_container">
                    <label for="lastName">Last Name : </label>
                    <input type="text" name="lastName">
                  </div>
                </li>

                <li>
                  <div class="ginput_container">
                    <label for="email">Email : </label>
                    <input type="text" name="email">
                  </div>
                </li>

                <input name="franchise" type="hidden" value="3">
                <input name="program" type="hidden" value="UNARxCard">
                <input name="rxgrp" type="hidden" value="PARXPSPA">
                <input name="fromaddress" type="hidden" value="cs@unarxcard.com">
                <input name="disableAdditionalSavings" type="hidden" value="0">
                <input name="disableReferralLogos" type="hidden" value="0">
                </ul>

                <input type="submit" class="next-button" value="Next">
              </form>
          </div>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </section>

<?php get_footer(); ?>

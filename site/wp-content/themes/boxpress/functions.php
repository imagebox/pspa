<?php
/**
 * BoxPress functions and definitions
 *
 * @package BoxPress
 */
if (!function_exists('write_log')) {
  function write_log($log)
  {
    if (is_array($log) || is_object($log)) {
      error_log(print_r($log, true));
    } else {
      error_log($log);
    }
  }
}

if (!function_exists('boxpress_setup')):
  function boxpress_setup()
{

    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails', array('post', 'page'));
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ));
    add_theme_support('woocommerce');

    // custom image sizes
    add_image_size('home_slideshow', 1600, 565, true);
    add_image_size('home_index_thumb', 860, 350, true);
    add_image_size('team_member_photo', 100, 133, true);
    add_image_size('small_square', 150, 150, true);
    add_image_size('sponsor_logo', 240, 70, false);
    add_image_size('banner_image', 600, 200, true);
    add_image_size('block_full_width', 1600, 500, true);
    add_image_size('block_half_width', 800, 500, true);
    add_image_size('newsletter_thumb', 149, 193, false);
  }
endif;
add_action('after_setup_theme', 'boxpress_setup');

/**
 * Enqueue scripts and styles.
 */

function boxpress_scripts()
{

  /**
   * Styles
   */

  // Google Fonts - Replace with your fonts URL
  $style_font_url = 'https://fonts.googleapis.com/css?family=PT+Serif|Source+Sans+Pro:400,600,700';
  wp_enqueue_style('google-fonts', $style_font_url, array(), false, 'screen');

  // Main screen styles
  $style_screen_path = get_template_directory_uri() . '/assets/css/style.min.css';
  $style_screen_ver  = filemtime(get_template_directory() . '/assets/css/style.min.css');
  wp_enqueue_style('screen', $style_screen_path, array('google-fonts'), $style_screen_ver, 'screen');

  // Main print styles
  $style_print_path = get_template_directory_uri() . '/assets/css/print.min.css';
  $style_print_ver  = filemtime(get_template_directory() . '/assets/css/print.min.css');
  wp_enqueue_style('print', $style_print_path, array('screen'), $style_print_ver, 'print');

  /**
   * Scripts
   */

  // HTML5 Shiv - Only load for <IE9
  $script_html5shiv_path = get_template_directory_uri() . '/assets/js/dev/html5shiv-printshiv.min.js';
  $script_html5shiv_ver  = filemtime(get_template_directory() . '/assets/js/dev/html5shiv-printshiv.min.js');
  wp_enqueue_script('html5shiv', $script_html5shiv_path, array(), $script_html5shiv_ver, false);
  wp_script_add_data('html5shiv', 'conditional', 'lt IE 9');

  // Modernizr
  $script_modernizr_path = get_template_directory_uri() . '/assets/js/dev/modernizr.min.js';
  $script_modernizr_ver  = filemtime(get_template_directory() . '/assets/js/dev/modernizr.min.js');
  wp_enqueue_script('modernizr', $script_modernizr_path, array(), $script_modernizr_ver, false);

  // Main site scripts
  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime(get_template_directory() . '/assets/js/build/site.min.js');
  wp_enqueue_script('site', $script_site_path, array('jquery'), $script_site_ver, true);

  // Single Posts
  if (is_singular() && comments_open() &&
    get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}
add_action('wp_enqueue_scripts', 'boxpress_scripts');

// Update CSS within in Admin
function admin_style() {
  $style_admin_path = get_template_directory_uri() . '/assets/css/admin.css';
  $style_admin_ver  = filemtime(get_template_directory() . '/assets/css/admin.css');
  wp_enqueue_style('admin-styles', $style_admin_path, false, $style_admin_ver);

}
add_action('admin_enqueue_scripts', 'admin_style');

/**
 * Query for child pages of current page
 *
 * @param  array  $options Accepts post type and depth.
 * @return string          Returns WP list of pages in html.
 */
function query_for_child_page_list($options = array())
{
  $default_options = array(
    'post_type' => 'page',
    'depth'     => 2,
  );
  $config = array_merge($default_options, $options);

  global $wp_query;
  $post = $wp_query->post;

  if ($post) {
    $parent_ID = $post->ID;

    if ($post->post_parent !== 0) {
      $ancestors = get_post_ancestors($post);
      $parent_ID = end($ancestors);
    }

    $list_pages_args = array(
      'post_type'   => $config['post_type'],
      'title_li'    => '',
      'child_of'    => $parent_ID,
      'depth'       => $config['depth'],
      'echo'        => false,
      'link_before' => '<div class="sidebar-nav-text">',
      'link_after'  => '</div>',
    );

    // Get list of pages
    return wp_list_pages($list_pages_args);
  }
}


// Remove social media, etc urls from backend user profile.
function modify_contact_methods($profile_fields) {

  // Remove old fields
  unset($profile_fields['facebook']);
  unset($profile_fields['instagram']);
  unset($profile_fields['linkedin']);
  unset($profile_fields['myspace']);
  unset($profile_fields['pinterest']);
  unset($profile_fields['soundcloud']);
  unset($profile_fields['tumblr']);
  unset($profile_fields['twitter']);
  unset($profile_fields['wikipedia']);
  unset($profile_fields['youtube']);

  return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');

/**
 * Update addresses by default for subscriptions
 */
add_filter( 'wcs_update_all_subscriptions_addresses_checked', '__return_true' );

/**
 * Limit membership to 1
 */
require get_template_directory() . '/inc/limit-memberships.php';

/**
 * Restrict purchase for specific membership subscription products
 */
//require get_template_directory() . '/inc/woo-memberships-product-purchase-limit.php';

/**
 * Conference Registration Functions
 */
//require get_template_directory() . '/inc/conference-form.php';

/**
 * Custom Post Types
 */
require get_template_directory() . '/inc/cpt/cpt-team.php';
require get_template_directory() . '/inc/cpt/cpt-ads.php';
require get_template_directory() . '/inc/cpt/cpt-newsletters.php';
require get_template_directory() . '/inc/cpt/cpt-news-briefs.php';

/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Gravity Forms // Map Submitted Field Values to Another Field
 */
require get_template_directory() . '/inc/gravity-forms-map-fields-to-field.php';

/**
 * Login setup
 */
//require get_template_directory() . '/inc/login.php';

/**
 * WooCommerce Styles/Themeing
 */
require get_template_directory() . '/inc/woo-styles.php';

/**
 * WooCommerce Behavior Modification
 */
require get_template_directory() . '/inc/woo-actions.php';

// include your composer dependencies
require_once get_template_directory() . '/vendor/autoload.php';

/**
 * Google Civic Lookup
 */
require get_template_directory() . '/inc/civic-lookup.php';

// $civicInfo = lookupCivicInfo('237 Princeton Ave Pittsburgh PA');

/**
 * Accessibility
 */
require get_template_directory() . '/inc/walkers/class-aria-walker-nav-menu.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin.php';
require get_template_directory() . '/inc/editor.php';

/**
 * Conference Report Related Functions
 */
require get_template_directory() . '/inc/conference-exports.php';

/**
 * PDF certificate and card
 */
require get_template_directory() . '/inc/pdf-generators.php';

/**
 * ACF
 */
// Blocks
require get_template_directory() . '/inc/acf-blocks.php';
// Options Pages
require get_template_directory() . '/inc/acf-options.php';
// Search Queries
require get_template_directory() . '/inc/acf-search.php';

/**
 * Plugin Settings
 */
require get_template_directory() . '/inc/plugin-settings/gravity-forms-settings.php';
require get_template_directory() . '/inc/plugin-settings/tribe-events-settings.php';


add_action('woocommerce_order_status_changed', 'send_custom_email_notifications', 10, 4 );
function send_custom_email_notifications( $order_id, $old_status, $new_status, $order ){
    if ( $new_status == 'cancelled' || $new_status == 'failed' ){
        $wc_emails = WC()->mailer()->get_emails(); // Get all WC_emails objects instances
        $customer_email = $order->get_billing_email(); // The customer email
    }

    if ( $new_status == 'cancelled' ) {
        // change the recipient of this instance
        $wc_emails['WC_Email_Cancelled_Order']->recipient = $customer_email;
        // Sending the email from this instance
        $wc_emails['WC_Email_Cancelled_Order']->trigger( $order_id );
    } 
    elseif ( $new_status == 'failed' ) {
        // change the recipient of this instance
        $wc_emails['WC_Email_Failed_Order']->recipient = $customer_email;
        // Sending the email from this instance
        $wc_emails['WC_Email_Failed_Order']->trigger( $order_id );
    } 
}

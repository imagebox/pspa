<?php
/**
 * Template Name: Regional Representatives
 *
 * Page template to displaying the regional representatives from the officer directory
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <div class="region-groups region-groups-skinny">
            <?php $team_teams = get_terms( 'regions', array('order' => 'ASC') ); ?>
            			<?php foreach ($team_teams as $team_team ) {
            				$team_team_query = new WP_Query( array(
            					'post_type' => 'team',
            					'posts_per_page' => -1,
            					'team_type' => 'regional-representatives',
            					// 'orderby' => 'name',
            					// 'order' => 'ASC',

            					'tax_query' => array(
            						array(
            							'taxonomy' => 'regions',
            							'field' => 'slug',
            							'terms' => array($team_team->slug),
            							'operator' => 'IN'
            						)
            					)
            				));
            			?>

            			<div class="region-heading">
            				<h2><?php echo $team_team->name;?></h2>
            				<p><?php echo $team_team->description;?></p>
            			</div>

            			<div class="officer-grid">

            				<?php
            				    if ( $team_team_query->have_posts() ) : while ( $team_team_query->have_posts() ) : $team_team_query->the_post();

            						get_template_part( 'template-parts/content/content', 'team' );

            				 	endwhile; endif;
            				?>
            			</div><!-- .team-grid -->

            	<?php
            		$team_team_query = null;
            		wp_reset_postdata();
            		}
            	?>

          </div>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </section>


<?php get_footer(); ?>

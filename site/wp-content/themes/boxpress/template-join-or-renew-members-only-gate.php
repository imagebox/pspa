<?php
/**
 * Template Name: Join or Renew (Members Only Gate)
 *
 * Page template for the 'Join or Renew' page.
 *
 * @package BoxPress
 */

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap">

      <div class="l-main-col page-content">

        <div class="mo-gated-intro">
          <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; ?>
        </div>

        <div class="l-join-or-renew">

          <div class="join">
            <div class="intro">
              <img src="<?php bloginfo('template_directory');?>/assets/img/dist/icons/icon-join.png" width="78" height="78" alt="">
              <h1>Become a part of PSPA.</h1>
              <p class="p-lead">Ready to enjoy all the great benefits of a PSPA membership? Sign up and start making the most out of your career, today!</p>
              <a class="button button--arrow" href="/membership/joining-pspa-register/">Join Now</a>
            </div>
            <h2>Want to learn more about membership?</h2>
            <ul>
              <li><a href="/membership/reasons-to-join-pspa/"><span>Reasons to Join</span> <svg width="23" height="23"><use xlink:href="#arrow-blue"></use></svg></a></li>
              <li><a href="/membership/benefits-of-membership/"><span>Membership Benefits</span> <svg width="23" height="23"><use xlink:href="#arrow-blue"></use></svg></a></li>
              <li><a href="/membership/membership-levels/"><span>Membership Levels</span> <svg width="23" height="23"><use xlink:href="#arrow-blue"></use></svg></a></li>
            </ul>
          </div>

          <div class="renew">
            <div class="intro">
              <img src="<?php bloginfo('template_directory');?>/assets/img/dist/icons/icon-renew.png" width="78" height="78" alt="">
              <h1>Login to your account to renew.</h1>
              <p class="p-lead">Existing Members can now renew their subscriptions by logging in to their account. Click <a href="/wp-content/uploads/2020/06/PSPA-Renewal-Instructions.pdf" target="_blank">here</a> for instructions to activate your account, reset your password, and renew your membership.</p>
              <p>Do you prefer to pay by check or have your employer pay by check? Print the paper registration form and mail or fax as directed on the form.</p>
              <a class="button" href="/wp-content/uploads/2020/06/PSPA-membership-application-2020-2021-.pdf" target="_blank">Paper Registration Form</a>
              <p style="margin-top:1em;">We hope that, whichever payment method you select, you will login to enjoy your member benefits.</p>
              <p class="p-lead">Login and renew to keep your benefits, today!</p>
            </div>
            <?php echo do_shortcode('[wppb-login]'); ?>
          </div>

        </div>


        <div class="back-top back-top--article vh">
          <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
        </div>
      </div>


    </div>
  </section>

<?php get_footer(); ?>

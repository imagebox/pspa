<?php
/**
 * Template Name: Sitemap
 *
 * Page template to displaying the sitemap.
 *
 * @package BoxPress
 */
?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap">

      <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
      <?php endwhile; ?>

      <div class="sitemap-list">
        <ul>
          <?php
            wp_list_pages(
              array(
                'exclude' => '5539,5540,5541,13918',
                'title_li' => '',
              )
            );
          ?>
        </ul>
      </div>

      <div class="back-top back-top--article vh">
        <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
      </div>

    </div>
  </section>

<?php get_footer(); ?>

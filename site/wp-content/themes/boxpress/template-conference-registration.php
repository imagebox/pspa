<?php
/**
 * Template Name: Conference Registration
 *
 * Page template for testing.
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">

          <?php
            $current_user_id = get_current_user_id();
            $current_user_args = array(
              'status' => array( 'active', 'complimentary' ),
            );

            $active_memberships = wc_memberships_get_user_memberships( $current_user_id, $current_user_args );

          ?>

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <?php get_template_part( 'template-parts/member-check' ); ?>

          <div class="conference-registration-buttons">
            <?php if ( ! empty( $active_memberships ) ) { ?>

              <h2><?php the_field('member_graduate_heading');?></h2>
              <?php if( have_rows('member_graduate_buttons') ):?>
                <div class="button-group">
                <?php while ( have_rows('member_graduate_buttons') ) : the_row(); ?>

                  <?php
                    $link = get_sub_field('button');
                    if( $link ):
                    	$link_url = $link['url'];
                    	$link_title = $link['title'];
                    	$link_target = $link['target'] ? $link['target'] : '_self';
                    	?>
                    	<a class="button button--arrow" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    <?php endif; ?>

                <?php endwhile; ?>
                </div>
              <?php endif; ?>

              <hr />

              <h2><?php the_field('member_student_heading');?></h2>

              <?php if( have_rows('member_student_buttons') ):?>
                <div class="button-group">
                <?php while ( have_rows('member_student_buttons') ) : the_row(); ?>

                  <?php
                    $link = get_sub_field('button');
                    if( $link ):
                    	$link_url = $link['url'];
                    	$link_title = $link['title'];
                    	$link_target = $link['target'] ? $link['target'] : '_self';
                    	?>
                    	<a class="button button--arrow" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    <?php endif; ?>

                <?php endwhile; ?>
                </div>
              <?php endif; ?>

            <?php } else { ?>

              <h2><?php the_field('non_members_graduate_heading');?></h2>

              <?php if( have_rows('non_members_graduate_buttons') ):?>
                <div class="button-group">
                <?php while ( have_rows('non_members_graduate_buttons') ) : the_row(); ?>

                  <?php
                    $link = get_sub_field('button');
                    if( $link ):
                    	$link_url = $link['url'];
                    	$link_title = $link['title'];
                    	$link_target = $link['target'] ? $link['target'] : '_self';
                    	?>
                    	<a class="button button--arrow" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    <?php endif; ?>

                <?php endwhile; ?>
                </div>
              <?php endif; ?>

              <hr />

              <h2><?php the_field('non_members_student_heading');?></h2>

              <?php if( have_rows('non_member_student_buttons') ):?>
                <div class="button-group">
                <?php while ( have_rows('non_member_student_buttons') ) : the_row(); ?>

                  <?php
                    $link = get_sub_field('button');
                    if( $link ):
                    	$link_url = $link['url'];
                    	$link_title = $link['title'];
                    	$link_target = $link['target'] ? $link['target'] : '_self';
                    	?>
                    	<a class="button button--arrow" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    <?php endif; ?>

                <?php endwhile; ?>
                </div>
              <?php endif; ?>

            <?php } ?>
          </div>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </section>

<?php get_footer(); ?>

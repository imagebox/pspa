<?php
/**
 * Template Name: News Briefs
 *
 * Page template to displaying the News Briefs
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <div class="page-content">
            <ul class="newsletter-posts">
              <?php
                $the_query = new WP_Query( array(
                    'post_type' => 'news_brief',
                    'posts_per_page' => -1,
                ) );
                while ( $the_query->have_posts() ) : $the_query->the_post();
              ?>

              <?php
                $newsletter_file = get_field('news_brief');
                if( $newsletter_file ):
              ?>
                  <li><a href="<?php echo $newsletter_file['url']; ?>" target="_blank"><?php the_title();?></a></li>
              <?php endif; ?>

              <?php
                endwhile;
                wp_reset_postdata();
              ?>
            </ul>
          </div>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </section>

<?php get_footer(); ?>

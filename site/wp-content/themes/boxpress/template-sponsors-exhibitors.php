<?php
/**
 * Template Name: Sponsorships and Exhibiting
 *
 * Page template for the 'Sponsorships and Exhibiting' page.
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header();?>

  <?php require_once 'template-parts/banners/banner--page.php';?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if (!$child_pages_list) {echo 'wrap--limited';}?>">

      <div class="<?php if ($child_pages_list) {echo 'l-sidebar';}?>">
        <div class="l-main-col">
          <?php //if (is_user_logged_in()): ?>

            <?php while (have_posts()): the_post();?>
              <?php get_template_part('template-parts/content/content', 'page');?>
            <?php endwhile;?>

            <?php if( have_rows('membership_type') ): ?>
              <div class="membership-options">
                <?php while ( have_rows('membership_type') ) : the_row(); ?>
                  <div class="member-row">
                    <h2><?php the_sub_field('heading'); ?></h2>
                    <?php if( have_rows('membership_option') ): ?>
                      <div class="member-grid">
                        <?php while ( have_rows('membership_option') ) : the_row(); ?>
                          <div class="member-option">
                            <h3><?php the_sub_field('title'); ?></h3>
                            <h4><?php the_sub_field('cost'); ?></h4>
                            <a href="<?php the_sub_field('link'); ?>" class="button button--arrow">Buy Now</a>
                          </div>
                        <?php endwhile; ?>
                      </div>
                    <?php else : endif; ?>
                  </div><!--.member-row-->
                <?php endwhile; ?>
              </div>
            <?php else : endif; ?>


            <div class="back-top back-top--article vh">
              <a href="#main"><?php _e('Back to Top', 'boxpress');?></a>
            </div>
          </div>

          <?php if ($child_pages_list): ?>
            <div class="l-aside-col">
              <?php get_sidebar();?>
            </div>
          <?php endif;?>
        <?php //else: ?>

        <?php //endif;?>
      </div>

    </div>
  </section>

<?php get_footer();?>

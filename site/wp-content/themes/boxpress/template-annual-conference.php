<?php
/**
 * Template Name: Annual Conference Rates & Registration
 *
 * Page template for the annual conference page.
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();
$display_conference_registration = get_field('toggle_conference_registration','option') == true;
$conference_registration_inactive_message = get_field('conference_registration_inactive_message', 'option');
?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">

          <?php if( $display_conference_registration ): ?>

            <?php while ( have_posts() ) : the_post(); ?>
              <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
            <?php endwhile; ?>

          <?php endif; ?>

          <?php if( ! $display_conference_registration ): ?>

            <div class="page-content">
              <?php echo $conference_registration_inactive_message; ?>
            </div>

          <?php endif; ?>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </section>

<?php get_footer(); ?>

<?php
/**
 * Template header
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 *
 * @package BoxPress
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes();?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php wp_head();?>
  <?php the_field('header_tracking_codes', 'option'); // Header Tracking Codes ?>
  <style>
  .order-again {display:none;}
  </style>
</head>
<body <?php body_class();?>>

<?php if (is_front_page()) {?>
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3"></script>
<?php }?>

<?php
include get_template_directory() . '/template-parts/global/svg.php';
include get_template_directory() . '/template-parts/global/header-mobile.php';
$user_logged_in  = is_user_logged_in();
$current_user_id = get_current_user_id();
?>

<div class="site-wrap">
<header id="masthead" class="site-header" role="banner">
  <div class="wrap">

    <div class="site-header-inner">
      <div class="header-col-1">

        <div class="site-branding">
          <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
            <h1>
              <div class="pspa-logo"></div>
              <span><?php bloginfo('name');?></span>
            </h1>
          </a>
        </div>

      </div>
      <div class="header-col-2">

        <div class="header--buttons">
          <?php include get_template_directory() . '/template-parts/search-bar.php';?>
          <?php if ($user_logged_in && !wc_memberships_is_user_active_member($current_user_id, 'conference-attendee')) {?>
            <a class="button button--yellow" href="<?php echo wp_logout_url(get_permalink()); ?>">LOGOUT</a>
            <a class="button button--blue" href="<?php echo esc_url(home_url('/my-account')); ?>">My Account</a>
          <?} else {?>
            <a class="button button--yellow" href="<?php echo esc_url(home_url('/log-in')); ?>">Member Login</a>
            <a class="button button--teal" href="<?php echo esc_url(home_url('/join-or-renew')); ?>">Join or Renew</a>
          <?php }?>

          <?php
            $wp_cart_item_count = WC()->cart->cart_contents_count;
          ?>
          <?php if ($wp_cart_item_count > 0): ?>

            <a class="button button--teal button--cart menu-cart-link" href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>">
              <span class="vh"><?php _e('View Cart', 'boxpress');?></span>
              <svg class="cart-icon-svg" width="20" height="20">
                <use xlink:href="#cart-icon" />
              </svg>
              <?php if ($wp_cart_item_count > 0): ?>
                <span class="menu-cart-link-count"><?php
                if ($wp_cart_item_count <= 9) {
                  echo esc_html($wp_cart_item_count);
                } else {
                  echo '9+';
                }
                ?></span>
              <?php endif;?>
            </a>

          <?php endif;?>
        </div>

        <?php if (has_nav_menu('utility')): ?>
          <nav class="js-accessible-menu navigation--utility"
            aria-label="<?php _e('Utility Navigation', 'boxpress');?>"
            role="navigation">
            <ul class="nav-list">
              <?php
wp_nav_menu(array(
  'theme_location' => 'utility',
  'items_wrap'     => '%3$s',
  'container'      => false,
  'walker'         => new Aria_Walker_Nav_Menu(),
));
?>
            </ul>
          </nav>
        <?php endif;?>

      </div>
    </div>
  </div>
  <?php if (has_nav_menu('primary')): ?>
  <div class="header-main-navigation">
    <div class="wrap">
      <nav class="js-accessible-menu navigation--main"
        aria-label="<?php _e('Main Navigation', 'boxpress');?>"
        role="navigation">
        <ul class="nav-list">
          <?php
wp_nav_menu(array(
  'theme_location' => 'primary',
  'items_wrap'     => '%3$s',
  'container'      => false,
  'walker'         => new Aria_Walker_Nav_Menu(),
));
?>
          <?php if ($user_logged_in && !wc_memberships_is_user_active_member($current_user_id, 'conference-attendee')) {?>
            <li class="members-only"><a href="/members-only/">Members Only</a></li>
          <?php }?>
        </ul>
      </nav>
    </div>
  </div>
  <?php endif;?>
</header>

<main id="main" class="site-main" role="main">

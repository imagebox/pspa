<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package BoxPress
 */
?>
</main>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="primary-footer">
    <div class="wrap">

      <div class="footer-col-1">
        <?php if ( has_nav_menu( 'footer' )) : ?>
          <nav class="js-accessible-menu navigation--footer"
            aria-label="<?php _e( 'Footer Navigation', 'boxpress' ); ?>"
            role="navigation">
            <ul class="nav-list">
              <?php
                wp_nav_menu( array(
                  'theme_location'  => 'footer',
                  'items_wrap'      => '%3$s',
                  'container'       => false,
                  'walker'          => new Aria_Walker_Nav_Menu(),
                ));
              ?>
            </ul>
          </nav>
        <?php endif; ?>
      </div>

      <div class="footer-col-2">
        <div class="footer-social">
          <?php get_template_part( 'template-parts/global/social-nav' ); ?>
        </div>
      </div>

    </div>
  </div>
  <div class="site-info">
    <div class="wrap">
      <div class="site-copyright">
        <p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></p>
      </div>
      <div class="imagebox">
        <p><?php _e('Website by', 'boxpress'); ?> <a href="https://imagebox.com" target="_blank"><span class="vh">Imagebox</span><svg width="81" height="23"><use xlink:href="#imagebox-logo"></use></svg></a></p>
      </div>
    </div>
  </div>
</footer>
</div>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>

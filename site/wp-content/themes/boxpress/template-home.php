<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package BoxPress
 */
get_header(); ?>

<article class="homepage">
  <?php
    get_template_part( 'template-parts/homepage/masthead-carousel' );
    get_template_part( 'template-parts/homepage/notice-banner' );
    get_template_part( 'template-parts/homepage/member-callout' );
    get_template_part( 'template-parts/homepage/get-involved' );
    get_template_part( 'template-parts/homepage/career-center' );
    get_template_part( 'template-parts/homepage/for-pa-students' );
    get_template_part( 'template-parts/homepage/ad-space-1' );
    get_template_part( 'template-parts/homepage/latest' );
    get_template_part( 'template-parts/homepage/sponsors-carousel' );
    get_template_part( 'template-parts/homepage/ad-space-2' );
  ?>
</article>

<?php get_footer(); ?>

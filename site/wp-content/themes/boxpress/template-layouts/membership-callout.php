<?php
/**
 * Displays the Button Block layout
 *
 * @package BoxPress
 */

$mc_block_heading = get_sub_field( 'heading' );
$button_block_bkg   = get_sub_field( 'background' );

?>
<section class="button-block-layout section <?php echo $button_block_bkg; ?>">
  <div class="wrap">
    <div class="button-block">

      <?php if ( ! empty( $mc_block_heading )) : ?>

        <div class="callout-header">
          <h2><?php echo $mc_block_heading; ?></h2>
        </div>

      <?php endif; ?>

      <div class="callout-body">
        <div class="button-callout-content membership-callouts">

          <a href="/product-category/memberships/" class="button button--arrow">Join Now</a>
          <a href="/join-or-renew/" class="button button--arrow">Renew Membership</a>

        </div>
      </div>
    </div>
  </div>
</section>

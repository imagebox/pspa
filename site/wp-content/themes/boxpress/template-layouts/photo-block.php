<?php
/**
 *
 * @package BoxPress
 */

$photo_image  = get_sub_field( 'photo_block_image' );
$is_parallax  = get_sub_field( 'enable_parallax' );
$photo_image_size = 'block_full_width';
$photo_heading = get_sub_field('photo_block_heading');
$photo_copy = get_sub_field('photo_block_copy');
$photo_button = get_sub_field('photo_block_button');

?>
<?php if ( $photo_image ) : ?>

  <section class="photo-block-layout <?php
      if ( $is_parallax ) {
        echo 'photo-block-layout--parallax';
      }
    ?>" style="background-image:url('<?php echo esc_url( $photo_image['sizes'][ $photo_image_size ] ); ?>');">

    <?php if ($photo_heading || $photo_copy || $photo_button) : ?>

      <div class="photo-block-layout__inner">
        <?php if ( $photo_heading ) : ?>
          <h1><?php echo $photo_heading;?></h1>
        <?php endif;?>

        <?php if ( $photo_copy ) : ?>
          <p><strong><?php echo $photo_copy;?></strong></p>
        <?php endif;?>

        <?php
        if( $photo_button ):
        	$photo_button_url = $photo_button['url'];
        	$photo_button_title = $photo_button['title'];
        	$photo_button_target = $photo_button['target'] ? $photo_button['target'] : '_self';
        	?>
        	<a class="button button--arrow" href="<?php echo esc_url($photo_button_url); ?>" target="<?php echo esc_attr($photo_button_target); ?>"><?php echo esc_html($photo_button_title); ?></a>
        <?php endif; ?>


      </div>

    <?php endif; ?>

  </section>

<?php endif; ?>

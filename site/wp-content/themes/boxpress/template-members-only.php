<?php
/**
 * Template Name: Members Only
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap">

      <?php while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class( 'content--page' ); ?>>
          <div class="page-content">
            <?php the_content(); ?>

            <?php
              // $user_id = get_current_user_id();
              // $key = 'MBPSPANumber';
              // $single = true;
              // $pspa_num = get_user_meta( $user_id, $key, $single );
              // echo '<p><strong>Your PSPA Number:</strong> ' . $pspa_num . '</p>';
            ?>

            <div class="account-masthead">
              <div class="account-intro">
                <div class="account-intro-left">
                  <h2>
                    <svg class="icon" width="56" height="56">
                      <use xlink:href="#member-simple"></use>
                    </svg>
                    <span>My Account</span>
                  </h2>
                </div>
                <div class="account-intro-right">
                  <ul>
                    <li><a href="/my-account/"><span>Dashboard</span><svg class="icon" width="23" height="23"><use xlink:href="#arrow-teal"></use></svg></a></li>
                    <li><a href="/my-account/edit-account/"><span>Edit Profile</span><svg class="icon" width="23" height="23"><use xlink:href="#arrow-teal"></use></svg></a></li>
                    <li><a href="/my-account/"><span>Manage/Renew Membership</span><svg class="icon" width="23" height="23"><use xlink:href="#arrow-teal"></use></svg></a></li>
                  </ul>
                </div>
              </div>


              <?php
              $event_callout = get_field('event_callout');
              if( $event_callout ): ?>
                <div class="registration-callout">
                  <a href="<?php echo esc_url( $event_callout['link']['url'] ); ?>" target="<?php echo esc_url( $event_callout['link']['target'] ); ?>">
                    <svg class="icon" width="56" height="56">
                      <use xlink:href="#icon-event"></use>
                    </svg>
                    <h2><?php echo $event_callout['heading']; ?></h2>
                    <p><span>Learn More</span> <svg class="icon" width="23" height="23"><use xlink:href="#arrow-blue"></use></svg></p>
                  </a>
                </div>
              <?php endif; ?>


            </div>

          </div>
        </article>
      <?php endwhile; ?>


        <?php if( have_rows('callouts') ): ?>
          <div class="members-only-callouts">
            <ul>
              <?php while ( have_rows('callouts') ) : the_row(); ?>
                <li>
                  <?php
                    $link = get_sub_field('link');
                    if( $link ):
                    	$link_url = $link['url'];
                    	$link_title = $link['title'];
                    	$link_target = $link['target'] ? $link['target'] : '_self';
                    	?>
                    	<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                        <img src="<?php the_sub_field('icon'); ?>" alt="" width="40" height="40">
                        <h6><?php echo esc_html($link_title); ?></h6>
                      </a>
                    <?php endif; ?>

                </li>
              <?php endwhile; ?>
            </ul>
          </div>
          <?php else : ?>
        <?php endif; ?>


        <div class="back-top back-top--article vh">
          <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
        </div>

    </div>
  </section>

<?php get_footer(); ?>

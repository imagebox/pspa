<?php
/**
 * Template Name: Member Test
 *
 * Page template for testing.
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">


          <?php
            $current_user_id = get_current_user_id();
            $current_user_args = array(
              'status' => array( 'active', 'complimentary' ),
            );

            $active_memberships = wc_memberships_get_user_memberships( $current_user_id, $current_user_args );

            if ( ! empty( $active_memberships ) ) {
              echo '<h3><strong>Status:</strong> Active</h3>';
              echo '<script>
                $mem_status = "active";
                console.log($mem_status);
              </script>';

                if (wc_memberships_is_user_active_member($current_user_id, 'prospective-pa-student')) {
                  echo '<p><strong>Type:</strong> Prospective PA Student</p>';
                  echo '<script>
                    $mem_type = "prospective-pa-student";
                    console.log($mem_type);
                  </script>';
                }


                if (wc_memberships_is_user_active_member($current_user_id, 'student')) {
                  echo '<p><strong>Type:</strong> Student</p>';
                  echo '<script>
                    $mem_type = "student";
                    console.log($mem_type);
                  </script>';
                }


                if (wc_memberships_is_user_active_member($current_user_id, 'solo-practice-physician')) {
                  echo '<p><strong>Type:</strong> Solo Practice Physician</p>';
                  echo '<script>
                    $mem_type = "solo-practice-physician";
                    console.log($mem_type);
                  </script>';
                }


                if (wc_memberships_is_user_active_member($current_user_id, 'sustaining')) {
                  echo '<p><strong>Type:</strong> Sustaining</p>';
                  echo '<script>
                    $mem_type = "sustaining";
                    console.log($mem_type);
                  </script>';
                }


                if (wc_memberships_is_user_active_member($current_user_id, 'associate')) {
                  echo '<p><strong>Type:</strong> Associate</p>';
                  echo '<script>
                    $mem_type = "associate";
                    console.log($mem_type);
                  </script>';
                }


                if (wc_memberships_is_user_active_member($current_user_id, 'affiliate')) {
                  echo '<p><strong>Type:</strong> Affiliate</p>';
                  echo '<script>
                    $mem_type = "affiliate";
                    console.log($mem_type);
                  </script>';
                }


                if (wc_memberships_is_user_active_member($current_user_id, 'fellow')) {
                  echo '<p><strong>Type:</strong> Fellow</p>';
                  echo '<script>
                    $mem_type = "fellow";
                    console.log($mem_type);
                  </script>';
                }




            } else {

              echo '<p><strong>Status:</strong> Inactive</p>';
              echo '<script>
                $mem_status = "inactive";
                console.log($mem_status);
              </script>';

            }// end check for active or complimentary membership status


          ?>


          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </section>

<?php get_footer(); ?>

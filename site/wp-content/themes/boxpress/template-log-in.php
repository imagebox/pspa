<?php
/**
 * Template Name: Log In
 *
 * Page template for the 'Log In' page.
 *
 * @package BoxPress
 */

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap">

      <div class="l-main-col page-content">

        <div class="pspa-login-form">
          <?php
            if ( ! is_user_logged_in() ) { // Display WordPress login form:
              echo '<h2>Login</h2>';
              $login_args = array(
                'echo'           => true,
                'remember'       => true,
                'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
                'form_id'        => 'loginform',
                'id_username'    => 'user_login',
                'id_password'    => 'user_pass',
                'id_remember'    => 'rememberme',
                'id_submit'      => 'wp-submit',
                'label_username' => __( 'Email Address' ),
                'label_password' => __( 'Password' ),
                'label_remember' => __( 'Remember Me' ),
                'label_log_in'   => __( 'Log In' ),
                'value_username' => '',
                'value_remember' => false
              );

              wp_login_form($login_args);

            ?>

            <p><a href="/my-account/lost-password/" class="button">Forgot password?</a></p>


            <?php } else { // If logged in:
                wp_loginout( home_url() ); // Display "Log Out" link.

            }
            ?>
        </div>

        <div class="back-top back-top--article vh">
          <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
        </div>
      </div>


    </div>
  </section>

<?php get_footer(); ?>

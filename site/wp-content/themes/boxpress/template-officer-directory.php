<?php
/**
 * Template Name: Officer Directory
 *
 * Page template to displaying the officer directory
 *
 * @package BoxPress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </section>

  <section class="section l-officer-directory">

    <div class="officer-directory-filter" id="officer-directory">
      <div class="wrap">
        <h6>Sort by</h6>
        <ul>
          <li class="active" data-tab="board-of-directors">Board of Directors</li>
          <li data-tab="committee-chairpersons">Committee Chairpersons</li>
          <li data-tab="liaisons">Liaisons</li>
          <li data-tab="regional-representatives">Regional Representatives</li>
          <li data-tab="pspa-delegates">PSPA Delegates</li>
        </ul>
      </div>
    </div>

    <div class="officer-directory-content">
      <div class="wrap">

        <div class="officer-directory-group active" id="directory-board-of-directors">
          <?php get_template_part( 'template-parts/team/board-of-directors' ); ?>
        </div>

        <div class="officer-directory-group" id="directory-committee-chairpersons">
          <?php get_template_part( 'template-parts/team/committee-chairpersons' ); ?>
        </div>

        <div class="officer-directory-group" id="directory-liaisons">
          <?php get_template_part( 'template-parts/team/liaisons' ); ?>
        </div>

        <div class="officer-directory-group" id="directory-regional-representatives">
          <?php get_template_part( 'template-parts/team/regional-representatives' ); ?>
        </div>

        <div class="officer-directory-group" id="directory-pspa-delegates">
          <?php get_template_part( 'template-parts/team/pspa-delegates' ); ?>
        </div>

      </div>
    </div>

  </section>

<?php get_footer(); ?>

<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Display -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.23
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>

<header class="banner">
  <div class="wrap">
    <img class="banner-image" draggable="false" src="/wp-content/uploads/2019/06/pspa-banner3-599x200.png" alt="">
		<div class="banner-title">
      <span class="h1">Events &amp; CME</span>
    </div>
	</div>
</header>

<section class="fullwidth-column section">
  <div class="wrap">

    <div class="l-sidebar">

      <div class="l-main-col">
        <main id="tribe-events-pg-template" class="tribe-events-pg-template">
        	<?php tribe_events_before_html(); ?>
        	<?php tribe_get_view(); ?>
        	<?php tribe_events_after_html(); ?>
        </main> <!-- #tribe-events-pg-template -->

        <div class="back-top back-top--article vh">
          <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
        </div>
      </div>

      <div class="l-aside-col">
        <aside class="sidebar" role="complementary">
          <div class="sidebar-widget">
            <nav class="sidebar-nav">
              <?php
                $ancestor_id = 5301; // id of 'Events & CME' fake parent page
                $descendants = get_pages( array( 'child_of' => $ancestor_id ) );
                $incl = '';

                foreach ( $descendants as $page ) {
                  if ( ( $page->post_parent == $ancestor_id ) || ( $page->post_parent == $post->post_parent ) || ( $page->post_parent == $post->ID )){
                    $incl .= $page->ID . ",";
                  }
                }
              ?>
              <ul>
                <?php
                  wp_list_pages( array(
                    'child_of'    => $ancestor_id,
                    'include'     => $incl,
                    'link_before' => '<div class="sidebar-nav-text">',
                    'link_after' => '</div>',
                    'title_li'    => '',
                  ) );
                ?>
              </ul>
            </nav>
          </div>
        </aside>
      </div>

    </div>

  </div>
</section>

<?php
get_footer();

<?php
/**
 * Template Name: Map Dynamics
 *
 * Page template to display the advanced page builder.
 *
 * @package BoxPress
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap">

      <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
      <?php endwhile; ?>

    </div>
  </section>

  <?php
    $map_dynamics_embed = get_field('embed_code');
  ?>

  <?php if( $map_dynamics_embed ): ?>
  <div class="map-dynamics-container">
    <?php echo $map_dynamics_embed; ?>
  </div>
  <?php endif; ?>

  <div class="back-top back-top--article vh">
    <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
  </div>

<?php get_footer(); ?>

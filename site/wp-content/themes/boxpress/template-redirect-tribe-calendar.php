<?php
/**
 * Template Name: Redirect to Tribe Calendar
 *
 * Page template to redirect the page to events calendar archive.
 *
 * @package BoxPress
 */

wp_redirect('/events/', 301 );
exit;


?>

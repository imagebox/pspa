<?php
/**
 * Template Name: Page Redirect
 *
 * Page template to redirect the page to what is specified in the content editor.
 *
 * @package BoxPress
 */

if (function_exists('have_posts') && have_posts()){
	while (have_posts()){

		// get the post
			the_post();

		// get content
			ob_start();
			the_content();
			$contents	= ob_get_contents();
			ob_end_clean();

		// correctly build the link

			// grab the 'naked' link
				$link	= strip_tags($contents);
				$link	= preg_replace('/\s/', '', $link);

			// work out
				if(!preg_match('%^http://%', $link)){
					$host	= $_SERVER['HTTP_HOST'];
					$dir	= dirname($_SERVER['PHP_SELF']);
					$link	= "http://$host$dir/$link";
					}

			// do the link
				header("Location: $link");
				die('');
				}

	}
?>

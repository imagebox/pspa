<?php
/**
 * WooCommerce - Disable General Stylesheet
 */
add_filter('woocommerce_enqueue_styles', 'boxpress_woo_dequeue_styles');
function boxpress_woo_dequeue_styles($enqueue_styles)
{
  unset($enqueue_styles['woocommerce-general']); // Remove the gloss
  //unset( $enqueue_styles['woocommerce-layout'] );   // Remove the layout
  //unset( $enqueue_styles['woocommerce-smallscreen'] );  // Remove the smallscreen optimisation
  return $enqueue_styles;
}


// Unhook WC Wrappers
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'boxpress_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'boxpress_wrapper_end', 10);

function boxpress_wrapper_start()
{
  echo '<header class="banner">
    <div class="wrap">
      <img class="banner-image" draggable="false" src="/wp-content/uploads/2019/06/pspa-banner-7.png" alt="">
      <div class="banner-title">
        <span class="h1">
          Shop
        </span>
      </div>
    </div>
  </header>';
  echo '<section class="section">';
  echo '<div class="wrap">';
  echo '<div class="page-content">';
}

function boxpress_wrapper_end()
{
  echo '</div>';
  echo '</div>';
  echo '</section>';
}

// Removes links back to product on cart page
add_filter('woocommerce_cart_item_permalink', '__return_null');

// Removes 'Downloads' link from 'My Account'
add_filter('woocommerce_account_menu_items', 'imagebox_remove_downloads_my_account', 999);
function imagebox_remove_downloads_my_account($items)
{
  $items['downloads'] = 'Documents';
  return $items;
}

function wpb_woo_endpoint_title($title, $id)
{
  if (is_wc_endpoint_url('downloads') && in_the_loop()) { // add your endpoint urls
    $title = "Documents"; // change your entry-title
  }
  return $title;
}
add_filter('the_title', 'wpb_woo_endpoint_title', 10, 2);

//Change the Billing Details checkout label to Your Details
function wc_billing_field_strings($translated_text, $text, $domain)
{
  switch ($translated_text) {
    case 'Billing details':
      $translated_text = __('Credit Card Billing Details', 'woocommerce');
      break;
  }
  return $translated_text;
}
add_filter('gettext', 'wc_billing_field_strings', 20, 3);

// My Subscription - Change to 'Manage/Renew Subscription'
function wc_my_subscription_strings($translated_text, $text, $domain)
{
  switch ($translated_text) {
    case 'My Subscription':
      $translated_text = __('Manage/Renew Subscription', 'woocommerce');
      break;
  }
  return $translated_text;
}
add_filter('gettext', 'wc_my_subscription_strings', 20, 3);

// Profile builder pro - email validation change.
function pbp_email_valid_strings($translated_text, $text, $domain)
{
  switch ($translated_text) {
    case 'Please try a different one!':
      $translated_text = __('Please <a href="/log-in">login</a> to your account. Need to reset your password? <a href="/my-account/lost-password/">Click here</a>.', 'profile-builder');
      break;
  }
  return $translated_text;
}
add_filter('gettext', 'pbp_email_valid_strings', 20, 3);

// Account details - Change to 'Edit Profile' (sidebar link)
function wc_edit_profile_strings($translated_text, $text, $domain)
{
  switch ($translated_text) {
    case 'Account details':
      $translated_text = __('Edit Profile', 'woocommerce');
      break;
  }
  return $translated_text;
}
add_filter('gettext', 'wc_edit_profile_strings', 20, 3);

// Change 'resubscribe' button to renew now
function wc_resubscribe_strings($translated_text, $text, $domain)
{
  switch ($translated_text) {
    case 'Resubscribe':
      $translated_text = __('Renew Now', 'woocommerce');
      break;
  }
  return $translated_text;
}
add_filter('gettext', 'wc_resubscribe_strings', 20, 3);

//Change the Billing Details checkout label to Your Details
function wc_billing_address_field_strings($translated_text, $text, $domain)
{
  switch ($translated_text) {
    case 'Billing address':
      $translated_text = __('Credit Card Billing Details', 'woocommerce');
      break;
  }
  return $translated_text;
}
add_filter('gettext', 'wc_billing_address_field_strings', 20, 3);

// Add text next to 'sale' price indicating when sale ends
add_filter('woocommerce_get_price_html', 'custom_price_html', 100, 2);
function custom_price_html($price, $product)
{

  $sales_price_from = get_post_meta($product->get_id(), '_sale_price_dates_from', true);
  $sales_price_to   = get_post_meta($product->get_id(), '_sale_price_dates_to', true);

  if (is_single() && $product->is_on_sale() && $sales_price_to != "") {

    $sales_price_date_from = date("m/d/y", $sales_price_from);
    $sales_price_date_to   = date("m/d/y", $sales_price_to);

    $price = str_replace('</ins>', ' </ins> <b>(Discounted price available until ' . $sales_price_date_to . ')</b>', $price);
  }

  return apply_filters('woocommerce_get_price', $price);
}

// Remove related products
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

// Change "Ship to a different addres?" text on checkout
function shipchange($translated_text, $text, $domain)
{
  switch ($translated_text) {
    case 'Ship to a different address?':
      $translated_text = __('Work Address different from the Home Address?', 'woocommerce');
      break;
  }
  return $translated_text;
}
add_filter('gettext', 'shipchange', 20, 3);

// Ship to a different address opened by default
add_filter('woocommerce_ship_to_different_address_checked', '__return_true');

add_filter('woocommerce_account_menu_items', 'misha_one_more_link');
function misha_one_more_link($menu_links)
{

  // we will hook "anyuniquetext123" later
  $new = array('anyuniquetext123' => 'Members Only');

  // or in case you need 2 links
  // $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );

  // array_slice() is good when you want to add an element between the other ones
  $menu_links = array_slice($menu_links, 0, 1, true)
   + $new
   + array_slice($menu_links, 1, null, true);

  return $menu_links;

}

add_filter('woocommerce_get_endpoint_url', 'misha_hook_endpoint', 10, 4);
function misha_hook_endpoint($url, $endpoint, $value, $permalink)
{

  if ($endpoint === 'anyuniquetext123') {
    // ok, here is the place for your custom URL, it could be external
    $url = site_url('/members-only/');
  }

  return $url;

}


// allow compelted orders to be editable
add_filter( 'wc_order_is_editable', 'wc_make_processing_orders_editable', 10, 2 );
function wc_make_processing_orders_editable( $is_editable, $order ) {
    if ( $order->get_status() == 'completed' ) {
        $is_editable = true;
    }

    return $is_editable;
}

<?php
/**
 * Ads Custom Post Type
 *
 * @package BoxPress
 */

function cpt_adverts() {

  $label_plural   = 'Ads';
  $label_singular = 'Ad';

  $labels = array(
    'name'                  => _x( "{$label_plural}", 'Post Type General Name', 'boxpress' ),
    'singular_name'         => _x( "{$label_singular}", 'Post Type Singular Name', 'boxpress' ),
    'menu_name'             => __( "{$label_plural}", 'boxpress' ),
    'name_admin_bar'        => __( "{$label_singular}", 'boxpress' ),
    'parent_item_colon'     => __( "Parent {$label_singular}:", 'boxpress' ),
    'all_items'             => __( "All {$label_plural}", 'boxpress' ),
    'add_new_item'          => __( "Add New {$label_singular}", 'boxpress' ),
    'add_new'               => __( "Add New", 'boxpress' ),
    'new_item'              => __( "New {$label_singular}", 'boxpress' ),
    'edit_item'             => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'           => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'             => __( "View {$label_singular}", 'boxpress' ),
    'search_items'          => __( "Search {$label_singular}", 'boxpress' ),
    'not_found'             => __( "Not found", 'boxpress' ),
    'not_found_in_trash'    => __( "Not found in Trash", 'boxpress' ),
    'items_list'            => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation' => __( "{$label_plural} list navigation", 'boxpress' ),
    'filter_items_list'     => __( "Filter {$label_plural} list", 'boxpress' ),
  );

  $args = array(
    'label'                 => __( "{$label_plural}", 'boxpress' ),
    'description'           => __( "{$label_singular} Custom Post Type", 'boxpress' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'thumbnail', 'revisions', 'custom-fields' ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => false,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => false,
    'capability_type'       => 'post',
    'menu_icon'             => 'dashicons-analytics',
    'rewrite'               => false,
  );

  register_post_type( 'adverts', $args );
}
add_action( 'init', 'cpt_adverts', 0 );

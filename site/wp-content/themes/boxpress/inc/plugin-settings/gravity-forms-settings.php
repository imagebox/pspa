<?php
/**
 * Gravity Forms Settings
 *
 * @package BoxPress
 */
if ( function_exists( 'gravity_form' )) {

  // Move GF scripts to footer
  add_filter( 'gform_init_scripts_footer', '__return_true' );

  // Scroll to confirmation
  add_filter( 'gform_confirmation_anchor', '__return_true' );
}

// function email_header(){
// 	return "<div style='width: 600px; margin: 0 auto; font-family:Arial, sans-serif;'><img width='268' height='268' align='center' alt='".get_bloginfo('name')."' src='https://pspa.net/wp-content/themes/boxpress/assets/img/dist/branding/pspa-badge.png' style='width: 268px; margin: 0 auto;display:block;' />
// ";
// }
// function email_footer(){
// 	return "</div>";
// }
// //Append header + footer to all emails
// add_filter( 'gform_notification', 'form_notification_email', 10, 3 );
// function form_notification_email($notification, $form, $entry) {
// 	$notification['message'] = email_header()."Thank you for your Conference Registration!
//
//
// ".$notification['message']."
//
// ";
// 	$notification['message'] .= email_footer();
// 	return $notification;
// }

<?php
/**
 * Tribe Events Calendar
 *
 * @package BoxPress
 */

if ( function_exists( 'tribe_get_events' )) {
  define( 'TRIBE_DISABLE_TOOLBAR_ITEMS', true );
}

<h1>Updating Campaigner with all currently active members</h1>
<?
set_time_limit(0);
ini_set('memory_limit', '160000000000M');
global $wpdb;

$args = array('orderby' => 'registered', 'order' => 'DESC');
// $args = array(
//   'search'         => 'BRWELLERDIEHL@GEISINGER.EDU',
//   'search_columns' => array('user_login', 'user_email'),
// );
$user_query = new WP_User_Query($args);

$users      = $user_query->get_results();
$counter    = 0;
$total      = count($users);
$total_text = "";

foreach ($users as $key => $user) {
  $user_subscriptions = wcs_get_users_subscriptions($user->ID);
  $user_subscription  = null;
  $member_status      = 'expired';

  foreach ($user_subscriptions as $subscription) {
    if ($subscription->has_status('active')) {
      $user_subscription = $subscription;
      break;
    }
  }

  write_log('$user_subscriptions--------------------------------------------------');
  write_log($user_subscription);
  write_log('/$user_subscriptions^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');

  if (wc_memberships_is_user_active_member($user->ID)) {
    $member_status = 'active';
    send_to_campaigner($user->ID, $member_status, ($member_status == 'active' ? $user_subscription : null));

  }
  $text = $user->user_email . ' set to ' . $member_status . " and their record was updated in Campaigner<br>";
  output_progress($counter++, $total, $text);
}

/**
 * Output span with progress.
 *
 * @param $current integer Current progress out of total
 * @param $total   integer Total steps required to complete
 */
function output_progress($current, $total, $text)
{
  global $total_text;
  $total_text .= "$text\n";
  write_log($current . '/' . $total . ' - ' . round($current / $total * 100) . "% complete\n$total_text\n\n");
  // do_flush();
}

/**
 * Flush output buffer
 */
function do_flush()
{
  echo (str_repeat(' ', 256));
  if (@ob_get_contents()) {
    @ob_end_flush();
  }
  flush();
}
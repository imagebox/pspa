<?php

$conference_product_ids = get_conference_product_ids();
$conference_orders      = get_orders_ids_by_product_ids($conference_product_ids);

$processed_orders_grouped = array();
$processed_orders         = array();
$products_exported        = array();
$all_products_ordered     = array();

foreach ($conference_orders as $key => $order_id) {
  $order     = wc_get_order($order_id);
  $date_paid = $order->get_date_paid()->date_i18n();

  foreach ($order->get_items() as $item_id => $item_data) {
    $product = $item_data->get_product();

    if ($product) {
      $subtract_extras = 0.00;
      if ($product->get_type() == 'bundle') {
        $entry_ids = get_gravity_form_entries_by_order_item_id($item_id);
        foreach ($entry_ids as $key => $entry_id) {
          $entry = GFAPI::get_entry($entry_id);
          if (!isset($entry->errors)) {
            $form_meta = get_gravity_form_meta($entry['form_id']);
            $form_meta = json_decode($form_meta[0]);
          }
          $category = 'Unknown';
          foreach ($entry as $key => $entry_value) {
            if (is_numeric($key)) {
              $label = get_gf_label_by_form_element_id($form_meta, $key);
              if ($label == 'Conference Code' && $category == 'Unknown') {
                $category = $entry_value;
              }
              if ($key == 32) {
                $subtract_extras = $entry_value;
              }
            }
          }

          foreach ($entry as $key => $entry_value) {
            if (is_numeric($key)) {
              $gf_label                                                  = get_gf_label_by_form_element_id($form_meta, $key);
              $processed_orders_grouped[$category][$entry_id][$gf_label] = $entry_value;
              $processed_orders[$entry_id][$gf_label]                    = $entry_value;
            }
          }
          $processed_orders_grouped[$category][$entry_id]['date_paid'] = $date_paid;
          $processed_orders[$entry_id]['date_paid']                    = $date_paid;
        }
      }
      $item_total = $item_data->get_subtotal() - $subtract_extras; // Get the item line total

      if (!isset($products_exported[$product->get_name()])) {
        $products_exported[$product->get_name()]['total'] = (float) $item_total;
        $products_exported[$product->get_name()]['count'] = (int) $item_data->get_quantity();
      } else {
        $products_exported[$product->get_name()]['total'] += (float) $item_total;
        $products_exported[$product->get_name()]['count'] += (int) $item_data->get_quantity();
      }
      $product_name = $product->get_name(); // Get the product name
      $product_id   = $product->get_id(); // Get the product id

      $item_quantity = $item_data->get_quantity(); // Get the item quantity

      $processed_orders_grouped[$category][$entry_id]['products'][$product_id]['product_name'] = $product_name;
      $processed_orders_grouped[$category][$entry_id]['products'][$product_id]['product_id']   = $product_id;
      $processed_orders_grouped[$category][$entry_id]['products'][$product_id]['quantity']     = $item_quantity;
      $processed_orders_grouped[$category][$entry_id]['products'][$product_id]['total']        = number_format($item_total, 2);
      $processed_orders_grouped[$category][$entry_id]['products'][$product_id]['date_paid']    = $date_paid;
      // $processed_orders_grouped[$category][$entry_id]['products'][$product_id]['is_product_membership'] = is_product_membership($product);

      $processed_orders[$entry_id]['products'][$product_id]['product_name'] = $product_name;
      $processed_orders[$entry_id]['products'][$product_id]['product_id']   = $product_id;
      $processed_orders[$entry_id]['products'][$product_id]['quantity']     = $item_quantity;
      $processed_orders[$entry_id]['products'][$product_id]['total']        = number_format($item_total, 2);
      $processed_orders[$entry_id]['products'][$product_id]['date_paid']    = $date_paid;
      // $processed_orders[$entry_id]['products'][$item_id]['is_product_membership'] = is_product_membership($product);
    }
  }
}

$translate_conference_code_to_title = array(
  'P'  => 'PSPA Fellow',
  'S'  => 'PSPA Student',
  'C'  => 'Constituent Fellow',
  'CS' => 'Constituent Student',
  'A'  => 'Membership & Conference',
  'A1' => 'Student 1-yr membership & conference',
  'A2' => 'Student 2-yr membership & conference',
  'XM' => 'Exempt Member (Free tuition)',
  'N'  => 'Non-member fellow',
  'NS' => 'Non-member student',
  'NA' => 'Non-attendee (speaker, VIP, etc.)',
  'D'  => 'Deleted',
);

<?php
/**
 * WordPress Editor related functions
 *
 * @package BoxPress
 */

/**
 * Editor Formats
 */

function boxpress_mce_buttons($buttons)
{
  array_unshift($buttons, 'styleselect');
  return $buttons;
}
add_filter('mce_buttons_2', 'boxpress_mce_buttons');

function boxpress_mce_before_init_insert_formats($init_array)
{
  $style_formats = array(
    array(
      'title'    => 'Lead Paragraph',
      'selector' => 'p',
      'classes'  => 'p-lead',
    ),
    array(
      'title'    => 'Button',
      'selector' => 'a',
      'classes'  => array('button', 'button--arrow'),
    ),
    // array(
    //   'title'     => 'Text Button',
    //   'selector'  => 'a',
    //   'classes'   => 'text-button',
    // ),
  );
  $init_array['style_formats'] = json_encode($style_formats);

  return $init_array;
}
add_filter('tiny_mce_before_init', 'boxpress_mce_before_init_insert_formats');

/**
 *  Remove the h5 and h6 tags from the WordPress editor.
 */
function boxpress_remove_headings_from_editor($settings)
{
  $settings['block_formats'] = 'Paragraph=p;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Preformatted=pre;';
  return $settings;
}
add_filter('tiny_mce_before_init', 'boxpress_remove_headings_from_editor');

/**
 * Editor Styles
 */

function boxpress_theme_add_editor_styles()
{
  add_editor_style('custom-editor-style.css');
}
add_action('admin_init', 'boxpress_theme_add_editor_styles');

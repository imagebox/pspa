<?php

if (isset($_GET['action']) && $_GET['action'] == 'card_pdf') {
  add_action('wp', 'card_pdf');
}

function card_pdf()
{
  require ABSPATH . 'wp-content/themes/boxpress/inc/membership-check.php';

  if (!$is_active_member) {
    die('Your membership has expired');
  }
  $mpdf          = new \Mpdf\Mpdf();
  $mpdf->debug   = true;
  $user_id       = get_current_user_id();
  $user          = get_userdata($user_id);
  $user_meta     = get_user_meta($user_id);
  $memberships   = wc_memberships_get_user_active_memberships($user_id);
  $subscriptions = wcs_get_users_subscriptions($user_id);

  if (!isset($user_meta['MBYearJoined']) || !isset($user_meta['MBYearJoined'][0]) || $user_meta['MBYearJoined'][0] == '') {
    $user_meta['MBYearJoined']    = [];
    $user_meta['MBYearJoined'][0] = date('Y');
  }
  $total        = 0.00;
  $date_paid    = '';
  $date_expired = '';

  foreach ($subscriptions as $subscription) {
    if ($subscription->has_status(array('active'))) {
      $total        = $subscription->get_total();
      $date_paid    = $subscription->get_date('start');
      $date_expired = $subscription->get_date('next_payment');
      $date         = date_create($date_paid);
      $date_paid    = date_format($date, "m/d/Y");
      $date         = date_create($date_expired);
      $date_expired = date_format($date, "m/d/Y");
      break;
    }
  }

  ob_start();
  ?><html>
      <head>
          <style type="text/css">
              .card {
                background-image: url('<?=site_url('/wp-content/themes/boxpress/inc/card-certificate/card.png');?>');
                background-repeat: no-repeat;
                background-size: 50%;
                width:  100%;
                height: 100%;
                font-size: 9px;
              }

              .card td {
                font-size: 8px;
              }

              .first_col {
                width:  170px;
              }

              .second_col {
                width:  30px;
              }

              .first_row {
                height: 10px;
              }

              .second_row {
                height: 36px;
              }
              .card td.expires {
                font-size: 11px;
                color: #004890;
                padding: 10px 0px 0px 22px;
                font-style: italic;
              }
              .member_since {
                padding: 30px 0 0 93px;
              }
          </style>
      </head>
      <body>
          <div class="card">
              <table>
                  <tr>
                      <td class="first_row"></td>
                      <td></td>
                      <td class="member_since"><?=$user_meta['MBYearJoined'][0] != '' ? $user_meta['MBYearJoined'][0] : date('Y');?></td>
                  </tr>
                  <tr>
                      <td class="second_row"></td>
                      <td></td>
                      <td></td>
                  </tr>
                  <tr>
                      <td class="first_col"></td>
                      <td class="second_col"></td>
                      <td><?=$user_meta['MBPSPANumber'][0];?></td></td>
                  </tr>
                  <tr>
                      <td class="first_col"></td>
                      <td></td>
                      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$memberships[0]->plan->post->post_title;?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><?=$total;?></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$date_paid;?></td>
                  </tr>
                  <tr>
                    <td class="expires">Expires: <?=$date_expired;?></td>
                    <td></td>
                    <td></td>
                  </tr>
              </table>
          </div>
      </body>
  </html><?
  $html = ob_get_contents();
  ob_end_clean();
// Write some HTML code:
  $mpdf->WriteHTML($html);
  $mpdf->Output('PSPA-Membership-Card.pdf', \Mpdf\Output\Destination::DOWNLOAD);
}

if (isset($_GET['action']) && $_GET['action'] == 'certificate_pdf') {
  add_action('wp', 'certificate_pdf');
}

function certificate_pdf()
{
  require ABSPATH . 'wp-content/themes/boxpress/inc/membership-check.php';

  if (!$is_active_member) {
    die('Your membership has expired');
  }
  $mpdf = new \Mpdf\Mpdf([
    'orientation'   => 'L',
    'margin_left'   => 20,
    'margin_right'  => 0,
    'margin_top'    => 5,
    'margin_bottom' => 0,
    'margin_header' => 0,
    'margin_footer' => 0,
  ]);
  $mpdf->debug   = true;
  $user_id       = get_current_user_id();
  $user          = get_userdata($user_id);
  $user_meta     = get_user_meta($user_id);
  $memberships   = wc_memberships_get_user_active_memberships($user_id);
  $subscriptions = wcs_get_users_subscriptions($user_id);

  $date_paid    = '';
  $date_expired = '';
  foreach ($subscriptions as $subscription) {
    if ($subscription->has_status(array('active'))) {
      $total        = $subscription->get_total();
      $date_paid    = $subscription->get_date('start');
      $date_expired = $subscription->get_date('next_payment');
      $date         = date_create($date_paid);
      $date_paid    = date_format($date, "m/d/Y");
      $date         = date_create($date_expired);
      $date_expired = date_format($date, "F j, Y");
      break;
    }
  }

  ob_start();
  ?><html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Libre+Caslon+Text&display=swap" rel="stylesheet">
        <style type="text/css">
            .certificate {
              background-image: url('<?=site_url('/wp-content/themes/boxpress/inc/card-certificate/certificate2.png');?>');
              background-repeat: no-repeat;
              background-size: 100%;
              width:  96%;
              height: 100%;
            }

            .certificate .name {
              font-family: 'Libre Caslon Text', serif;
              font-size: 36px;
              text-align: center;
              width: 100%;
              padding-top:  295px;
            }

            .certificate .date {
              font-family: 'Libre Caslon Text', serif;
              font-size: 16px;
              padding-left: 690px;
              padding-top: 50px;
            }

            .certificate .member_info {
              font-family: 'Libre Caslon Text', serif;
              padding-left: 150px;
              padding-top: 265px;
            }

        </style>
      </head>
      <body>
          <div class="certificate">
              <div class="name"><?=titleCase($user_meta['first_name'][0]) . ' ' . titleCase($user_meta['last_name'][0]);?></div>
              <div class="date"><?=$date_expired;?></div>
              <div class="member_info"><?=$user_meta['MBPSPANumber'][0];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$memberships[0]->plan->post->post_title;?></div>
          </div>
      </body>
  </html><?
  $html = ob_get_contents();
  ob_end_clean();
  // Write some HTML code:
  $mpdf->WriteHTML($html);
  $mpdf->Output('PSPA-Membership-Certificate.pdf', \Mpdf\Output\Destination::DOWNLOAD);
}

function titleCase($string)
{
  $word_splitters       = array(' ', '-', "O'", "L'", "D'", 'St.', 'Mc');
  $lowercase_exceptions = array('the', 'van', 'den', 'von', 'und', 'der', 'de', 'da', 'of', 'and', "l'", "d'");
  $uppercase_exceptions = array('III', 'IV', 'VI', 'VII', 'VIII', 'IX');

  $string = strtolower($string);
  foreach ($word_splitters as $delimiter) {
    $words    = explode($delimiter, $string);
    $newwords = array();
    foreach ($words as $word) {
      if (in_array(strtoupper($word), $uppercase_exceptions)) {
        $word = strtoupper($word);
      } else
      if (!in_array($word, $lowercase_exceptions)) {
        $word = ucfirst($word);
      }

      $newwords[] = $word;
    }

    if (in_array(strtolower($delimiter), $lowercase_exceptions)) {
      $delimiter = strtolower($delimiter);
    }

    $string = join($delimiter, $newwords);
  }
  return $string;
}

<?php

function lookupCivicInfo($address)
{
  $ret_val = array(
    'pa_house'  => 0,
    'pa_senate' => 0,
  );
  $results = false;
  $client  = new Google_Client();
  $client->setApplicationName("PSPA_Civic_Lookup");
  $client->setDeveloperKey("AIzaSyBcuHn8MtPnnyyU6UIEt883SgXf5BteGMA");

  $service   = new Google_Service_CivicInfo($client);
  $optParams = array(
    'address'        => $address,
    'includeOffices' => true,
  );
  try {
    $results = $service->representatives->representativeInfoByAddress($optParams);
  } catch (Exception $e) {}
  if ($results) {

    foreach ($results->divisions as $key => $division) {
      $spl_key = explode('/', $key);
      foreach ($spl_key as $k => $v) {
        $sub_keys = explode(':', $v);
        if ($sub_keys[0] === 'sldl') {
          $ret_val['pa_house'] = $sub_keys[1];
        }
        if ($sub_keys[0] === 'sldu') {
          $ret_val['pa_senate'] = $sub_keys[1];
        }
      }
    }
  }
  write_log('------------lookupCivicInfo---------------');
  write_log($address);
  write_log($ret_val);
  write_log($results);
  write_log('------------/lookupCivicInfo---------------');
  return $ret_val;
}

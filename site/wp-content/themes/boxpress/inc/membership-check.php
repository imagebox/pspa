<?php

$is_active_member = false;
$user_memberships = wc_memberships_get_user_active_memberships(get_current_user_id());

if ($user_memberships) {
  $is_active_member = true;
}

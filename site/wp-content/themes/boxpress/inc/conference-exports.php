<?php
/**
 * Export Data to CSV file
 * Could be used in WordPress plugin or theme
 */

// Add action hook only if action=rRegistrantsByCategory

if (isset($_GET['action']) && $_GET['action'] == 'rRegistrantsByCategory') {
  // Handle CSV Export
  add_action('admin_init', 'rRegistrantsByCategory');
}

if (isset($_GET['action']) && $_GET['action'] == 'rFinancials') {
  // Handle CSV Export
  add_action('admin_init', 'rFinancials');
}

if (isset($_GET['action']) && $_GET['action'] == 'rRegistrationDeskList') {
  // Handle CSV Export
  add_action('admin_init', 'rRegistrationDeskList');
}

if (isset($_GET['action']) && $_GET['action'] == 'rFunRunReport') {
  // Handle CSV Export
  add_action('admin_init', 'rFunRunReport');
}

if (isset($_GET['action']) && $_GET['action'] == 'rWorkshopSignups') {
  // Handle CSV Export
  add_action('admin_init', 'rWorkshopSignups');
}

if (isset($_GET['action']) && $_GET['action'] == 'rAllData') {
  // Handle CSV Export
  add_action('admin_init', 'rAllData');
}

function get_count_data_csv()
{
  $file = ABSPATH . 'wp-content/themes/boxpress/inc/meal-report-data.csv';
  $csv  = array_map('str_getcsv', file($file));
  array_walk($csv, function (&$a) use ($csv) {
    $a = array_combine($csv[0], $a);
  });
  array_shift($csv); # remove column header

  $result = array();
  foreach ($csv as $key => $value) {
    $result[$value['product_id']] = $value;
  }
  return $result;
}

function erase_val(&$myarr)
{
  $myarr = array_map(create_function('$n', 'return "";'), $myarr);
}

function rAllData()
{
  security_check();
  require 'conference-export-array-setup.php';
  $args = array(
    'limit' => 1000,
  );
  $all_conf_products = wc_get_products($args);
  $header_row        = array_keys($processed_orders[array_shift(array_keys($processed_orders))]);
  array_pop($header_row); // get rid of the products key here...
  $header_row_conf_only = $header_row;

  $conf_products = array();

  foreach ($all_conf_products as $ac_product) {
    if (get_class($ac_product) == 'WC_Product_Variable') {
      $variations = $ac_product->get_available_variations();
      foreach ($variations as $k => $variation) {
        $conf_products[$variation['variation_id']]       = '';
        $header_row[$variation['variation_id']]          = $variation['attributes']['attribute_workshop-preference'];
        $header_row[$variation['variation_id'] . '_fee'] = $variation['attributes']['attribute_workshop-preference'] . ' Fee';
      }

    } else {
      $conf_products[$ac_product->get_id()]       = '';
      $header_row[$ac_product->get_id()]          = $ac_product->get_name();
      $header_row[$ac_product->get_id() . '_fee'] = $ac_product->get_name() . ' Fee';
    }
  }
  $base_row = array_flip(array_keys($header_row));
  erase_val($base_row);

  $filename = 'rAllData-' . time() . '.csv';

  $data_rows = array();
  foreach ($processed_orders as $key => $conf_order) {
    $row      = $base_row;
    $products = array_pop($conf_order);
    foreach ($header_row_conf_only as $key => $value) {
      $row[$key] = $conf_order[$value];
    }
    foreach ($products as $product_id => $product) {
      $row[$product_id]          = 'X';
      $row[$product_id . '_fee'] = $product['total'];
    }
    $data_rows[] = $row;
  }

  output_data($filename, $header_row, $data_rows);
}

function rConferenceCounts()
{
  require 'conference-export-array-setup.php';
  // echo '<pre>';
  // print_r($processed_orders);
  // die();
  $counts = array(
    'All PSPA Members'                          => 0,
    'All Constituents'                          => 0,
    'All Non-Members'                           => 0,
    'All Non-Attendees'                         => 0,
    'Registrant Guests'                         => 0,
    'Total Registered'                          => 0,
    'PSPA Graduates'                            => 0,
    'PSPA Students'                             => 0,
    'Membership and Conference Graduates'       => 0,
    'Membership and Conference Students - 1 yr' => 0,
    'Membership and Conference Students - 2 yr' => 0,
    'Constituent Members Graduates'             => 0,
    'Constituent Members Students'              => 0,
    'Non-Members Graduates'                     => 0,
    'Non-Members Students'                      => 0,
    'Meals Wednesday'                           => 0,
    'Meals Thursday'                            => 0,
    'Meals Friday'                              => 0,
    'Meals Saturday'                            => 0,
    'Non-Attendee Thursday'                     => 0,
    'Non-Attendee Friday Luncheon'              => 0,
    'Non-Attendee Friday Dinner'                => 0,
    'VIPs'                                      => 0,
    'Volunteers'                                => 0,
    'Speakers'                                  => 0,
    'Leadership Panelists'                      => 0,
    'Scholarship Award Winners'                 => 0,
    'Poster Presenters'                         => 0,
    'Challenge Bowl'                            => 0,
    'Diversity Project Winners'                 => 0,
    'VIP Guests'                                => 0,
  );

  $count_data = get_count_data_csv();

  foreach ($processed_orders as $key => $conf_order) {
    if ($conf_order['Categories VIP']) {
      $counts['VIPs']++;
    }
    if ($conf_order['Categories Volunteer']) {
      $counts['Volunteers']++;
    }
    if ($conf_order['Categories Guest']) {
      $counts['VIP Guests']++;
    }
    if ($conf_order['Categories Scholarship Winner']) {
      $counts['Scholarship Award Winners']++;
    }
    if ($conf_order['Categories Poster Presenter']) {
      $counts['Poster Presenters']++;
    }
    if ($conf_order['Categories Diversity Project Winner']) {
      $counts['Diversity Project Winners']++;
    }
    if ($conf_order['Categories Challenge Bowl Faculty']) {
      $counts['Challenge Bowl']++;
    }
    if ($conf_order['Categories Challenge Bowl Contestant']) {
      $counts['Challenge Bowl']++;
    }
    if ($conf_order['Categories Leadership Panelist']) {
      $counts['Leadership Panelists']++;
    }
    if ($conf_order['I am bringing a guest'] == 'Yes') {
      $counts['Total Registered']++;
      $counts['Registrant Guests']++;
    }

    // Meals data in the Gravity Form entry
    if ($conf_order['Separately Auction Reception']) {
      if ($conf_order['Conference Code'] != 'NA' && $conf_order['Conference Code'] != 'D') {
        $counts['Meals Thursday']++;
      } elseif ($conf_order['Conference Code'] == 'NA') {
        $counts['Non-Attendee Thursday']++;
      }
    }
    if ($conf_order['Separately Challenge Bowl']) {
      if ($conf_order['Conference Code'] != 'NA' && $conf_order['Conference Code'] != 'D') {
        $counts['Meals Friday']++;
      } elseif ($conf_order['Conference Code'] == 'NA') {
        $counts['Non-Attendee Friday']++;
      }
    }
    if ($conf_order['I am bringing a guest'] == 'Yes' && $conf_order['Fees'] == 'All Meals/Events|100') {
      $counts['Meals Wednesday']++;
      $counts['Meals Thursday']++;
      $counts['Meals Friday']++;
      $counts['Meals Saturday']++;
    }

    /*
    'VIPs'                                      => 0,
    'Volunteers'                                => 0,
    'Speakers'                                  => 0,
    'Leadership Panelists'                      => 0,
    'Scholarship Award Winners'                 => 0,
    'Poster Presenters'                         => 0,
    'Challenge Bowl'                            => 0,
    'Diversity Project Winners'                 => 0,
    'VIP Guests'                                => 0,

    'P'  => 'PSPA Fellow',
    'S'  => 'PSPA Student',
    'C'  => 'Constituent Fellow',
    'CS' => 'Constituent Student',
    'A'  => 'Membership & Conference',
    'A1' => 'Student 1-yr membership & conference',
    'A2' => 'Student 2-yr membership & conference',
    'XM' => 'Exempt Member (Free tuition)',
    'N'  => 'Non-member fellow',
    'NS' => 'Non-member student',
    'NA' => 'Non-attendee (speaker, VIP, etc.)',
    'D'  => 'Deleted',

    'All PSPA Members'                          => 0,
    'All Constituents'                          => 0,
    'All Non-Members'                           => 0,
    'Registrant Guests'                         => 0,
    'Total Registered'                          => 0,
    'PSPA Graduates'                            => 0,
    'PSPA Students'                             => 0,
    'Membership and Conference Graduates'       => 0,
    'Membership and Conference Students - 1 yr' => 0,
    'Membership and Conference Students - 2 yr' => 0,
    'Constituent Members Graduates'             => 0,
    'Constituent Members Students'              => 0,
    'Non-Members Graduates'                     => 0,
    'Non-Members Students'                      => 0,
     */

    switch (strtoupper($conf_order['Conference Code'])) {
      case 'P':
        $counts['Total Registered']++;
        $counts['All PSPA Members']++;
        $counts['PSPA Graduates']++;
        break;
      case 'S':
        $counts['Total Registered']++;
        $counts['All PSPA Members']++;
        $counts['PSPA Students']++;
        break;
      case 'C':
        $counts['Total Registered']++;
        $counts['Constituent Members Graduates']++;
        $counts['All Constituents']++;
        break;
      case 'CS':
        $counts['Total Registered']++;
        $counts['Constituent Members Students']++;
        $counts['All Constituents']++;
        break;
      case 'A':
        $counts['Total Registered']++;
        $counts['All PSPA Members']++;
        $counts['PSPA Graduates']++;
        $counts['Membership and Conference Graduates']++;
        break;
      case 'A1':
        $counts['Total Registered']++;
        $counts['All PSPA Members']++;
        $counts['PSPA Students']++;
        $counts['Membership and Conference Students - 1 yr']++;
        break;
      case 'A2':
        $counts['Total Registered']++;
        $counts['All PSPA Members']++;
        $counts['PSPA Students']++;
        $counts['Membership and Conference Students - 2 yr']++;
        break;
      case 'XM':
        $counts['Total Registered']++;
        $counts['All PSPA Members']++;
        $counts['PSPA Graduates']++;
        break;
      case 'N':
        $counts['Total Registered']++;
        $counts['All Non-Members']++;
        $counts['Non-Members Graduates']++;
        break;
      case 'NS':
        $counts['Total Registered']++;
        $counts['All Non-Members']++;
        $counts['Non-Members Students']++;
        break;
      case 'NA':
        $counts['Total Registered']++;
        $counts['All Non-Attendees']++;
        break;
      case 'D':
        break;
      default:
        break;
    }
    foreach ($conf_order['products'] as $product) {
      if (in_array($product['product_id'], array_keys($count_data))) {
        if ($count_data[$product['product_id']]['day'] == 'all') {
          $counts['Meals Wednesday']++;
          $counts['Meals Thursday']++;
          $counts['Meals Friday']++;
          $counts['Meals Saturday']++;
        } elseif ($count_data[$product['product_id']]['day'] == 'wed') {
          $counts['Meals Wednesday']++;
        } elseif ($count_data[$product['product_id']]['day'] == 'thu') {
          $counts['Meals Thursday']++;
        } elseif ($count_data[$product['product_id']]['day'] == 'fri') {
          $counts['Meals Friday']++;
        } elseif ($count_data[$product['product_id']]['day'] == 'sat') {
          $counts['Meals Saturday']++;
        }
      }
    }
  }
  return $counts;
}

function rFinancials()
{
  security_check();
  require 'conference-export-array-setup.php';

  $filename = 'rFinancials-' . time() . '.csv';

  $header_row = array(
    'Last Name',
    'First Name',
    'Title',
    'Attendee Type',
    'All',
    'We',
    'Th',
    'Fr',
    'Sa',
    'Tuition Charges',
    'Gst Charges',
    'WkShp',
    'Charity',
    'Fun Run',
    'Membership Chg',
    'Total Paid',
    'Date Paid',
  );
  $sum_row = array(
    '',
    '',
    '',
    '',
    'All'             => 0,
    'We'              => 0,
    'Th'              => 0,
    'Fr'              => 0,
    'Sa'              => 0,
    'Tuition Charges' => 0,
    'Gst Charges'     => 0,
    'WkShp'           => 0,
    'Charity'         => 0,
    'Fun Run'         => 0,
    'Membership Chg'  => 0,
    'Total Paid'      => 0,
    '',
  );
  $data_rows = array();

  foreach ($processed_orders as $key => $conf_order) {
    $day_attending = array(
      'all' => '',
      'wed' => '',
      'thu' => '',
      'fri' => '',
      'sat' => '',
    );

    $workshops_attending = array(
      'Thursday 7:00am – 9:00am'   => '',
      'Thursday 4:30pm – 6:30pm'   => '',
      'Friday 7:00am – 9:00am'     => '',
      'Friday 4:00pm – 6:00pm'     => '',
      'Saturday 8:00am – 10:00am'  => '',
      'Saturday 10:00am – 12:00pm' => '',
    );

    $tuition_total    = 0.0;
    $guest_total      = 0.0;
    $workshop_total   = 0.0;
    $charity_total    = 0.0;
    $fun_run_total    = 0.0;
    $membership_total = 0.0;
    $net_total        = 0.0;

    foreach ($conf_order['products'] as $product) {
      $net_total += $product['total'];
      if (stripos($product['product_name'], 'Full Conference') !== false) {
        $day_attending['all'] = 'X';
        $tuition_total += $product['total'];
      } elseif (stripos($product['product_name'], 'Daily Tuition') !== false) {
        $tuition_total += $product['total'];
      } elseif (stripos($product['product_name'], 'Wednesday - Conference') !== false) {
        $day_attending['wed'] = 'X';
        $tuition_total += $product['total'];
      } elseif (stripos($product['product_name'], 'Thursday - Conference') !== false) {
        $day_attending['thu'] = 'X';
        $tuition_total += $product['total'];
      } elseif (stripos($product['product_name'], 'Friday - Conference') !== false) {
        $day_attending['fri'] = 'X';
        $tuition_total += $product['total'];
      } elseif (stripos($product['product_name'], 'Saturday - Conference') !== false) {
        $day_attending['sat'] = 'X';
        $tuition_total += $product['total'];
      } elseif (stripos($product['product_name'], 'Conference WorkShop') !== false) {
        $workshop_total += $product['total'];
      } elseif (stripos($product['product_name'], 'Charity') !== false) {
        $charity_total += $product['total'];
      } elseif ((stripos($product['product_name'], 'Membership -') !== false) || (stripos($product['product_name'], 'Student -') === 0)) {
        $membership_total += $product['total'];
      }
    }

    // add guest fees
    if ($conf_order['I am bringing a guest'] == 'Yes') {
      $guest_charge_fields = array(
        'Separately Challenge Bowl',
        'Separately Auction Reception',
        'Fees',
      );
      foreach ($guest_charge_fields as $value) {
        $exp = explode('|', $conf_order[$value]);
        if ((float) $exp[1] > 0) {
          $guest_total += (float) $exp[1];
        }
      }
      $net_total += $guest_total;
    }

    // add fun run fees
    if ($conf_order['Options'] != '') {
      $exp = explode('|', $conf_order['Options']);
      $fun_run_total += (float) $exp[1];
    }
    $net_total += $fun_run_total;

    $row = array(
      titleCase($conf_order['Name Last']),
      titleCase($conf_order['Name First']),
      $conf_order['Title'],
      $translate_conference_code_to_title[$conf_order['Conference Code']],
      $day_attending['all'],
      $day_attending['wed'],
      $day_attending['thu'],
      $day_attending['fri'],
      $day_attending['sat'],
      $tuition_total,
      $guest_total,
      $workshop_total,
      $charity_total,
      $fun_run_total,
      $membership_total,
      $net_total,
      $conf_order['date_paid'],
    );

    $sum_row['Tuition Charges'] += $tuition_total;
    $sum_row['Gst Charges'] += $guest_total;
    $sum_row['WkShp'] += $workshop_total;
    $sum_row['Charity'] += $charity_total;
    $sum_row['Fun Run'] += $fun_run_total;
    $sum_row['Membership Chg'] += $membership_total;
    $sum_row['Total Paid'] += $net_total;

    if ($day_attending['all'] == 'X') {
      $sum_row['All']++;
    }
    if ($day_attending['wed'] == 'X') {
      $sum_row['We']++;
    }
    if ($day_attending['thu'] == 'X') {
      $sum_row['Th']++;
    }
    if ($day_attending['fri'] == 'X') {
      $sum_row['Fr']++;
    }
    if ($day_attending['sat'] == 'X') {
      $sum_row['Sa']++;
    }
    $data_rows[] = $row;
  }
  $data_rows[] = $sum_row;
  output_data($filename, $header_row, $data_rows);
}

function rRegistrantsByCategory()
{
  security_check();
  require 'conference-export-array-setup.php';

  $filename = 'rRegistrantsByCategory-' . time() . '.csv';

  $header_row = array(
    'Registrant',
    'Title',
    'Address',
    'Email',
    'PA Program',
    'CSt',
  );
  $data_rows = array();
  foreach ($processed_orders_grouped as $group => $group_data) {
    $data_rows[] = [$translate_conference_code_to_title[$group]];
    foreach ($group_data as $key => $conf_order) {
      $row = array(
        titleCase($conf_order['Name First'] . ' ' . $conf_order['Name Last']),
        $conf_order['Title'],
        $conf_order['Home Address Street Address'] . ' ' . $conf_order['Home Address City'] . ', ' . $conf_order['Home Address State / Province'] . ' ' . $conf_order['Home Address ZIP / Postal Code'],
        $conf_order['Email'],
        $conf_order['PA Program'],
        $conf_order['Constituent Chapter: State Abbreviation'],
      );
      $data_rows[] = $row;
    }
  }
  output_data($filename, $header_row, $data_rows);
}

function rRegistrationDeskList()
{
  security_check();
  require 'conference-export-array-setup.php';

  $filename = 'rRegistrationDeskList-' . time() . '.csv';

  $header_row = array(
    'Last Name',
    'First Name',
    'Title',
    'Guest Name',
    'Days Attending',
    'All',
    'We',
    'Th',
    'Fr',
    'Sa',
    'Workshops attending total',
    'Thu 7:00am',
    'Thu 4:30pm',
    'Fri 7:00am',
    'Fri 4:00pm',
    'Sat 8:00am',
    'Sat 10:00am',
    'Workshop Charges',
    'Donated Amount',
    'Fun Run Charges',
    'Faculty Forum',
    'Leadership Forum',
    'Product Theater',
    'Student Forum - 1st Year',
    'Student Forum - 2nd Year',
  );
  $data_rows = array();

  // Last Name, First Name, Title, Guest Name, Days Attending, Workshops attending, Workshop Charges, Donated Amount, Fun Run Charges, and which of the Free events they are attending (Faculty forum, student forum, leadership forum, product theater
  foreach ($processed_orders as $key => $conf_order) {
    $days_attending            = 0;
    $workshops_attending_total = 0;
    $workshop_total            = 0;
    $donated_amount            = 0;
    $fun_run_charges           = 0;

    $day_attending = array(
      'all' => '',
      'wed' => '',
      'thu' => '',
      'fri' => '',
      'sat' => '',
    );

    $workshops_attending = array(
      'Thursday 7:00am - 9:00am'   => '',
      'Thursday 4:30pm - 6:30pm'   => '',
      'Friday 7:00am - 9:00am'     => '',
      'Friday 4:00pm - 6:00pm'     => '',
      'Saturday 8:00am - 10:00am'  => '',
      'Saturday 10:00am - 12:00pm' => '',
    );

    foreach ($conf_order['products'] as $product) {
      $workshop_data          = array();
      $workshop_name          = '';
      $workshop_timeslot_data = array();
      $workshop_timeslot      = '';

      $net_total += $product['total'];
      if (stripos($product['product_name'], 'Full Conference') !== false) {
        $days_attending       = 4;
        $day_attending['all'] = 'X';
      } elseif (stripos($product['product_name'], 'Wednesday - Conference') !== false) {
        $days_attending++;
        $day_attending['wed'] = 'X';
      } elseif (stripos($product['product_name'], 'Thursday - Conference') !== false) {
        $days_attending++;
        $day_attending['thu'] = 'X';
      } elseif (stripos($product['product_name'], 'Friday - Conference') !== false) {
        $days_attending++;
        $day_attending['fri'] = 'X';
      } elseif (stripos($product['product_name'], 'Saturday - Conference') !== false) {
        $days_attending++;
        $day_attending['sat'] = 'X';
      } elseif (stripos($product['product_name'], 'Conference WorkShop') !== false) {
        $workshop_total += $product['total'];
        $workshops_attending_total++;

        $workshop_data = explode(' - ', $product['product_name']);
        $workshop_name = array_pop($workshop_data);

        // handle the case when there is a hyphenated workshop name
        if (stripos($workshop_data[count($workshop_data) - 1], ':') === false) {
          $workshop_name .= ' - ' . array_pop($workshop_data);
        }
        $workshop_timeslot_data[] = array_pop($workshop_data);
        $workshop_timeslot_data[] = array_pop($workshop_data);
        $workshop_timeslot        = implode(' - ', array_reverse($workshop_timeslot_data));

        $workshops_attending[$workshop_timeslot] = trim($workshop_name);
      } elseif (stripos($product['product_name'], 'Charity') !== false) {
        $donated_amount += (float) preg_replace("/[^0-9]/", "", $product['total']) / 100;
      }
    }

    // add fun run fees
    if ($conf_order['Options'] != '') {
      $exp = explode('|', $conf_order['Options']);
      $fun_run_charges += (float) $exp[1];
    }

    $row = array(
      titleCase($conf_order['Name Last']),
      titleCase($conf_order['Name First']),
      $conf_order['Title'],
      titleCase($conf_order['Guest Name First'] . ' ' . $conf_order['Guest Name Last']),
      $days_attending,
      $day_attending['all'],
      $day_attending['wed'],
      $day_attending['thu'],
      $day_attending['fri'],
      $day_attending['sat'],
      $workshops_attending_total,
      $workshops_attending['Thursday 7:00am - 9:00am'],
      $workshops_attending['Thursday 4:30pm - 6:30pm'],
      $workshops_attending['Friday 7:00am - 9:00am'],
      $workshops_attending['Friday 4:00pm - 6:00pm'],
      $workshops_attending['Saturday 8:00am - 10:00am'],
      $workshops_attending['Saturday 10:00am - 12:00pm'],
      '$' . $workshop_total,
      '$' . $donated_amount,
      '$' . $fun_run_charges,
      $conf_order['Free Activities Faculty Forum (Free)'] ? 'X' : '',
      $conf_order['Free Activities Leadership Forum (Free)'] ? 'X' : '',
      $conf_order['Free Activities Product Theater (Free) HIV Prevention-Gilead (Pre-registration required)'] ? 'X' : '',
      $conf_order['Free Activities Student Forum - 1st Year (Free)'] ? 'X' : '',
      $conf_order['Free Activities Student Forum - 2nd Year (Free)'] ? 'X' : '',
    );
    $data_rows[] = $row;
  }
  output_data($filename, $header_row, $data_rows);
}

function rWorkshopSignups()
{
  security_check();
  require 'conference-export-array-setup.php';
  ?>
  <style type="text/css">
    body, table {
      font-family: Arial;
      font-size: 12px;
    }
    h1, h2 {
      text-align: center;
    }
    td {
      border-bottom: 1px solid black;
      height: 20px;
      padding: 10px 0;
    }
    table {
      width: 100%;
    }
    th {
      text-align: left;
    }
    .header {
      text-decoration: underline;
    }
  </style>
  <?

  $workshops = array();

  //Sign-in Sheet for each Workshop. Includes Last Name, First Name and place for attendee signature
  foreach ($processed_orders as $key => $conf_order) {
    foreach ($conf_order['products'] as $product) {
      if (stripos($product['product_name'], 'Conference WorkShop') !== false) {
        if (!isset($workshops[$product['product_name']])) {
          $workshops[$product['product_name']] = array();
        }
        $workshops[$product['product_name']][] = array(
          'Name' => titleCase($conf_order['Name Last'] . ', ' . $conf_order['Name First']),
        );
      }
    }
  }

  $title_output = '';
  foreach ($workshops as $workshop_title => $workshop) {
    usort($workshop, function ($a, $b) {
      return strcmp($a['Name'], $b['Name']);
    });
    if ($title_output != $workshop_title) {
      if ($title_output) {
        ?>
        <div style="page-break-after: always;"></div>
        <?
      }

      // give some blank lines for walk-ins
      $workshop[] = ['Name' => ' <br>'];
      $workshop[] = ['Name' => ' <br>'];
      $workshop[] = ['Name' => ' <br>'];
      $workshop[] = ['Name' => ' <br>'];

      echo '<h1 style="margin: 0; border: 1px solid black; padding: 8px;">PSPA Annual Conference ' . date('Y') . ' Workshop Sign-In</h1><h2 style="margin: 0; border: 1px solid black; border-top: none; padding: 8px;">' . str_replace(['Conference WorkShop: '], [''], $workshop_title) . '</h2><p style="text-align: center;">If you have pre-registered for this workshop, please sign next to your printed name. If your name is not listed, please print and sign your name so that we have a record of your attendance at this workshop.</p>';
      $title_output = $workshop_title;
    }
    $counter = 0;
    $max_col = 20;
    $col     = array(
      0 => '',
      1 => '',
    );
    foreach ($workshop as $name) {
      $col[($counter++ <= $max_col) ? 0 : 1] .= '<tr><td>' . $name['Name'] . '</td></tr>';
    }
    ?>
    <table>
      <tr>
        <th width="50%"><table><tr><th class="header">Print Name</th><th class="header">Signature</th></tr></table></th>
        <th width="50%">
          <?if ($counter > $max_col) {?>
            <table><tr><th class="header">Print Name</th><th class="header">Signature</th></tr></table>
          <?}?>
          </th>
      </tr>
      <tr>
        <td valign="top" width="50%" style="border-bottom: none;"><table><?=$col[0];?></table></td>
        <td valign="top" width="50%" style="border-bottom: none;"><table><?=$col[1];?></table></td>
      </tr>
    </table>
    <?
  }
  $html = ob_get_contents();
  ob_end_clean();

  $mpdf = new \Mpdf\Mpdf([
    'margin_left'   => 5,
    'margin_right'  => 5,
    'margin_top'    => 5,
    'margin_bottom' => 5,
  ]);
  $mpdf->debug = true;
  $mpdf->WriteHTML($html);
  $mpdf->Output('PSPA-Workshop-Signup-' . time() . '.pdf', \Mpdf\Output\Destination::DOWNLOAD);
}

// thad trying to do a csv export

function rFunRunReport()
{
  security_check();
  require 'conference-export-array-setup.php';

  $filename = 'rFunRunReport-' . time() . '.csv';

  $header_row = array(
    'Last Name',
    'First Name',
    'T-Shirt Size',
  );
  $data_rows = array();

  // Last Name, First Name, T-Shirt Size
  foreach ($processed_orders as $key => $conf_order) {

    if ($conf_order['Options'] == 'Particpate Fun Run / Bike / Run + Shirt|15') {
      $row = array(
        titleCase($conf_order['Name Last']),
        titleCase($conf_order['Name First']),
        titleCase($conf_order['Select T-Shirt Size']),
      );
      $data_rows[] = $row;
    }
  }

  output_data($filename, $header_row, $data_rows);
}

// end thad trying to do a csv export

function security_check()
{
  // Check for current user privileges
  if (!current_user_can('manage_options')) {return false;}

  // Check if we are in WP-Admin
  if (!is_admin()) {return false;}

  // Nonce Check
  $nonce = isset($_GET['_wpnonce']) ? $_GET['_wpnonce'] : '';
  if (!wp_verify_nonce($nonce, 'PSPAReports')) {
    die('Security check error');
  }

  ob_start();
}

function output_data($filename = 'csv-export', $header_row = array(), $data_rows = array())
{
  $fh = @fopen('php://output', 'w');
  fprintf($fh, chr(0xEF) . chr(0xBB) . chr(0xBF));
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Content-Description: File Transfer');
  header('Content-type: text/csv');
  header("Content-Disposition: attachment; filename={$filename}");
  header('Expires: 0');
  header('Pragma: public');
  fputcsv($fh, $header_row);
  foreach ($data_rows as $data_row) {
    fputcsv($fh, $data_row);
  }
  fclose($fh);

  ob_end_flush();

  die();
}

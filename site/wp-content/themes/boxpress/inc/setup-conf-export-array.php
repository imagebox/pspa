<?php
/****
These are all the Product Bundles that combine the Gravity Form and applicable products together
****/
$conference_orders = get_orders_ids_by_product_ids([
  8350,
  8532,
  8658,
  8663,
  8664,
  8665,
  8671,
  8683,
  14146,
  14597,
  14634,
  14651,
  24199,
  24201
]);
// echo "\n\n" . count($conference_orders) . " orders match\n\n>";
// print_r($conference_orders);

//
// print_r($order);

// $conference_orders = array(
// 0 => 24284);
$processed_orders_grouped = array();
$processed_orders = array();
$products_exported = array();

foreach ($conference_orders as $key => $order_id) {
  $order = wc_get_order($order_id);

  $entry = GFAPI::get_entry(get_gravity_form_entry_by_order_id($order_id));

  if(!isset($entry->errors)) {
    $form_meta = get_gravity_form_meta($entry['form_id']);
    $form_meta = json_decode($form_meta[0]);
  }
  foreach ($entry as $key => $entry_value) {
    $category = 'Unknown';
    if(is_numeric($key)) {
      $label = get_gf_label_by_form_element_id($form_meta, $key);
      if($label == 'Conference Route') {
        $category = $entry_value;
        break;
      }
    }
  }

  $processed_orders_grouped[$category][$order_id]['order_total'] = $order->get_formatted_order_total();
  $processed_orders[$order_id]['order_total'] = $order->get_formatted_order_total();
  foreach ($entry as $key => $entry_value) {
    if(is_numeric($key)) {
      $gf_label = get_gf_label_by_form_element_id($form_meta, $key);
      $processed_orders_grouped[$category][$order_id][$gf_label] = $entry_value;
      $processed_orders[$order_id][$gf_label] = $entry_value;
    }
  }

  // Iterating through each "line" items in the order
  foreach ($order->get_items() as $item_id => $item_data) {

    // Get an instance of corresponding the WC_Product object
    $product = $item_data->get_product();
    if($product) {
      if(!isset($products_exported[$product->get_name()])) {
        $products_exported[$product->get_name()]['total'] = (float)$item_data->get_total();
        $products_exported[$product->get_name()]['count'] = (int)$item_data->get_quantity();
      } else {
        $products_exported[$product->get_name()]['total'] += (float)$item_data->get_total();
        $products_exported[$product->get_name()]['count'] += (int)$item_data->get_quantity();
      }
      $product_name = $product->get_name(); // Get the product name

      $item_quantity = $item_data->get_quantity(); // Get the item quantity

      $item_total = $item_data->get_total(); // Get the item line total

      $processed_orders_grouped[$category][$order_id]['products'][$item_id]['product_name'] = $product_name;
      $processed_orders_grouped[$category][$order_id]['products'][$item_id]['quantity'] = $item_quantity;
      $processed_orders_grouped[$category][$order_id]['products'][$item_id]['total'] = number_format( $item_total, 2 );

      $processed_orders[$order_id]['products'][$item_id]['product_name'] = $product_name;
      $processed_orders[$order_id]['products'][$item_id]['quantity'] = $item_quantity;
      $processed_orders[$order_id]['products'][$item_id]['total'] = number_format( $item_total, 2 );
    }
  }
}
?>
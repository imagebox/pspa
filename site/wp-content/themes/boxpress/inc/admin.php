<?php
/**
 * WordPress admin related functions
 *
 * @package BoxPress
 */

/**
 * BoxPress Logo
 */
function my_login_logo()
{?>
  <style type="text/css">
    body.login {
      font-size: 1em;
    }
    body.login div#login,
    body.login div#login h1 {
      font-size: inherit;
    }
    body.login div#login h1 a {
      background-image: url( '<?php echo get_stylesheet_directory_uri(); ?>/assets/img/dist/branding/pspa-badge-transparent.png' );
      padding-bottom: 0.875em;
      height: 134px;
      width: 134px;
      font-size: inherit;
      background-size: 134px 134px;
      transition: opacity 500ms;
    }
    body.login div#login h1 a:hover,
    body.login div#login h1 a:focus {
      opacity: 0.75;
    }
  </style>
<?php }
add_action('login_enqueue_scripts', 'my_login_logo');

/**
 * Show the kitchen sink by default
 */
function unhide_kitchensink($args)
{
  $args['wordpress_adv_hidden'] = false;
  return $args;
}
add_filter('tiny_mce_before_init', 'unhide_kitchensink');

/**
 * Set default media link to 'none'
 */
update_option('image_default_link_type', 'none');

/**
 * Set default gallery link to 'file'
 */

function my_gallery_default_type_set_link($settings)
{
  $settings['galleryDefaults']['link'] = 'file';
  return $settings;
}
add_filter('media_view_settings', 'my_gallery_default_type_set_link');

/**
 * Remove 'Editor' from Appearance menu
 */
function remove_editor_menu()
{
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);

/**
 * Remove CSS section from Appearance menu
 * @param $wp_customize WP_Customize_Manager
 */
function prefix_remove_css_section($wp_customize)
{
  $wp_customize->remove_section('custom_css');
}
add_action('customize_register', 'prefix_remove_css_section', 15);

/**
 * Add User Role Class to Body
 */
function print_user_classes()
{
  if (is_user_logged_in()) {
    add_filter('body_class', 'class_to_body');
  }
}
add_action('init', 'print_user_classes');

// Add user role class and user id to body tag
function class_to_body($classes)
{
  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_roles   = $current_user->roles;
    $user_ID      = $current_user->ID;

    // Add user ID Class
    $classes[] = "user-id-{$user_ID}";

    if ($user_roles) {
      foreach ($user_roles as $role) {
        // Add user role Classes
        $classes[] = "user-role-{$role}";
      }
    }

    return $classes;
  }
}

add_action('admin_menu', 'my_admin_menu');

/**
 * Get All orders IDs for a given product ID.
 *
 * @param  integer  $product_id (required)
 * @param  array    $order_status (optional) Default is 'wc-completed'
 *
 * @return array
 */
function get_orders_ids_by_product_ids($product_ids, $order_status = array('wc-completed', 'wc-processing'))
{
  global $wpdb;

  $sql = "
        SELECT DISTINCT order_items.order_id
        FROM {$wpdb->prefix}woocommerce_order_items as order_items
        LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
        LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
        WHERE posts.post_type = 'shop_order'
        AND posts.post_status IN ( '" . implode("','", $order_status) . "' )
        AND order_items.order_id >= 25185
        AND order_items.order_item_type = 'line_item'
        AND order_item_meta.meta_key = '_product_id'";
  if (is_array($product_ids)) {
    $product_ids_str = implode("','", $product_ids);
    $sql .= " AND order_item_meta.meta_value IN ('$product_ids_str')";
  } else {
    $sql .= " AND order_item_meta.meta_value = '$product_ids'";

  }
  $results = $wpdb->get_col($sql);

  return $results;
}

function get_order_for_logged_in_user() {
  $args = array(
      'customer_id' => get_current_user_id(),
  );
  $orders = wc_get_orders( $args );
  return $orders;
}

function get_conference_product_ids()
{
  global $wpdb;
  // term_taxonomy_id = 48 is the id of the Annual Conference Bundle category
  $sql = "SELECT DISTINCT wp_posts.ID
          FROM wp_posts
          LEFT JOIN wp_term_relationships
          ON (wp_posts.ID = wp_term_relationships.object_id)
          LEFT JOIN wp_postmeta
          ON wp_posts.ID = wp_postmeta.post_id
          WHERE 1=1
          AND ( wp_term_relationships.term_taxonomy_id = 49 )
          AND wp_posts.post_type = 'product'
          AND wp_posts.post_status = 'publish'
          GROUP BY wp_posts.ID
          ORDER BY wp_posts.post_date DESC";
  $results = $wpdb->get_col($sql);
  return $results;
}

function is_product_membership($product)
{
  if (get_class($product) == 'WC_Product_Subscription_Variation') {
    return true;
  }
  $terms = get_the_terms($product->get_id(), 'product_cat');
  if ($terms) {
    return in_array('memberships', array_map(function ($t) {return $t->slug;}, $terms));
  }
  return false;
}

function is_product_one_time_student($product)
{
  $terms = get_the_terms($product->get_id(), 'product_cat');
  if ($terms) {
    return in_array('one-time-student', array_map(function ($t) {return $t->slug;}, $terms));
  }
  return false;
}

function is_product_one_time_graduate($product)
{
  $terms = get_the_terms($product->get_id(), 'product_cat');
  if ($terms) {
    return in_array('one-time-graduate', array_map(function ($t) {return $t->slug;}, $terms));
  }
  return false;
}

function order_contains_membership(WC_Order $order)
{
  $items = $order->get_items();
  foreach ($items as $key => $product) {
    if (is_product_membership($product->get_product())) {
      return true;
    }
  }
  return false;
}

function get_gravity_form_entries_by_order_item_id($order_item_id)
{
  global $wpdb;
  $sql = "SELECT entry_id FROM {$wpdb->prefix}gf_entry_meta WHERE meta_key ='woocommerce_order_item_number' AND meta_value = '$order_item_id'";

  $results = $wpdb->get_col($sql);
  if (is_array($results)) {
    return $results;
  } else {
    return false;
  }
}

function get_gravity_form_meta($form_id)
{
  global $wpdb;
  $sql = "SELECT display_meta FROM {$wpdb->prefix}gf_form_meta WHERE form_id = '$form_id'";

  $results = $wpdb->get_col($sql);
  return $results;
}

function get_gf_label_by_form_element_id($form_data, $element_id)
{
  $element_base = floor((float) $element_id);
  foreach ($form_data->fields as $key => $field) {
    if ($field->id == $element_base) {
      $label = $field->label;
      if ($element_id != $element_base && isset($field->inputs)) {
        foreach ($field->inputs as $input) {
          if ($input->id == $element_id) {
            $label .= ' ' . $input->label;
          }
        }
      }

      return $label;
    }
  }
  return false;
}

function my_admin_menu()
{
  add_menu_page('Conference Reports', 'Conference Reports', 'manage_options', 'conference-reports.php', 'pspa_conference_reports', 'dashicons-tickets', 6);
  add_submenu_page('conference-reports.php', 'Fun Run Report', 'Fun Run Report', 'manage_options', 'fun-run-report.php', 'imagebox_fun_run');
  // add_submenu_page('conference-reports.php', 'Campaigner Sync Active', 'Campaigner Sync Active', 'manage_options', 'myplugin/myplugin-admin-sub-page.php', 'imagebox_admin_sub_page');
  add_submenu_page('users.php', 'Duplicate User Detection', 'Duplicate User Detection', 'manage_options', 'pspa_duplicate_detection.php', 'pspa_duplicate_detection');
}

function pspa_conference_reports()
{
  require get_template_directory() . '/inc/admin-page-conference-reports.php';
}

function pspa_duplicate_detection()
{
  require get_template_directory() . '/inc/admin-page-duplicate-detection.php';
}

function imagebox_admin_sub_page()
{
  require get_template_directory() . '/inc/admin-page-campainer-testing.php';
  require get_template_directory() . '/inc/fun-run-report.php';
}

function imagebox_fun_run()
{
  require get_template_directory() . '/inc/fun-run-report.php';
}

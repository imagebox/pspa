<?php
use \Vynyl\Campaigner\API;
use \Vynyl\Campaigner\Config\VanillaConfig;
/**
 * WooCommerce - Actions to customize Woo behavior
 */

add_action('woocommerce_thankyou', 'imagebox_redirectcustom');
add_action('woocommerce_before_edit_account_form', 'imagebox_custom_edit_account_text');
add_action('wp_head', 'imagebox_quantity_wp_head');

add_filter('woocommerce_checkout_fields', 'imagebox_override_checkout_fields');
add_filter('gplc_remove_choices', '__return_false');
add_filter('gplc_pre_render_choice', 'my_add_how_many_left_message', 10, 5);

// remove out of the box woo actions we don't want/need
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

function imagebox_redirectcustom($order_id)
{
  write_log('imagebox_redirectcustom');
  $order          = wc_get_order($order_id);
  $has_membership = order_contains_membership($order);

  write_log($has_membership ? '$has_membership == true' : '$has_membership == false');

  if ($has_membership) {
    $url = get_site_url(null, '/thank-you-membership/');
  } else {
    $url = get_site_url(null, '/thank-you-conference/');
  }

  if ($order && $order->get_status() != 'failed') {
    write_log('order is NOT failed');
    $userdata = get_userdata(get_current_user_id());
    if ($has_membership) {
      // add PSPA number if there has not yet been a member number assigned
      imagebox_add_pspa_num_yr_joined($order);
    }
  }
  wp_safe_redirect($url);
  exit;
}

function get_gravity_form_entry_by_order_id($order_id)
{
  global $wpdb;
  $sql = "SELECT entry_id FROM {$wpdb->prefix}gf_entry_meta WHERE meta_key ='woocommerce_order_number' AND meta_value = '$order_id'";

  $results = $wpdb->get_col($sql);
  if (is_array($results)) {
    return $results[0];
  } else {
    return false;
  }
}

function get_gravity_form_entries_by_order_id($order_id)
{
  global $wpdb;
  $sql = "SELECT entry_id FROM {$wpdb->prefix}gf_entry_meta WHERE meta_key ='woocommerce_order_number' AND meta_value = '$order_id'";

  $results = $wpdb->get_col($sql);
  if (is_array($results)) {
    return $results;
  } else {
    return false;
  }
}
// SELECT entry_id FROM wp_gf_entry_meta WHERE meta_key ='woocommerce_order_number' AND meta_value = '47894'
function imagebox_add_pspa_num_yr_joined($order = null)
{
  write_log('imagebox_add_pspa_num_yr_joined');
  if ($order) {
    write_log('has order in imagebox_add_pspa_num_yr_joined');
    $user_id = (int) $order->get_user_id();
  } else {
    $user_id = get_current_user_id();
  }
  $current_max_pspa_num  = (int) get_option('options_starting_pspa_number', 1000000);
  $next_pspa_num         = $current_max_pspa_num + 1;
  $current_user_pspa_num = (int) get_user_meta($user_id, 'MBPSPANumber', $single = true);

  if ($current_user_pspa_num < 1) {
    write_log('has $current_user_pspa_num < 1');
    update_user_meta($user_id, 'MBPSPANumber', $next_pspa_num);
    update_user_meta($user_id, 'MBYearJoined', date('Y'));
    update_option('options_starting_pspa_number', $next_pspa_num);
    send_to_campaigner($user_id);
  }
}

function region_from_county($county)
{
  $county              = trim(strtoupper($county));
  $counties_to_regions = [
    'ALLEGHENY'      => 1,
    'ARMSTRONG'      => 1,
    'BEAVER'         => 1,
    'BUTLER'         => 1,
    'FAYETTE'        => 1,
    'GREENE'         => 1,
    'INDIANA'        => 1,
    'LAWRENCE'       => 1,
    'WASHINGTON'     => 1,
    'WESTMORELAND'   => 1,

    'BLAIR'          => 2,
    'BEDFORD'        => 2,
    'CAMBRIA'        => 2,
    'FULTON'         => 2,
    'HUNTINGDON'     => 2,
    'SOMERSET'       => 2,

    'ADAMS'          => 3,
    'CUMBERLAND'     => 3,
    'DAUPHIN'        => 3,
    'FRANKLIN'       => 3,
    'JUNIATA'        => 3,
    'LEBANON'        => 3,
    'MIFFLIN'        => 3,
    'PERRY'          => 3,
    'YORK'           => 3,
    'LANCASTER'      => 3,

    'BERKS'          => 4,
    'BUCKS'          => 4,
    'CHESTER'        => 4,
    'DELAWARE'       => 4,
    'MONTGOMERY'     => 4,
    'PHILADELPHIA'   => 4,

    'CARBON'         => 5,
    'LEHIGH'         => 5,
    'LACKAWANNA'     => 5,
    'LUZERNE'        => 5,
    'MONROE'         => 5,
    'NORTHAMPTON'    => 5,
    'PIKE'           => 5,
    'SUSQUEHANNA'    => 5,
    'WAYNE'          => 5,
    'WYOMING'        => 5,

    'BRADFORD'       => 6,
    'COLUMBIA'       => 6,
    'LYCOMING'       => 6,
    'MONTOUR'        => 6,
    'NORTHUMBERLAND' => 6,
    'SCHUYLKILL'     => 6,
    'SNYDER'         => 6,
    'SULLIVAN'       => 6,
    'TIOGA'          => 6,
    'UNION'          => 6,

    'CAMERON'        => 7,
    'CENTRE'         => 7,
    'CLEARFIELD'     => 7,
    'CLINTON'        => 7,
    'ELK'            => 7,
    'JEFFERSON'      => 7,
    'MCKEAN'         => 7,
    'POTTER'         => 7,

    'CLARION'        => 8,
    'CRAWFORD'       => 8,
    'ERIE'           => 8,
    'FOREST'         => 8,
    'MERCER'         => 8,
    'VENANGO'        => 8,
    'WARREN'         => 8,
  ];

  write_log('-----------county----------------');
  write_log($county);
  write_log($counties_to_regions);
  write_log('-----------county----------------');
  if (array_key_exists($county, $counties_to_regions)) {
    return $counties_to_regions[$county];
  }
  return false;
}

function imagebox_override_checkout_fields($fields)
{
  $fields['billing']['billing_email']['label'] = 'Billing email address';
  unset($fields['billing']['billing_country']);
  return $fields;
}

function imagebox_custom_edit_account_text()
{
  ?>
  <p>Please click on the tabs below and fill in the mandatory <strong>Address Info</strong>, and as much of the <strong>Other Info</strong> as you can.</p>
<?php
}

function imagebox_quantity_wp_head()
{
  if (is_product() && !is_single('14045')) {?>
  <style type="text/css">.quantity, .buttons_added { width:0; height:0; display: none; visibility: hidden; }</style>
  <?php
}
}

function my_add_how_many_left_message($choice, $exceeded_limit, $field, $form, $count)
{

  $limit         = rgar($choice, 'limit');
  $how_many_left = max($limit - $count, 0);

  $message = "($how_many_left spots left)";

  $choice['text'] = $choice['text'] . " $message";

  return $choice;
}

function imagebox_woocommerce_before_customer_login_form($evolve_woocommerce_after_customer_login_form)
{
  if (strpos($_SERVER['REQUEST_URI'], 'team') !== false) {
    ?>
  <p>You've been invited to join the PSPA!  If you have an account, please login below to join the team.</p>
  <p>To create a new account, <strong><a href="<?=get_site_url(null, '/membership/joining-pspa-register/member-registration/');?>">register as a member here</a></strong>, and return to this page to finish joining the team.</p>
  <?php
}
};
add_action('woocommerce_before_customer_login_form', 'imagebox_woocommerce_before_customer_login_form', 10, 1);

add_action('woocommerce_subscription_status_updated', 'imagebox_update_campaigner', 100, 3);
function imagebox_update_campaigner($subscription, $new_status, $old_status)
{
  write_log('woocommerce_subscription_status_updated');
  $user_id = $subscription->get_customer_id();
  send_to_campaigner($user_id, $new_status, $subscription);
}

function change_campaigner_email_address($old_email, $new_email)
{
  $config     = new VanillaConfig();
  $api        = new API($config);
  $connection = $api->getConnection();
  $data       = [
    'EmailAddress' => $new_email,
  ];
  $update_response = $connection->post('Subscribers/' . $old_email . '/ChangeEmail', $data);
}

function send_to_campaigner($user_id, $new_status = false, $subscription = false)
{
  if($_SERVER['SERVER_ADDR'] == '::1') {
    return true;
  }

  $config     = new VanillaConfig();
  $api        = new API($config);
  $connection = $api->getConnection();

  $trans_usermeta_to_campaigner = array(
    'first_name'          => 'FirstName',
    'last_name'           => 'LastName',
    // billing is HOME address
    'billing_address_1'   => 'ADDRESS1',
    'billing_address_2'   => 'ADDRESS2',
    'billing_city'        => 'CITY',
    'billing_postcode'    => 'ZIP',
    'billing_state'       => 'State',
    'billing_phone'       => 'Phone',
    // shipping is WORK address
    'shipping_address_1'  => 'BAddress1',
    'shipping_address_2'  => 'BAddress2',
    'shipping_city'       => 'BCity',
    'shipping_postcode'   => 'BZip',
    'shipping_state'      => 'BState',
    'MBPSPANumber'        => 'PSPA_NO',
    'MBHideFromDirectory' => 'OmitFromWebDirectory',
    'MBMedicalBoardNum'   => 'MED_BOARD',
    'MBCounty'            => 'COUNTY',
    'MBRegion'            => 'REGION',
    'MBPreferredRegion'   => 'PreferredRegion',
    'MBSenDist'           => 'SEN_DIST',
    'MBHouseDist'         => 'HOUSE_DIST',
    'MBStudentSchool'     => 'PA_PROGRAM',
    'MBYearGraduation'    => '',
    'MBAAPANum'           => 'AAPA_NO',
    'MBNCCPANum'          => 'NCCPA_NO',
    'MBOsteoNum'          => 'OSTEO_BOAR',
    'MBPracticeSetting'   => 'PRAC_STG',
    'MBSpecialty'         => 'SPECIALITY',
    'MBMonthBirth'        => 'Birth Mon',
    'MBReferred'          => 'Referred By',
    'MBYearJoined'        => 'YearJoined',
  );

  $user_info = get_userdata($user_id);
  $user_meta = get_user_meta($user_id);

  $create_new    = false;
  $response_body = '';

  try {
    $response      = $connection->get('Subscribers/' . $user_info->user_email);
    $response_body = $response->getBody();

    write_log('$response_body');
    write_log($response_body);
    if (isset($response_body['ErrorCode'])) {
      $create_new = true;
    }
  } catch (Exception $e) {
    $create_new = true;
  }
  write_log("Initial Campaigner lookup");
  write_log($response_body);
  write_log($create_new ? 'Create NEW!' : 'Update Campaigner Record');
  write_log("New status");
  write_log($new_status);

  $subscriber = array(
    'EmailAddress' => $user_info->user_email,
    'CustomFields' => array(
    ),
  );
  if ($create_new) {
    $subscriber['Lists'] = array('18497887'); // All PA DB Campaigner list
  }

  foreach ($user_meta as $key => $value) {
    if (in_array($key, array_keys($trans_usermeta_to_campaigner))) {
      if ($value[0]) {
        $subscriber['CustomFields'][] = array(
          'FieldName' => $trans_usermeta_to_campaigner[$key],
          'Value'     => $value[0],
        );
      }
    }
  }

  if ($new_status) {
    $subscriber['CustomFields'][] = array(
      'FieldName' => 'Is Member',
      'Value'     => $new_status == 'active' ? '1' : '0',
    );
  } else {
    $args = ['status' => $new_status];

    $membership = wc_memberships_get_user_memberships($user_id, $args);

    write_log('$membership--------------------------------------------------');
    write_log($membership);
    write_log('/$membership^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');

    if (!empty($membership)) {
      $membership_meta = get_post_meta($membership[0]->id, '', true);

      write_log('$membership_meta--------------------------------------------------');
      write_log($membership_meta);
      write_log('/$membership_meta^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');

      $subscriber['CustomFields'][] = array(
        'FieldName' => 'Is Member',
        'Value'     => $membership[0]->get_status() == 'active' ? '1' : '0',
      );
    }
  }

  if ($subscription) {
    $subscriber['CustomFields'][] = array(
      'FieldName' => 'LastRegistered',
      'Value'     => date("Y", strtotime($subscription->get_date('start'))),
    );
    $subscriber['CustomFields'][] = array(
      'FieldName' => 'Expires',
      'Value'     => date("Y", strtotime($subscription->get_date('next_payment'))),
    );
    $subscriber['CustomFields'][] = array(
      'FieldName' => 'Last Paid',
      'Value'     => $subscription->get_total(),
    );
  } elseif (!empty($membership)) {
    if ($membership_meta['_start_date'][0]) {
      $subscriber['CustomFields'][] = array(
        'FieldName' => 'LastRegistered',
        'Value'     => date("Y", strtotime($membership_meta['_start_date'][0])),
      );
    }
    if ($membership_meta['_end_date'][0]) {
      $subscriber['CustomFields'][] = array(
        'FieldName' => 'Expires',
        'Value'     => date("Y", strtotime($membership_meta['_end_date'][0])),
      );
    }
  }

  write_log("New Member data");
  write_log($subscriber);
  $response_from_campaigner = $connection->post('Subscribers/', $subscriber);
  write_log("New Record Campaigner response");
  write_log($response_from_campaigner);
}

function add_to_recipients($reciepient = '', $object = '')
{
  write_log('------------add_to_recipients---------------');
  write_log($reciepient);
  write_log('Order ID: ' . $object->get_id());
  $entry_ids = get_gravity_form_entries_by_order_id($object->get_id());
  $entry_id  = null;
  $entry     = null;
  write_log($entry_ids);
  foreach ($entry_ids as $key => $entry_id) {
    if ($entry_id) {
      $entry = GFAPI::get_entry($entry_id);
      if (isset($entry[6]) && is_email($entry[6])) {
        $reciepient .= ", " . $entry[6];
      }
    }
  }
  write_log($entry_id);
  write_log($entry);
  write_log('reciepients: ' . $reciepient);
  write_log('------------/add_to_recipients---------------');
  return $reciepient;
}

add_filter('woocommerce_email_recipient_customer_completed_order', 'add_to_recipients', 10, 2); // customer_completed_order
// customer_processing_order

add_action('profile_update', 'wp_profile_updated', 10, 2);
function wp_profile_updated($user_id, $old_user_data)
{
  global $wpdb;
  $user_info = get_userdata($user_id);
  $user_meta = get_user_meta($user_id);

  write_log('profile_update');
  if ($user_meta['billing_state'][0] == 'PA' || $user_meta['billing_state'][0] == 'Pennsylvania') {
    $address = $user_meta['billing_address_1'][0] . ' ' . $user_meta['billing_address_2'][0] . ' ' . $user_meta['billing_city'][0] . ' ' . $user_meta['billing_state'][0] . ' ' . $user_meta['billing_postcode'][0] . ' ';
    $civic   = lookupCivicInfo($address);
    update_user_meta($user_id, 'MBSenDist', $civic['pa_house']);
    update_user_meta($user_id, 'MBHouseDist', $civic['pa_senate']);
    $sql        = "SELECT county FROM wp_tt_pa_cities_counties WHERE zip = " . $user_meta['billing_postcode'][0];
    $new_county = $wpdb->get_var($sql);
    $new_county = strtoupper(str_replace(' County', '', $new_county));
    update_user_meta($user_id, 'MBCounty', $new_county);
    update_user_meta($user_id, 'MBRegion', region_from_county($new_county));
  } else {
    update_user_meta($user_id, 'MBCounty', 'OTHER');
    update_user_meta($user_id, 'MBRegion', '0');
  }

  // handle the special case of the user updates their email address
  if ($user_info->user_email != $old_user_data->user_email) {
    change_campaigner_email_address($old_user_data->user_email, $user_info->user_email);
    sleep(5);
  }
  send_to_campaigner($user_id);
}

add_action('wppb_register_success', 'wppbc_register_success', 20, 3);
add_action('wppb_edit_profile_success', 'wppbc_register_success', 20, 3);

function wppbc_register_success($http_request, $form_name = null, $user_id = null)
{
  write_log('wppbc_register_success');
  if (is_numeric($http_request)) {
    $user_id      = $http_request;
    $http_request = $_POST;
  }
  write_log('------------wppb_edit_profile_success---------------');
  write_log($http_request);
  write_log($form_name);
  write_log($user_id);

  if ($http_request['billing_state'][0] == 'PA' || $http_request['billing_state'][0] == 'Pennsylvania') {
    $address = $http_request['billing_address_1'][0] . ' ' . $http_request['billing_address_2'][0] . ' ' . $http_request['billing_city'][0] . ' ' . $http_request['billing_state'][0] . ' ' . $http_request['billing_postcode'][0] . ' ';
    $civic   = lookupCivicInfo($address);
    update_user_meta($user_id, 'MBSenDist', $civic['pa_house']);
    update_user_meta($user_id, 'MBHouseDist', $civic['pa_senate']);
    $sql        = "SELECT county FROM wp_tt_pa_cities_counties WHERE zip = " . $http_request['billing_postcode'][0];
    $new_county = $wpdb->get_var($sql);
    $new_county = strtoupper(str_replace(' County', '', $new_county));
    update_user_meta($user_id, 'MBCounty', $new_county);
    update_user_meta($user_id, 'MBRegion', region_from_county($new_county));
  } else {
    update_user_meta($user_id, 'MBCounty', 'OTHER');
    update_user_meta($user_id, 'MBRegion', '0');
  }

  // $address = $http_request['billing_address_1'] . ' ' . $http_request['billing_address_2'] . ' ' . $http_request['billing_city'] . ' ' . $http_request['billing_state'] . ' ' . $http_request['billing_postcode'] . ' ';
  // $civic   = lookupCivicInfo($address);
  // update_user_meta($user_id, 'MBSenDist', $civic['pa_house']);
  // update_user_meta($user_id, 'MBHouseDist', $civic['pa_senate']);
  // update_user_meta($user_id, 'MBRegion', region_from_county($http_request['MBCounty']));
  // if ($http_request['MBYearJoined'] == '') {
  //   update_user_meta($user_id, 'MBYearJoined', date('Y'));
  // }

  send_to_campaigner($user_id);

  write_log('------------/wppb_edit_profile_success---------------');
}

add_action('fue_before_variable_replacements', 'fue_register_before_variable_replacements', 10, 4);
function fue_register_before_variable_replacements($var, $email_data, $fue_email, $queue_item)
{
  write_log('------------fue_before_variable_replacements---------------');
  write_log($var);
  write_log($email_data);
  write_log($fue_email);
  write_log($queue_item);
  write_log('------------/fue_before_variable_replacements---------------');
  $pspa_num        = get_user_meta($email_data['user_id'], 'MBPSPANumber', true);
  $region          = get_user_meta($email_data['user_id'], 'MBRegion', true);
  $real_first_name = get_user_meta($email_data['user_id'], 'first_name', true);
  $variables       = array(
    'pspa_num'        => $pspa_num,
    'region'          => $region,
    'real_first_name' => $real_first_name,
  );
  $var->register($variables);
}

add_filter('wppb_edit_other_users_dropdown_query_args', 'wppb_users');
function wppb_users($q)
{
  $q['meta_key'] = 'last_name';
  $q['orderby']  = 'meta_value';
  $q['order']    = 'ASC';
  return $q;
}

add_action('wppb_before_saving_form_values', 'tt_wppb_before_saving_form_values');
function tt_wppb_before_saving_form_values($request = null, $args = null)
{
  // echo '<pre>';
  // print_r($request);
  // print_r($args);
  // die();
}

add_filter('gform_disable_address_map_link', '__return_true');

add_filter('login_redirect', function ($url, $query, $user) {
  return site_url('/members-only/');
}, 10, 3);

function after_delete_user($user_id)
{
  global $wpdb;

  $user_obj = get_userdata($user_id);
  $email    = $user_obj->user_email;

  $config          = new VanillaConfig();
  $api             = new API($config);
  $connection      = $api->getConnection();
  $delete_response = $connection->delete('Subscribers/' . $email);
  write_log('------------$delete_response---------------');
  write_log($delete_response);
}
add_action('delete_user', 'after_delete_user');

add_action('wc_memberships_for_teams_add_team_member', 'imagebox_handle_invitation_action', 20, 3);

function imagebox_handle_invitation_action($team_member, $team, $user_membership)
{
  imagebox_add_pspa_num_yr_joined();
} 

function hide_auto_renew_toggle()
{
  ?>
  <style type="text/css"> 
    .wcs-auto-renew-toggle {
        visibility: hidden;
        position: relative;
    }
    .wcs-auto-renew-toggle:after {
        visibility: visible;
        position: absolute;
        top: 0;
        left: 0;
        content: "No Renewal";
    }
  </style>
  <?
}

add_filter( 'wcs_can_user_resubscribe_to_subscription', 'ibx_wcs_can_user_resubscribe_to_subscription', 10, 3 );
add_filter( 'wcs_can_user_resubscribe_to_subscription', 'ibx_wcs_can_user_resubscribe_to_subscription', 10, 3 );
add_filter( 'woocommerce_subscriptions_can_user_renew_early', 'ibx_woocommerce_subscriptions_can_user_renew_early', 10, 4);

function ibx_woocommerce_subscriptions_can_user_renew_early($can_renew_early, $subscription, $user_id, $reason)
{
  $t_items = $subscription->get_items();
  foreach ($t_items as $key => $item) {
    if(is_product_one_time_student($item->get_product())) {
      $can_renew_early = false;
      hide_auto_renew_toggle();
    } elseif(is_product_one_time_graduate($item->get_product())) {
      $can_renew_early = false;
      hide_auto_renew_toggle();
    }
  }
  return $can_renew_early;
}

function ibx_wcs_can_user_resubscribe_to_subscription($can_user_resubscribe, $subscription, $user_id)
{
  $t_items = $subscription->get_items();
  foreach ($t_items as $key => $item) {
    if(is_product_one_time_student($item->get_product())) {
      $can_user_resubscribe = false;
      hide_auto_renew_toggle();
    } elseif(is_product_one_time_graduate($item->get_product())) {
      $can_user_resubscribe = false;
      hide_auto_renew_toggle();
    }
  }
  return $can_user_resubscribe;
}

function ibx_validate_add_cart_item( $passed, $product_id, $quantity, $variation_id = '', $variations= '' ) {
    $product_to_add = wc_get_product( $product_id );
    $product_to_add_is_one_time_student = is_product_one_time_student($product_to_add);
    $product_to_add_is_one_time_graduate = is_product_one_time_graduate($product_to_add);

    $memberships = wc_memberships_get_user_active_memberships();

    if ( ! empty( $memberships ) ) {
      foreach ($memberships as $key => $membership) {
        $subscription = $membership->get_subscription();
      }

      $t_items = $subscription->get_items();
      foreach ($t_items as $key => $item) {
        if($product_to_add_is_one_time_student && is_product_one_time_student($item->get_product())) {
          $passed = false;
          wc_add_notice( __( 'You can not add or renew this membership', 'textdomain' ), 'error' );
        } elseif($product_to_add_is_one_time_graduate && is_product_one_time_graduate($item->get_product())) {
          $passed = false;
          wc_add_notice( __( 'You can not add or renew this membership', 'textdomain' ), 'error' );
        }
      }
    }
    return $passed;
}
add_filter( 'woocommerce_add_to_cart_validation', 'ibx_validate_add_cart_item', 10, 5 );

add_filter( 'woocommerce_get_order_item_totals', 'ibx_woocommerce_get_order_item_totals', 10, 3 );
function ibx_woocommerce_get_order_item_totals($total_rows, $order, $tax_display)
{
  $is_prepaid = false;

  if( $order->get_used_coupons() ) {  
    foreach( $order->get_used_coupons() as $coupon) {
      if(strtoupper($coupon) == 'PSPAPREPAID') {
        $is_prepaid = true;
      }
    }
  }

  if($is_prepaid) {
    unset($total_rows['discount']);
    $total_rows['order_total']['value'] = $total_rows['cart_subtotal']['value'];
  }
  return $total_rows;
}
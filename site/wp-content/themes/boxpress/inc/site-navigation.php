<?php
/**
 * Register site navigations
 *
 * @package BoxPress
 */

function boxpress_register_theme_navs()
{
  register_nav_menus(array(
    'primary' => __('Primary Menu', 'boxpress'),
    'utility' => __('Utility Menu', 'boxpress'),
    'footer'  => __('Footer Menu', 'boxpress'),
  ));
}
add_action('after_setup_theme', 'boxpress_register_theme_navs');

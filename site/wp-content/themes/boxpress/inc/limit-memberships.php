<?php

/**
 * Do not grant membership access to purchasers if they already hold an active membership
 *
 * @param bool $grant_access true if the purchaser should get memberships access from this order
 * @param array $args {
 *      @type int $user_id purchaser's WordPress user ID
 *      @type int $product_id the ID of the access-granting product
 *      @type int $order_id the ID of order for this purchase
 * }
 * @return bool $grant_access
 */
function sv_wc_memberships_limit_membership_count($grant_access, $args)
{

  // get all active memberships for the purchaser
  // you can remove any of these if you don't want to allow multiples
  // ie you may not want to count complimentary memberships
  $statuses = array(
    'status' => array('active', 'complimentary', 'pending', 'free_trial'),
  );

  $active_memberships = wc_memberships_get_user_memberships($args['user_id'], $statuses);

  // if there are any active memberships returned, do not grant access from purchase
  if (!empty($active_memberships)) {
    return false;
  }

  return $grant_access;
}
add_filter('wc_memberships_grant_access_from_new_purchase', 'sv_wc_memberships_limit_membership_count', 1, 2);

add_action('woocommerce_add_to_cart', 'custome_add_to_cart');
function custome_add_to_cart()
{
  global $woocommerce;
  $current_user = wp_get_current_user();
  $current_user->ID;

  // get all active memberships for the purchaser
  // you can remove any of these if you don't want to allow multiples
  // ie you may not want to count complimentary memberships
  $statuses = array(
    'status' => array('active', 'complimentary', 'pending', 'free_trial'),
  );

  $active_memberships = wc_memberships_get_user_memberships($current_user, $statuses);

  // if there are any active memberships returned, do not grant access from purchase
  if (!empty($active_memberships)) {
    return false;
  }
  // $product_id = $_POST['assessories'];

  // $found = false;

  // //check if product already in cart
  // if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
  //     foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
  //         $_product = $values['data'];
  //         if ( $_product->id == $product_id )
  //             $found = true;
  //     }
  //     // if product not found, add it
  //     if ( ! $found )
  //         WC()->cart->add_to_cart( $product_id );
  // } else {
  //     // if no products in cart, add it
  //     WC()->cart->add_to_cart( $product_id );
  // }
}

function filter_woocommerce_add_to_cart_validation($passed, $product_id, $quantity)
{
  $terms                 = get_the_terms($product_id, 'product_cat');
  $is_membership_product = in_array('memberships', array_map(function ($t) {return $t->slug;}, $terms));

  $current_user = wp_get_current_user();
  $statuses     = array(
    'status' => array('active', 'complimentary', 'pending', 'free_trial'),
  );

  // $active_memberships = wc_memberships_get_user_memberships($current_user, $statuses);

  global $woocommerce;
  $items                         = $woocommerce->cart->get_cart();
  $is_in_cart_membership_product = false;
  foreach ($items as $item => $values) {
    $_product                      = wc_get_product($values['data']->get_id());
    $terms                         = get_the_terms($values['data']->get_id(), 'product_cat');
    $is_in_cart_membership_product = in_array('memberships', array_map(function ($t) {return $t->slug;}, $terms));
    if ($is_in_cart_membership_product && $is_membership_product) {
      wc_add_notice(__('You are not allowed to have more than one membership.', 'woocommerce'), 'error');
      $passed = false;
      break;
    }
  }
  return $passed;
}
// add the filter
add_filter('woocommerce_add_to_cart_validation', 'filter_woocommerce_add_to_cart_validation', 10, 3);

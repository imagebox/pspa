<h1>Manage Duplicate Members</h1>
<?php
$args                   = array('orderby' => 'registered', 'order' => 'DESC'); // , 'number' => 10
$user_query             = new WP_User_Query($args);
$duplicate_delete_nonce = wp_create_nonce('duplicate_delete');

$users = $user_query->get_results();

$dups_fla = array();
$dups_fl  = [];

foreach ($users as $key => $user) {
  $users[$key]->meta = get_user_meta($user->ID);
  if (isset($dups_fla[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0] . $users[$key]->meta['billing_address_1'][0]]['count'])) {
    $dups_fla[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0] . $users[$key]->meta['billing_address_1'][0]]['count'] += 1;
  } else {
    $dups_fla[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0] . $users[$key]->meta['billing_address_1'][0]]['count'] = 1;
  }

  if (isset($dups_fl[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0]]['count'])) {
    $dups_fl[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0]]['count'] += 1;
  } else {
    $dups_fl[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0]]['count'] = 1;
  }

  $dups_fla[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0] . $users[$key]->meta['billing_address_1'][0]]['ids'][] = $user->ID;
  $dups_fl[$users[$key]->meta['first_name'][0] . $users[$key]->meta['last_name'][0]]['ids'][]                                               = $user->ID;

}
?>
<h3>Duplicate First, Last Name and Address</h3>
<?
foreach ($dups_fla as $key => $fla) {
  if ($fla['count'] > 1) {
    // print_r($fla);
    echo '<ul>';
    foreach ($fla['ids'] as $key => $id) {
      $dup_user       = get_userdata($id);
      $dup_user->meta = get_user_meta($id);
      // print_r($dup_user);
      echo '<li>' . $dup_user->meta['first_name'][0] . ' ' . $dup_user->meta['last_name'][0] . ' (' . $dup_user->user_email . ')' . '<a href = "' . get_site_url() . '/wp-admin/user-edit.php?user_id=' . $dup_user->ID . '" target="_blank"> View/Edit</a> - <a href="' . wp_nonce_url("users.php?action=delete&amp;user=$dup_user->ID", 'bulk-users') . '" style="color: red;"><strong>DELETE THIS ONE</strong></a></li>';
    }
    echo '</ul><br>';
  }
}
?>
<h3>Duplicate First and Last Name</h3>
<?
foreach ($dups_fl as $key => $fl) {
  if ($fl['count'] > 1) {
    // print_r($fl);
    echo '<ul>';
    foreach ($fl['ids'] as $key => $id) {
      $dup_user       = get_userdata($id);
      $dup_user->meta = get_user_meta($id);
      // print_r($dup_user);
      echo '<li>' . $dup_user->meta['first_name'][0] . ' ' . $dup_user->meta['last_name'][0] . ' (' . $dup_user->user_email . ')' . '<a href = "' . get_site_url() . '/wp-admin/user-edit.php?user_id=' . $dup_user->ID . '" target="_blank"> View/Edit</a> - <a href="' . wp_nonce_url("users.php?action=delete&amp;user=$dup_user->ID", 'bulk-users') . '" style="color: red;"><strong>DELETE THIS ONE</strong></a></li>';
    }
    echo '</ul><br>';
  }
}
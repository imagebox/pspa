<div class="wrap">
  <h2>Conference Reports</h2><br>
  <a href="<?php echo admin_url('admin.php') ?>?action=rRegistrantsByCategory&_wpnonce=<?php echo wp_create_nonce('PSPAReports') ?>" class="page-title-action"><?php _e('Export Registrants By Category to CSV');?></a><br><br>
  <a href="<?php echo admin_url('admin.php') ?>?action=rFinancials&_wpnonce=<?php echo wp_create_nonce('PSPAReports') ?>" class="page-title-action"><?php _e('Export Financials to CSV');?></a><br><br>
  <a href="<?php echo admin_url('admin.php') ?>?action=rRegistrationDeskList&_wpnonce=<?php echo wp_create_nonce('PSPAReports') ?>" class="page-title-action"><?php _e('Export Registration Desk List to CSV');?></a><br><br>
  <a href="<?php echo admin_url('admin.php') ?>?action=rWorkshopSignups&_wpnonce=<?php echo wp_create_nonce('PSPAReports') ?>" class="page-title-action"><?php _e('Export Workshop Signups to PDF');?></a><br /><br />
  <a href="<?php echo admin_url('admin.php') ?>?action=rFunRunReport&_wpnonce=<?php echo wp_create_nonce('PSPAReports') ?>" class="page-title-action"><?php _e('Export Fun Run Particpant List to CSV');?></a><br /><br />
  <a href="<?php echo admin_url('admin.php') ?>?action=rAllData&_wpnonce=<?php echo wp_create_nonce('PSPAReports') ?>" class="page-title-action"><?php _e('Export All Conference Data to CSV');?></a>
</div>

<?
$counts = rConferenceCounts();
?>
<hr>
<h2>Attendance/Meals Report</h2>
<table>
  <tr>
    <td>
      <table>
        <tr>
          <th colspan="2">Registered Conference Attendees</th>
        </tr>
        <tr>
          <td>All PSPA Members</td>
          <td><?=$counts['All PSPA Members'];?></td>
        </tr>
        <tr>
          <td>All Constituents</td>
          <td><?=$counts['All Constituents'];?></td>
        </tr>
        <tr>
          <td>All Non-Members</td>
          <td><?=$counts['All Non-Members'];?></td>
        </tr>
        <tr>
          <td>Registrant Guests</td>
          <td><?=$counts['Registrant Guests'];?></td>
        </tr>
        <tr>
          <td>All Non-Attendees</td>
          <td><?=$counts['All Non-Attendees'];?></td>
        </tr>
        <tr>
          <td>Total Registered</td>
          <td><?=$counts['Total Registered'];?></td>
        </tr>
      </table>
    </td>
    <td rowspan="6" valign="top">
      <table>
        <tr>
          <td colspan="2"><h3>Meals (Registrants/Guests)</h3></td>
        </tr>
        <tr>
          <td>Wednesday</td>
          <td><?=$counts['Meals Wednesday'];?></td>
        </tr>
        <tr>
          <td>Thursday</td>
          <td><?=$counts['Meals Thursday'];?></td>
        </tr>
        <tr>
          <td>Friday</td>
          <td><?=$counts['Meals Friday'];?></td>
        </tr>
        <tr>
          <td>Saturday</td>
          <td><?=$counts['Meals Saturday'];?></td>
        </tr>
        <tr>
          <td colspan="2"><h3>Non-Attendee Meal Count</h3></td>
        </tr>
        <tr>
          <td>Thursday</td>
          <td><?=$counts['Non-Attendee Thursday'];?></td>
        </tr>
<!--         <tr>
          <td>Friday Luncheon</td>
          <td><?=$counts['Non-Attendee Friday Luncheon'];?></td>
        </tr> -->
        <tr>
          <td>Friday Dinner</td>
          <td><?=$counts['Non-Attendee Friday Dinner'];?></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <h4>Attendee Breakdown by Category</h4>
      <table>
        <tr>
          <th colspan="2">PSPA Members</th>
        </tr>
        <tr>
          <td>Graduates</td>
          <td><?=$counts['PSPA Graduates'];?></td>
        </tr>
        <tr>
          <td>Students</td>
          <td><?=$counts['PSPA Students'];?></td>
        </tr>
      </table>
    </td>
  </tr>

  <tr>
    <td>
      <table>
        <tr>
          <th colspan="2">Membership and Conference</th>
        </tr>
        <tr>
          <td>Graduates</td>
          <td><?=$counts['Membership and Conference Graduates'];?></td>
        </tr>
        <tr>
          <td>Students - 1 yr</td>
          <td><?=$counts['Membership and Conference Students - 1 yr'];?></td>
        </tr>
        <tr>
          <td>Students - 2 yr</td>
          <td><?=$counts['Membership and Conference Students - 2 yr'];?></td>
        </tr>
      </table>
    </td>
  </tr>
  <!--
'PSPA Graduates'                            => 0,
'PSPA Students'                             => 0,
'Membership and Conference Graduates'       => 0,
'Membership and Conference Students - 1 yr' => 0,
'Membership and Conference Students - 2 yr' => 0,
'Constituent Members Graduates'             => 0,
'Constituent Members Students'              => 0,
'Non-Members Graduates'                     => 0,
'Non-Members Students'                      => 0,
  -->
  <tr>
    <td>
      <table>
        <tr>
          <th colspan="2">Constituent Members</th>
        </tr>
        <tr>
          <td>Graduates</td>
          <td><?=$counts['Constituent Members Graduates'];?></td>
        </tr>
        <tr>
          <td>Students</td>
          <td><?=$counts['Constituent Members Students'];?></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table>
        <tr>
          <th colspan="2">Non-Members</th>
        </tr>
        <tr>
          <td>Graduates</td>
          <td><?=$counts['Non-Members Graduates'];?></td>
        </tr>
        <tr>
          <td>Students</td>
          <td><?=$counts['Non-Members Students'];?></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table>
        <tr>
          <td>VIPs</td>
          <td><?=$counts['VIPs'];?></td>
          <td>Poster Presenters</td>
          <td><?=$counts['Poster Presenters'];?></td>
        </tr>
        <tr>
          <td>Volunteers</td>
          <td><?=$counts['Volunteers'];?></td>
          <td>Challenge Bowl</td>
          <td><?=$counts['Challenge Bowl'];?></td>
        </tr>
        <tr>
          <td>Speakers</td>
          <td><?=$counts['Speakers'];?></td>
          <td>Diversity Project Winners</td>
          <td><?=$counts['Diversity Project Winners'];?></td>
        </tr>
        <tr>
          <td>Leadership Panelists</td>
          <td><?=$counts['Leadership Panelists'];?></td>
          <td>VIP Guests</td>
          <td><?=$counts['VIP Guests'];?></td>
        </tr>
        <tr>
          <td>Scholarship Award Winners</td>
          <td><?=$counts['Scholarship Award Winners'];?></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

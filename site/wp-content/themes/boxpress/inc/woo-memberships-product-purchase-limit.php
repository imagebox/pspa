<?php
/**
 * https://www.skyverge.com/blog/prevent-repeat-purchase-with-woocommerce/
 */
function sv_purchase_disabled_message() {

    global $product;
    $product_to_add = $product;
    $product_to_add_is_one_time_student = is_product_one_time_student($product_to_add);
    $product_to_add_is_one_time_graduate = is_product_one_time_graduate($product_to_add);

    $memberships = wc_memberships_get_user_active_memberships();

    if ( ! empty( $memberships ) ) {
      foreach ($memberships as $key => $membership) {
        $subscription = $membership->get_subscription();
      }

      $t_items = $subscription->get_items();
      foreach ($t_items as $key => $item) {
        if($product_to_add_is_one_time_student && is_product_one_time_student($item->get_product())) {
          $passed = false;
          echo '<div class="woocommerce"><div class="woocommerce-info wc-nonpurchasable-message">You\'ve already purchased this type ofmembership. It can only be purchased once.</div></div>';
        } elseif($product_to_add_is_one_time_graduate && is_product_one_time_graduate($item->get_product())) {
          $passed = false;
          echo '<div class="woocommerce"><div class="woocommerce-info wc-nonpurchasable-message">You\'ve already purchased this membership. It can only be purchased once.</div></div>';
        }
      }
    }
}
add_action( 'woocommerce_single_product_summary', 'sv_purchase_disabled_message', 31 );

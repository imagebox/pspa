<?php

function register_acf_block_types()
{

  // register a testimonial block.
  acf_register_block_type(array(
    'name'            => 'testimonial',
    'title'           => __('Testimonial'),
    'description'     => __('A custom testimonial block.'),
    'render_template' => 'template-parts/blocks/testimonial/testimonial.php',
    'category'        => 'formatting',
    'icon'            => 'admin-comments',
    'keywords'        => array('testimonial', 'quote'),
    'enqueue_style'   => get_template_directory_uri() . '/assets/css/style.min.css',
    // 'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.css',
  ));

  // register a testimonial block.
  acf_register_block_type(array(
    'name'            => 'photoblock',
    'title'           => __('Photo Block'),
    'description'     => __('A custom photo block.'),
    'render_template' => 'template-parts/blocks/photo/photo.php',
    'category'        => 'formatting',
    'icon'            => 'format-image',
    'keywords'        => array('photoblock', 'photo block'),
    //'enqueue_style'     => get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.css',
  ));

}

// Check if function exists and hook into setup.
if (function_exists('acf_register_block_type')) {
  add_action('acf/init', 'register_acf_block_types');
}

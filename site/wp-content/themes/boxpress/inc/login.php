<?
add_action('login_form_middle', 'add_lost_password_link');
function add_lost_password_link()
{
  return '<a href="/wp-login.php?action=lostpassword">Forgot Your Password?</a>';
}

if (!function_exists('boxpress_login_page')) {
  function boxpress_login_page()
  {
    $args = array(
      'echo'           => true,
      'remember'       => true,
      'redirect'       => (is_ssl() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
      'form_id'        => 'loginform',
      'id_username'    => 'user_login',
      'id_password'    => 'user_pass',
      'id_remember'    => 'rememberme',
      'id_submit'      => 'wp-submit',
      'label_username' => __('Username or Email Address'),
      'label_password' => __('Password'),
      'label_remember' => __('Remember Me'),
      'label_log_in'   => __('Log In'),
      'value_username' => '',
      'value_remember' => false,
    );
    wp_login_form($args);
    add_lost_password_link();
  }
  add_shortcode('boxpress-login-page', 'boxpress_login_page');
}

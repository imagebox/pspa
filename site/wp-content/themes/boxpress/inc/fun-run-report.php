<div class="wrap">
  <h1>Fun Run Report <a href="<?php echo admin_url('admin.php') ?>?action=rFunRunReport&_wpnonce=<?php echo wp_create_nonce('PSPAReports') ?>" class="page-title-action"><?php _e('Export Fun Run Particpant List to CSV');?></a></h1>
  <div class="fun-run-report">
    <div class="left-column">
      <?php
        $form_id = 14;
        $paging = array( 'offset' => 0, 'page_size' => 200 );
        $total_count     = 0;
        $sorting         = array( 'key' => '1.6', 'direction' => 'ASC' );

        $search_criteria = array(
          'field_filters' => array(
          'mode' => 'all',
            array(
              'key'   => '20',
              'operator' => 'contains',
              'value' => 'Yes'
            ),
            array(
              'key' => '22',
              'operator' => 'contains',
              'value' => 'Particpate Fun Run / Bike / Run + Shirt',
            )
          )
        );

        $entries = GFAPI::get_entries( $form_id, $search_criteria, $sorting, $paging, $total_count );
        $result  = GFAPI::count_entries( $form_id, $search_criteria );
      ?>

      <h2>Fun Run Participants - Total: <?php echo $result;?></h2>

      <table>
        <thead>
          <tr>
            <th>Last Name</th>
            <th>First Name</th>
            <th>T-Shirt Size</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($entries as $entry) {
            $first_name = rgar( $entry, '1.3' );
            $last_name = rgar( $entry, '1.6' );
            $shirt_size = rgar( $entry, '23');
          ?>
          <tr>
            <td><?php echo $last_name;?></td>
            <td><?php echo $first_name;?></td>
            <td><?php echo $shirt_size;?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>

    </div><!--left-column-->


    <div class="right-column">
      <?php
        // get total of small t-shirts
        $search_small = array(
          'field_filters' => array(
            array(
              'key'   => '23', // shirt selection field id
              'value' => 'Small'
            )
          )
        );
        $entry_count_small = GFAPI::count_entries(14, $search_small); // 14 is form id

        // get total of medium t-shirts
        $search_medium = array(
          'field_filters' => array(
            array(
              'key'   => '23', // shirt selection field id
              'value' => 'Medium'
            )
          )
        );
        $entry_count_medium = GFAPI::count_entries(14, $search_medium); // 14 is form id

        // get total of large t-shirts
        $search_large = array(
          'field_filters' => array(
            array(
              'key'   => '23', // shirt selection field id
              'value' => 'Large'
            )
          )
        );
        $entry_count_large = GFAPI::count_entries(14, $search_large); // 14 is form id


        // get total of x large t-shirts
        $search_xlarge = array(
          'field_filters' => array(
            array(
              'key'   => '23', // shirt selection field id
              'value' => 'X Large'
            )
          )
        );
        $entry_count_xlarge = GFAPI::count_entries(14, $search_xlarge); // 14 is form id


        // get total of xx large t-shirts
        $search_xxlarge = array(
          'field_filters' => array(
            array(
              'key'   => '23', // shirt selection field id
              'value' => 'XX Large'
            )
          )
        );
        $entry_count_xxlarge = GFAPI::count_entries(14, $search_xxlarge); // 14 is form id

      ?>

      <h2>All Shirt Size Totals</h2>
      <table>
        <thead>
          <tr>
            <th>Small</th>
            <th>Medium</th>
            <th>Large</th>
            <th>X Large</th>
            <th>XX Large</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $entry_count_small; ?></td>
            <td><?php echo $entry_count_medium; ?></td>
            <td><?php echo $entry_count_large; ?></td>
            <td><?php echo $entry_count_xlarge; ?></td>
            <td><?php echo $entry_count_xxlarge; ?></td>
          </tr>
        </tbody>
      </table>



      <?php
        $form_id_fr_tshirt = 14;
        $paging_fr_tshirt = array( 'offset' => 0, 'page_size' => 200 );
        $total_count_fr_tshirt     = 0;
        $sorting_fr_tshirt         = array( 'key' => '1.6', 'direction' => 'ASC' );

        $search_criteria_fr_tshirt = array(
          'field_filters' => array(
          'mode' => 'all',
            array(
              'key'   => '20',
              'operator' => 'contains',
              'value' => 'Yes'
            ),
            array(
              'key'   => '22',
              'operator' => 'contains',
              'value' => 'Fun Run Shirt Only'
            ),
          )
        );

        $entries_fr_tshirt = GFAPI::get_entries( $form_id_fr_tshirt, $search_criteria_fr_tshirt, $sorting_fr_tshirt, $paging_fr_tshirt, $total_count_fr_tshirt );
        $result_fr_tshirt  = GFAPI::count_entries( $form_id_fr_tshirt, $search_criteria_fr_tshirt );
      ?>

      <h2>Fun Run T-Shirt Only - Total: <?php echo $result_fr_tshirt;?></h2>

      <table>
        <thead>
          <tr>
            <th>Last Name</th>
            <th>First Name</th>
            <th>T-Shirt Size</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($entries_fr_tshirt as $entry_fr_tshirt) {
            $first_name = rgar( $entry_fr_tshirt, '1.3' );
            $last_name = rgar( $entry_fr_tshirt, '1.6' );
            $shirt_size = rgar( $entry_fr_tshirt, '23');
          ?>
          <tr>
            <td><?php echo $last_name;?></td>
            <td><?php echo $first_name;?></td>
            <td><?php echo $shirt_size;?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table>


    </div><!--right-column-->


  </div>

</div>

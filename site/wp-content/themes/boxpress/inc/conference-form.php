<?php

/**
 * Conference Registration Functions
 */

// PSPA Members, Professionals, Graduates
if (strpos($_SERVER['REQUEST_URI'], "/product/pspa-member-full-conference") !== false
 || strpos($_SERVER['REQUEST_URI'], "/product/pspa-member-daily-tuition-rates-conference") !== false
 //|| strpos($_SERVER['REQUEST_URI'], "/product/member-of-other-state-chapter-full-conference") !== false
){

  add_filter( 'gform_field_value_conference_route', 'pspa_custom_population_function' );
  function pspa_custom_population_function( $value ) {
    return 'professional_pspa_member';
  }

}


// --/product/member-of-other-state-chapter-full-conference/


//
// Constituent professionals
if (strpos($_SERVER['REQUEST_URI'], "/product/member-of-other-state-chapter-full-conference/") !== false){

  add_filter( 'gform_field_value_constituent', 'pspa_custom_constituent_population_function' );
  function pspa_custom_constituent_population_function( $value ) {
    return 'yes';
  }

  add_filter( 'gform_field_value_conference_route', 'pspa_custom_population_function' );
  function pspa_custom_population_function( $value ) {
    return 'constituent_member_professional';
  }

}


// Constituent students
if (strpos($_SERVER['REQUEST_URI'], "/product/student-member-of-other-state-chapter-full-conference/") !== false){

  add_filter( 'gform_field_value_constituent', 'pspa_custom_constituent_population_function' );
  function pspa_custom_constituent_population_function( $value ) {
    return 'yes';
  }

  add_filter( 'gform_field_value_conference_route', 'pspa_custom_population_function' );
  function pspa_custom_population_function( $value ) {
    return 'constituent_member_student';
  }

}



// Non-Members
if (strpos($_SERVER['REQUEST_URI'], "/product/non-member-full-conference") !== false
 || strpos($_SERVER['REQUEST_URI'], "/product/non-member-daily-tuition-rates-conference") !== false
 || strpos($_SERVER['REQUEST_URI'], "/product/non-member-full-conference-1-year-membership") !== false
){

  add_filter( 'gform_field_value_conference_route', 'pspa_custom_population_function' );
  function pspa_custom_population_function( $value ) {
    return 'professional_non_member';
  }

}


// Student Members
if (strpos($_SERVER['REQUEST_URI'], "/product/student-member-full-conference") !== false
 || strpos($_SERVER['REQUEST_URI'], "/product/student-member-daily-tuition-rates-conference") !== false
 // || strpos($_SERVER['REQUEST_URI'], "/product/student-member-of-other-state-chapter-full-conference") !== false
){

  add_filter( 'gform_field_value_conference_route', 'pspa_custom_population_function' );
  function pspa_custom_population_function( $value ) {
    return 'student_pspa_member';
  }

}


// Student Non-Members
if (strpos($_SERVER['REQUEST_URI'], "/product/student-non-member-full-conference") !== false
 || strpos($_SERVER['REQUEST_URI'], "/product/student-non-member-daily-tuition-rates-conference") !== false
){

  add_filter( 'gform_field_value_conference_route', 'pspa_custom_population_function' );
  function pspa_custom_population_function( $value ) {
    return 'student_non_member';
  }

}



// Conference Non-Registrants
if (strpos($_SERVER['REQUEST_URI'], "/product/conference-non-registrants") !== false){

  add_filter( 'gform_field_value_conference_route', 'pspa_custom_population_function' );
  function pspa_custom_population_function( $value ) {
    return 'non_registrant';
  }

}

/**
 * Conference Codes
 * P=PSPA Fellow
 * S=PSPA Student
 * C=Constituent Fellow
 * CS=Constituent Student
 * A=Membership & Conference
 * A1=Student 1-yr membership & conference
 * A2=Student 2-yr membership & conference
 * XM=Exempt Member (Free tuition)
 * N=Non-member fellow
 * NS=Non-member student
 * NA=Non-attendee (speaker, VIP, etc.)
 * D=Deleted.
 */

// PSPA Fellow
 if (strpos($_SERVER['REQUEST_URI'], "/product/pspa-member-full-conference/") !== false
  || strpos($_SERVER['REQUEST_URI'], "/product/pspa-member-daily-tuition-rates-conference/") !== false){
   add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_p' );
   function pspa_conf_code_population_function_p( $value ) {
     return 'P';
   }

 }


 // Constituent Fellow
  if (strpos($_SERVER['REQUEST_URI'], "/product/member-of-other-state-chapter-full-conference/") !== false){
    add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_c' );
    function pspa_conf_code_population_function_c( $value ) {
      return 'C';
    }

  }


  // Membership & Conference
   if (strpos($_SERVER['REQUEST_URI'], "/product/non-member-full-conference-1-year-membership/") !== false){
     add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_a' );
     function pspa_conf_code_population_function_a( $value ) {
       return 'A';
     }

   }


   // Non-member fellow
    if (strpos($_SERVER['REQUEST_URI'], "/product/non-member-full-conference/") !== false
     || strpos($_SERVER['REQUEST_URI'], "/product/non-member-daily-tuition-rates-conference/") !== false){
      add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_n' );
      function pspa_conf_code_population_function_n( $value ) {
        return 'N';
      }

    }


    // PSPA Student
     if (strpos($_SERVER['REQUEST_URI'], "/product/student-member-full-conference/") !== false
      || strpos($_SERVER['REQUEST_URI'], "/product/student-member-daily-tuition-rates-conference/") !== false){
       add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_s' );
       function pspa_conf_code_population_function_s( $value ) {
         return 'S';
       }

     }


     // Constituent Student
      if (strpos($_SERVER['REQUEST_URI'], "/product/student-member-of-other-state-chapter-full-conference/") !== false){
        add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_cs' );
        function pspa_conf_code_population_function_cs( $value ) {
          return 'CS';
        }

      }


      // Student 1-yr membership & conference
       if (strpos($_SERVER['REQUEST_URI'], "/product/student-non-member-full-conference-1-yr-membership/") !== false){
         add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_a1' );
         function pspa_conf_code_population_function_a1( $value ) {
           return 'A1';
         }

       }

       // Student 2-yr membership & conference
        if (strpos($_SERVER['REQUEST_URI'], "/product/student-non-member-full-conference-2-year-membership/") !== false){
          add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_a2' );
          function pspa_conf_code_population_function_a2( $value ) {
            return 'A2';
          }

        }


        // Non-member student
         if (strpos($_SERVER['REQUEST_URI'], "/product/student-non-member-full-conference/") !== false
          || strpos($_SERVER['REQUEST_URI'], "/product/student-non-member-daily-tuition-rates-conference/") !== false){
           add_filter( 'gform_field_value_conference_code', 'pspa_conf_code_population_function_ns' );
           function pspa_conf_code_population_function_ns( $value ) {
             return 'NS';
           }

         }
